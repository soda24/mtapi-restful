# -*- coding: utf-8 -*-
"""
    manage
    ~~~~~~

    Manager module
"""
import logging
import sys

from flask.ext.script import Manager

from flc import create_app

root = logging.getLogger()
root.setLevel(logging.INFO)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

manager = Manager(create_app())

root = logging.getLogger()
root.setLevel(logging.INFO)

if __name__ == "__main__":
    manager.run()
