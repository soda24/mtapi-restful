from flc.db.syncdb import *

generate_db_schema()

generate_data()

#Nuevo usuario
# from flc.tasks.teams import create_user; create_user('aaaaa@a.com', 1, "responsable", 1)

#Usuario existente sin perfil de responsable
# from flc.tasks.teams import create_profile; create_profile(1, 1, "responsable", 1)
