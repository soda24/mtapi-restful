# -*- coding: utf-8 -*-
import json
import logging
import uuid
from wsgiref import simple_server

import requests

from flc import create_app

# # Useful for debugging problems in your API; works with pdb.set_trace()
# app = create_app()

# if __name__ == '__main__':
#     httpd = simple_server.make_server('127.0.0.1', 8000, app)
#     httpd.serve_forever(debug=True)

from werkzeug.contrib.fixers import ProxyFix

app = create_app()

app.wsgi_app = ProxyFix(app.wsgi_app)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  return response

if __name__ == "__main__":
    # app.run('127.0.0.1', 8000, debug=True)
    app.run('0.0.0.0', 5000, debug=True)