# -*- coding: utf-8 -*-
# Copyright 2014-2015 Gran Liga
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy
# of the License at http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


from flask_security import UserMixin, RoleMixin

# from flc.db.connector import BaseModel
from flc.core import db, BaseModel
from flc.models import *
from flc.utils.logs import logger

import uuid
import datetime
import unicodedata

from dateutil.tz import tzlocal
from dateutil.relativedelta import relativedelta
# from flc.utils.checks import check_python
import xlrd
import csv
import random

# check_python()
password = 'lasagna'

avatars = [
    'default_avatar1.png',
    'default_avatar2.png',
    'default_avatar3.jpg',
    'default_avatar4.jpg',
    'default_avatar5.jpg',
    'default_avatar6.png',
    'default_avatar7.jpg',
    'default_avatar8.png',
]
def generate_db_schema():
    # Try to create the database tables, don't do anything if they fail
    try:
        print "Dropeando schema"
        BaseModel.metadata.drop_all(bind=db.engine)
        print "Creando schema"
        BaseModel.metadata.create_all(bind=db.engine)
    except Exception as e:
        logger.error(e)
        print(e)

def generate_data():
    print "creando usuario...."
    user = User(email='nicolas.reynoso@gmail.com', id=1, first_name=u'Nicolás Alejo', last_name="Reynoso", is_active=True)
    user.hash_password('lasagna')
    user.save()

    print "creando perfil...."
    profile = Profile(user_id=user.id, id=1, role='organizer')
    profile.save()

    print "creando perfil...."
    responsable = Profile(user_id=user.id, role='responsable')
    responsable.save()
    referee = Profile(user_id=user.id, role='referee')
    referee.save()
    observer = Profile(user_id=user.id, role='observer')
    observer.save()

    print "creando torneo...."
    tournament = Tournament(
        id=1,
        name="El torneo de tu vida",
        start_date="2015-04-01",
        can_edit=True,
        organizer_profile_id=1,
        min_players = 4,
        max_players = 5,
        max_players_in_field = 5,
        min_players_in_field = 4,
        max_players_age = 1,
        min_players_age = 1,
        max_changes_allowed = 1,
        match_day_of_week = 1,
        match_half_length = 1,
        match_recess_length = 1,
        match_between_match_length = 1,
        match_rematch = 1,
        teams_amount = 4,
        status = 'i',
        setup_stage = 3,
        is_setup_complete = False,
        is_active = True,
        is_fixture_complete = False,
        )
    tournament.save()

    # print "creando equipo...."
    # team = Team(name="River Plate", responsable_profile_id=responsable.profile_id, tournament_id=1, is_active=True, status='i')
    # team.save()
    # team = Team(name="Boca Juniors", responsable_profile_id=responsable.profile_id, tournament_id=1, is_active=True, status='i')
    # team.save()
    # team = Team(name="San Lorenzo", responsable_profile_id=responsable.profile_id, tournament_id=1, is_active=True, status='i')
    # team.save()
    # team = Team(name="Independiente", responsable_profile_id=responsable.profile_id, tournament_id=1, is_active=True, status='i')
    # team.save()

    TournamentReferee(tournament_id=1,  profile_id=referee.profile_id,  is_link_accepted=True).save()
    TournamentObserver(tournament_id=1, profile_id=observer.profile_id, is_link_accepted=True).save()
    sp = Sportcenter(name='La mejor cancha de tu vida', country='Argentina', state='Buenos Aires', city='Capital Federal', address='Heredia 550', fields=4)
    sp.save()
    TournamentSportcenter(tournament_id=1, sportcenter_id=sp.id).save()
    team_players = read_xls()
    for team_name, players in team_players.items():
        escudo = players['escudo']
        players = players['players']
        team = Team(name=team_name, responsable_profile_id=responsable.profile_id, tournament_id=1, is_active=True, status='i')
        team.save()
        n=team.id; group = n/1000;
        group_path = '-'.join([str(group*1000), str((group+1)*1000)])
        team.logo_path = 'teams/'+group_path+'/'+escudo
        team.save()
        for player in players:
            team_clean = elimina_tildes(team_name.replace(' ', ''))
            email = u'{}_{}@{}.com'.format(player['first_name'].replace(' ', '_'), player['last_name'], team_clean).replace(' ', '').lower()
            player['email'] = elimina_tildes(email)

            #CREAR JUGADOR
            user = User(email=player['email'], first_name=player['first_name'], last_name=player['last_name'], is_active=True, password='XourUemvJWotJHYZKENaaRyzRbweUIpC9GlWocPbdcOqUZbAqI7uo5bLy3ZmiYDPf/5DTg1FMXavLDlBZd5Csw==')
            user.avatar_path = u'players/0-1000/'+random.choice(avatars)
            # user.hash_password('lasagna')
            user.save()

            print "creando perfil....", user.id
            profile = Profile(user_id=user.id, role='player', is_active=True)
            profile.save()

            tp = TeamPlayer(
                team_id       = team.team_id,
                profile_id    = profile.profile_id,
                tournament_id = team.tournament_id,
                created_by    = 1,
                is_active     = True,
                is_link_accepted = True,
            )
            tp.save()
    Fixture.create(1, {'fixture': Fixture.generate(1)})

    for x in range(5):
        MatchObserver(match_id=x+1, profile_id=observer.id, is_link_accepted=True).save()


def read_xls():
    book = xlrd.open_workbook('deploy/EQUIPOS_A.xls') # Open an .xls file
    sheet = book.sheet_by_index(0) # Get the first sheet
    data = []
    teams = {}

    for counter in range(10000): # Loop for five times
        try:
            rowValues = sheet.row_values(counter, start_colx=0, end_colx=8)
            team, position, nationality, name, escudo = rowValues
            team = team.lower().title().strip()
            position = position.lower().title().strip()
            nationality = nationality.lower().title().strip()
            name = name.lower().title().strip()
            last_name, first_name = map(lambda x: x.strip(), name.split(','))
            if not team in teams:
                teams[team] = {'name': team, 'players': [], 'escudo': escudo}
            if name not in teams[team]:
                teams[team]['players'].append({'name': name, 'first_name': first_name, 'last_name': last_name, 'nationality': nationality, 'position': position})
            # data.append(','.join(map(lambda x: str(x), rowValues)))
        except IndexError:
            break
    return teams

def elimina_tildes(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

def get_hmac(passwd):
    h = hmac.new(SECURITY_PASSWORD_SALT, passwd.encode('utf-8'), hashlib.sha512)
    return base64.b64encode(h.digest())


