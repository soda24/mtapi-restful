# -*- coding: utf-8 -*-
import simplejson as json

from flask.ext.sqlalchemy import declarative_base, _QueryProperty, Model, _BoundDeclarativeMeta, BaseQuery

from flc import settings
from flc.utils.logs import logger

# def make_declarative_base():
#     """Creates the declarative base."""
#     base = declarative_base(cls=Model, name='Model',
#                             metaclass=_BoundDeclarativeMeta)
#     base.query = _QueryProperty()
#     return base


# class Model(object):
#     """Baseclass for custom user models."""

#     #: the query class used. The `query` attribute is an instance
#     #: of this class. By default a `BaseQuery` is used.
#     query_class = BaseQuery

#     #: an instance of `query_class`. Can be used to query the
#     #: database for instances of this model.
#     query = BaseQuery

# BaseModel = declarative_base()
