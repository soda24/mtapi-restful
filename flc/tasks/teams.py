# -*- coding: utf-8 -*-
from flc.models import *

def create_user(email, team_id, role, request_profile_id):
    requester = Profile.query.get(request_profile_id)
    email = email.strip().lower()
    user = User.query.filter_by(email=email).first()
    assert not user, "ya existe un usuario con el email provisto"


    #El usuario no existe por lo tanto lo creamos y obtenemos
    #el nuevo user_id y el token de registración
    profile, registration_token = \
    BasicUserProfile.create(
        email,
        role
    )
    
    profile_id = profile.profile_id

    team = Team.query.get(team_id)

    if role == 'responsable':
        link_reason = 'link_responsable_to_team'
        message = Message().message__link_non_existant_responsable_user_profile_to_team
        team.responsable_profile_id = profile.profile_id
        team.save()
    elif role == 'player':
        link_reason = 'link_player_to_team'
        message = Message().message__link_non_existant_player_user_profile_to_team
        assoc = TeamPlayer()
        assoc.team_id       = team.team_id
        assoc.profile_id    = profile.profile_id
        assoc.tournament_id = team.tournament_id
        assoc.created_by    = request_profile_id
        assoc.is_active     = True
        assoc.save()



    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = requester.role,
        petitioner_profile_id = request_profile_id,
        receiver_type         = role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'team',
        link_target_id        = team_id,
    )


    #Enviamos un mensaje al nuevo responsable/player para que se loguee
    message(
        requester,
        profile,
        team,
        registration_token,
        link_hash,
    )

    return profile.user.user_id, profile.profile_id

def create_profile(user_id, team_id, role, request_profile_id):
    requester = Profile.query.get(request_profile_id)
    user = User.query.get(user_id)

    #El profile no existe: lo creamos
    profile = Profile()
    profile.user_id = user.user_id
    profile.role = role
    profile.is_active = True
    profile.created_at = datetime.datetime.now()
    profile.save()

    team = Team.query.get(team_id)
    assert team

    if role == 'responsable':
        link_reason = 'link_responsable_to_team'
        message = Message().message__link_non_existant_responsable_profile_to_team
        team.responsable_profile_id = profile.profile_id
        team.save()
    elif role == 'player':
        link_reason = 'link_player_to_team'
        message = Message().message__link_non_existant_player_profile_to_team
        assoc = TeamPlayer()
        assoc.team_id       = team.team_id
        assoc.profile_id    = profile.profile_id
        assoc.tournament_id = team.tournament_id
        assoc.created_by    = request_profile_id
        assoc.is_active     = True
        assoc.save()

    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = requester.role,
        petitioner_profile_id = request_profile_id,
        receiver_type         = role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'team',
        link_target_id        = team_id,
    )

    message(
        requester,
        profile,
        team,
        link_hash,
        )
    return profile.profile_id

def assign_profile(profile_id, team_id, role, request_profile_id):
    requester = Profile.query.get(request_profile_id)
    profile = Profile.query.get(profile_id)
    assert profile.role == role

    team = Team.query.get(team_id)
    assert team

    if role == 'responsable':
        link_reason = 'link_responsable_to_team'
        message = Message().message__link_responsable_to_team
        team.responsable_profile_id = profile.profile_id
        team.save()
    elif role == 'player':
        link_reason = 'link_player_to_team'
        message = Message().message__link_player_to_team
        assoc = TeamPlayer()
        assoc.team_id       = team.team_id
        assoc.profile_id    = profile.profile_id
        assoc.tournament_id = team.tournament_id
        assoc.created_by    = request_profile_id
        assoc.is_active     = True
        assoc.save()

    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = requester.role,
        petitioner_profile_id = request_profile_id,
        receiver_type         = role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'team',
        link_target_id        = team_id,
    )

    message(
        requester,
        profile,
        team,
        link_hash,
        )
    return profile.profile_id