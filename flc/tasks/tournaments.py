# -*- coding: utf-8 -*-
from flc.models import *
import datetime

def create_user(email, tournament_id, role, request_profile_id):
    requester = Organizer.qget(request_profile_id)
    if not requester: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este torneo'})
    email = email.strip().lower()
    user = User.query.filter_by(email=email).first()
    assert not user, "ya existe un usuario con el email provisto"


    #El usuario no existe por lo tanto lo creamos y obtenemos
    #el nuevo user_id y el token de registración
    profile, registration_token = \
    BasicUserProfile.create(
        email,
        role
    )
    
    profile_id = profile.profile_id

    tournament = Tournament.query.get(tournament_id)
    if not tournament: abort(400, {'message': u'el torneo no existe'})

    if role == 'referee':
        link_reason = 'link_referee_to_tournament'
        message = Message().message__link_non_existant_referee_user_profile_to_tournament
        assoc = TournamentReferee()
    elif role == 'observer':
        link_reason = 'link_observer_to_tournament'
        message = Message().message__link_non_existant_observer_user_profile_to_tournament
        assoc = TournamentObserver()

    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = 'organizer',
        petitioner_profile_id = request_profile_id,
        receiver_type         = role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'tournament',
        link_target_id        = tournament.tournament_id,
    )

    assoc.tournament_id = tournament.tournament_id
    assoc.profile_id    = profile.profile_id
    assoc.created_at    = datetime.datetime.now()
    assoc.created_by    = request_profile_id
    assoc.save()

    #Enviamos un mensaje al nuevo usuario para que ingrese a GL
    message(
        requester,
        profile,
        tournament,
        registration_token,
        link_hash,
    )

    msg = u"{0}\nHemos enviado una solicitud a {1} para registrarse en la plataforma y gestionar al equipo {2}".format(
        u"El equipo se ha creado satisfactoriamente.",
        email,
        tournament.name
    )
    return profile.user.user_id, profile.profile_id

def create_profile(user_id, tournament_id, role, request_profile_id):
    requester = Organizer.qget(request_profile_id)
    if not requester: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este torneo'})
    user = User.query.get(user_id)
    if user.get_profile(role): raise Exception('el usuario ya tiene un perfil de {}'.format(role))

    #El profile no existe: lo creamos
    profile = Profile()
    profile.user_id = user.user_id
    profile.role = role
    profile.is_active = True
    profile.created_at = datetime.datetime.now()
    profile.first_name = user.first_name
    profile.last_name = user.last_name
    profile.save()

    tournament = Tournament.query.get(tournament_id)
    if not tournament: abort(400, {'message': u'el torneo no existe'})

    if role == 'referee':
        link_reason = 'link_referee_to_tournament'
        message = Message().message__link_non_existant_referee_profile_to_tournament
        assoc = TournamentReferee()
    elif role == 'observer':
        link_reason = 'link_observer_to_tournament'
        message = Message().message__link_non_existant_observer_profile_to_tournament
        assoc = TournamentObserver()

    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = 'organizer',
        petitioner_profile_id = request_profile_id,
        receiver_type         = role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'tournament',
        link_target_id        = tournament.tournament_id,
    )

    assoc.tournament_id = tournament.tournament_id
    assoc.profile_id    = profile.profile_id
    assoc.created_at    = datetime.datetime.now()
    assoc.created_by    = request_profile_id
    assoc.save()

    message(
        requester,
        profile,
        tournament,
        link_hash,
        )
    msg = u"Hemos enviado a {0} una solicitud para dirigir los partidos del torneo {1}".format(
        profile.user.name,
        tournament.name
        )
    return profile.profile_id

def link_profile(profile_id, tournament_id, request_profile_id):
    requester = Organizer.qget(request_profile_id)
    if not requester: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este torneo'})
    profile = Profile.query.get(profile_id)
    if not profile: raise Exception('el perfil no existe')

    tournament = Tournament.query.get(tournament_id)
    if not tournament: abort(400, {'message': u'el torneo no existe'})

    if profile.role == 'referee':
        link_reason = 'link_referee_to_tournament'
        message = Message().message__link_referee_to_tournament
        assoc = TournamentReferee()
    elif profile.role == 'observer':
        link_reason = 'link_observer_to_tournament'
        message = Message().message__link_observer_to_tournament
        assoc = TournamentObserver()

    link_hash = Linkage().link(
        link_reason           = link_reason,
        petitioner_type       = 'organizer',
        petitioner_profile_id = request_profile_id,
        receiver_type         = profile.role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'tournament',
        link_target_id        = tournament.tournament_id,
    )

    assoc.tournament_id = tournament.tournament_id
    assoc.profile_id    = profile.profile_id
    assoc.created_at    = datetime.datetime.now()
    assoc.created_by    = request_profile_id
    assoc.save()

    message(
        requester,
        profile,
        tournament,
        link_hash,
        )
    return profile.profile_id