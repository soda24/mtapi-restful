# -*- coding: utf-8 -*-
from flc.models import *
import datetime

def set_resource(profile_id, match_id, role, request_profile_id):
    requester = Organizer.qget(request_profile_id)
    now = datetime.datetime.now()
    match = Match.query.get(match_id)
    tournament = match.tournament
    if not requester: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este torneo'})

    profile = Profile.query.get(profile_id)
    if not profile: abort(400, {'message': u'no existe el perfil de {} provisto'.format(role)})
    if not profile.role == role: abort(400, {'message': u'el id de perfil provisto no pertenece a un rol de {}'.format(role)})

    #Update Match
    if role == 'observer':
        connection = MatchObserver()
        msg = Message().message__link_observer_to_match
    elif role == 'referee':
        connection = MatchReferee()
        msg = Message().message__link_referee_to_match
    else:
        abort(400, {'message': u'rol {} de perfil no contemplado'.format(role)})

    connection.profile_id = profile.profile_id
    connection.match_id = match.match_id
    connection.link = 'p'
    connection.role = role
    connection.created_by = requester.profile_id

    connection.save()

    #Create a link property
    link_hash = Linkage().link(
        link_reason           = 'link_{}_to_match'.format(role),
        petitioner_type       = requester.role,
        petitioner_profile_id = requester.profile_id,
        receiver_type         = profile.role,
        receiver_profile_id   = profile.profile_id,
        link_object           = 'profile',
        link_object_id        = profile.profile_id,
        link_target           = 'match',
        link_target_id        = match_id,
    )

    msg = msg(
        requester,
        profile,
        match,
        match.home_team,
        match.away_team,
        link_hash,
    )
