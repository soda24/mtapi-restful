import os
import uuid

# Add the current directory to the python path
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECURITY_PASSWORD_HASH = 'sha512_crypt'
SECURITY_PASSWORD_SALT = 'nTZY6asyMbVE7W'
SECURITY_REMEMBER_SALT = 'fbGt4uZmjkR3wb'
SECURITY_RESET_SALT = '4kCkuZ7mcDuuQF'
SECURITY_RESET_WITHIN = '5 days'
SECURITY_CONFIRM_WITHIN = '5 days'
SECURITY_SEND_REGISTER_EMAIL = False
SECURITY_FLASH_MESSAGES = True
SECRET_KEY = 'sNKxrYctLC7Jt4'

#EMAIL
MAIL_SERVER = 'granliga_dev'
SYSTEM_UUID = uuid.UUID('15e70b5a-aed2-4582-82e5-be726b8457c5')
SYSTEM_EMAIL = 'noreply@granliga.com'
SYSTEM_NAME = 'GranLiga.com'
MAIL_CATCH_ALL = True
SEND_MAIL_ENABLED = True
FORCED_MAIL = (u'GranLiga.com', 'noreply@granliga.com')
ADMINS = ['noreply@granliga.com']

MYSQL_HOST = u'granliga_dev' #new instance
MYSQL_USER = u'gl'
MYSQL_PASSWORD = u'root' #new instance
MYSQL_DB = u'granliga'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}:3306/{}?charset=utf8&use_unicode=0'.format(MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_DB)

#TEMPLATES
DOMAIN_NAME = '127.0.0.1:5001'
URI_SERVICE = 'http'


DEBUG = True

print "DEBUG mode: {}".format(repr(DEBUG))


# Restrict the extensions allowed for the uploaded files, we do other checks
# on the views, but this is the first level
ALLOWED_EXTENSIONS = [
    'cer', 'crt', 'pfx', 'key', 'pem', 'arm', 'crt',  # SSL files
]

# Select the default version of the API, this will load specific parts of your
# logic in the app.
DEFAULT_API = 'v1'

# Site domain, you usually want this to be your frontend url. This is used for
# login verification between other things
SITE_DOMAIN = 'http://www.granliga.com'
LOGIN_URL = 'https://www.granliga.com/login'

# How long the user session will last (in hours). Default: 168 (7 days)
SESSION_EXPIRES = 168

# Service tokens, this are usually the "client secret" or private API keys
# that you need to finish the OAuth validation. Remember NOT to commit back
# this values! They should remain known to you only!
GITHUB_SECRET = ''
FACEBOOK_SECRET = ''
LINKEDIN_SECRET = ''
GOOGLEPLUS_SECRET = ''
TWITTER_KEY = ''
TWITTER_SECRET = ''  # Twitter consumer secret
TWITTER_CALLBACK_URI = ''

# Main server token Make it unique and keep it away from strangers! This token
# is used in authentication and part of the storage encryption. This token
# is an example. **You MUST replace it!**
# SECRET = '-&3whmt0f&h#zvyc@yk4bs3g6biu9l&a%0l=5u*q2+rz(sypdk'
SECRET = 'sNKxrYctLC7Jt4'


# Logging settings. This is a standard python logging configuration. The levels
# are supposed to change depending on the settings file, to avoid clogging the
# logs with useless information.
LOGFILE = 'mtapi-falcon.log'
LOG_CONFIG = {
    "version": 1,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(filename)s->%(funcName)s:%(lineno)s] %(message)s",
            'datefmt': "%Y/%m/%d %H:%M:%S"
        },
    },
    'handlers': {
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, LOGFILE),
            'maxBytes': 2097152,  # 2MB per file
            'backupCount': 2,  # Store up to three files
            'formatter': 'standard',
        },
    },
    'loggers': {
        'sikr': {
            'handlers': ["logfile", ],
            'level': 'DEBUG',
        },
    }
}

ENDPOINT = '/api/v1'


#CELERY

REDIS_HOST = u'granliga_dev'
REDIS_PORT = u'6379'
REDIS_PASSWORD = u'v8mrHMXULCNUXmWG3yHBvgdrE'
CACHE_REDIS_HOST = REDIS_HOST
CACHE_REDIS_PORT = REDIS_PORT
CACHE_REDIS_PASSWORD = REDIS_PASSWORD
CACHE_REDIS_DB = 3
KVSESSION_REDIS_DB = 2
CELERY_BROKER_URL = 'redis://{}:{}/1'.format(REDIS_HOST, REDIS_PORT)
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_ENABLE_UTC = True
CELERY_ACCEPT_CONTENT=['json']

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
