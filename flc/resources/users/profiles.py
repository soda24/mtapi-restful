# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse

from flc.models import *
from flc.core import db

import traceback 
import sys

class Resource:

    def __init__(self, db):
        self.logger = logging.getLogger('user_profiles.' + __name__)

class ProfileCollection(restful.Resource):
    def __init__(self):
        self.reqparse = restful.reqparse.RequestParser()
        self.reqparse.add_argument('id', type = int, required = True,
            help = 'No profile id provided', location = 'json')
        super(ProfileCollection, self).__init__()

    def get(self):
        return db.session.query(Profile).all()


class ProfileResource(restful.Resource):

    def get(self, profile_id):
        return db.session.query(Profile).get(profile_id)

