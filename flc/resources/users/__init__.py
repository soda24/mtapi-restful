# -*- coding: utf-8 -*-

from .users import *
from .user_profiles import *
from .profiles import *
from .registration import *

resources = [
    ('users/users', UserCollection, "users.list_users"),
    ('users/users/<int:user_id>', UserResource, "users.show_user"),

    ('users/user_profiles', UserProfileCollection, "users.list_users_profiles"),
    ('users/user_profiles/<int:profile_id>', UserProfileResource, "users.show_user"),
    
    ('users/user_profiles/login/<id_or_email>', LoginResource, "users.login_user"),
    ('users/login', Login2Resource, "users.login_user2"),
    ('users/token', TokenResource, "users.get_token"),
    
    ('users/roles', RoleCollection, "users.list_roles"),
    ('users/roles/<int:role_id>', RoleResource, "users.get_role"),
    
    ('users/profiles', ProfileCollection, "users.list_profiles"),
    ('users/profiles/<int:profile_id>', ProfileResource, "users.get_profile"),

    ('users/registration/<token>', RegistrationResource, "users.registration"),
    ('users/registration/<token>/accept', PreRegistrationValidationResource, "users.registration_validation"),

]