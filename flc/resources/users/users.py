# -*- coding: utf-8 -*-
import logging

from flask import url_for, request, g
from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flask.ext.httpauth import HTTPBasicAuth

from flc.resources import Resource

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()

class UserCollection(Resource):
    def __init__(self):
        self.reqparse = restful.reqparse.RequestParser()
        self.reqparse.add_argument('email', type = str, required = True,
            help = 'No user id provided', location = 'json')
        self.reqparse.add_argument('password', type = str, required = True, help='no password provided', location = 'json')
        super(UserCollection, self).__init__()

    def get(self):
        self.parser.add_argument('profile_ids', type=str, location='args')
        self.parser.add_argument('ids', type=str, location='args')
        self.parser.add_argument('tournament_ids', type=str, location='args')
        q = self.parser.parse_args()
        if not request.args:
            return g.profile
        query = User.query
        qfilter = []
        user_ids = []
        response = dict(data=[])
        if q.ids:
            ids = map(int, q.ids.strip().split(','))
            user_ids += ids
        if q.profile_ids:
            profile_ids = map(int, q.profile_ids.strip().split(','))
            rows = Profile.query.filter(Profile.id.in_(profile_ids)).all()
            map_ids = dict([[r.profile_id, r.user_id] for r in rows])
            if rows:
                user_ids += map_ids.values()
                response['map'] = map_ids
        if not q.profile_ids and not q.ids:
            return dict('full userlist not allowed: filter by id or profile_id'), 403
        query = query.filter(User.id.in_(user_ids))
        response['data'] = query.all()
        return response

    def post(self):
        email = request.json.get('email')
        password = request.json.get('password')
        if email is None or password is None:
            abort(400, **{"message": "missing argument email or password", "status": 400}) # existing user
        if User.query.filter_by(email = email).first() is not None:
            abort(400, **{"message": "user already exists", "status": 400}) # existing user
        user = User(email = email)
        user.hash_password(password)
        user.is_active = True
        db.session.add(user)
        db.session.commit()
        return dict({ 'email': user.email, 'user_id': user.id }), 201, {'Location': url_for('userresource', user_id = user.id, _external = True)}

class UserResource(Resource):

    def get(self, user_id):
        user = User.query.filter_by(id = user_id).first()
        if user:
            return user
        else:
            return abort(400, **{'message': 'user does\'t exists', 'status': 400})

    def delete(self, user_id):
        user = User.query.filter_by(user_id = user_id).first()
        if user:
            return user
        else:
            return abort(400, **{'message': 'user does\'t exists', 'status': 400})

    def put(self, user_id):
        q = self.parser.parse_args()
        user = User.query.get(user_id)
        user.set_global(**request.json)
        user.save()
        return dict(message=u'el usuario fue actualizado con éxito', status='ok')

class RoleCollection(Resource):

    def get(self):
        return [{'hello': 'world'}]

class RoleResource(Resource):

    def get(self, role_id):
        return {'hello': 'world'}
