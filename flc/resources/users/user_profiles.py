# -*- coding: utf-8 -*-
import logging
from flask import request, g, url_for
from flask.ext import restful
from flask.ext.restful import reqparse

from flc.models import *
from flc.resources import Resource
from flc.core import db
import traceback 
import sys

from sqlalchemy import or_


class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()


class TokenResource(Resource):

    def get(self):
        token = g.user.generate_auth_token()
        print token
        print token.decode('ascii')
        return dict(token=token.decode('ascii'))

    def post(self):
        pass

class UserProfileCollection(Resource):
    def __init__(self):
        self.reqparse = restful.reqparse.RequestParser()
        self.reqparse.add_argument('id', type = int, required = True,
            help = 'No user id provided', location = 'json')
        self.reqparse.add_argument('email', type = str, default = "", location = 'json')
        super(UserCollection, self).__init__()

    def get(self):
        return db.session.query(User).all()


class UserProfileResource(Resource):

    def get(self, profile_id, **kwargs):
        profile = Profile.query.get(profile_id)
        assert profile, "No existe un perfil con ID: {}".format(profile_id)
        assert profile.user, "FATAL: Problemas con el usuario asociado al perfil ID: {}".format(profile_id)
        if 'info' in kwargs:
            return profile.info()
        profiles = profile.user.profiles.all()
        user = profile.user.to_json()
        user['profiles'] = profiles
        user['profile_id'] = profiles[0].id
        user['role'] = profiles[0].role
        return dict(user_profile=user), 200

class LoginResource(restful.Resource):
    def __init__(self):
        super(LoginResource, self).__init__()

    def get(self, id_or_email):
        try:
            dbuser = User.query.filter(or_(User.email==id_or_email.lower(), User.id==id_or_email.lower())).first()

            assert dbuser, "El usuario {} no existe".format(id_or_email.lower())
            profiles = dbuser.profiles.all()
            user = dbuser.to_json()
            user['profiles'] = profiles
            user['profile_id'] = profiles[0].id
            user['role'] = profiles[0].role
            user['password'] = dbuser.password
            return dict(user_profile=user), 200
        except Exception as error:
            message = repr(error)
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message=message), 403

    def post(self):
        try:
            verified = verify_password(request.authorization['username'], request.authorization['password'])
        except Exception as error:
            message = repr(error)
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message=message), 403

class Login2Resource(restful.Resource):

    def post(self):
        try:
            username_or_token = request.json.get('username')
            password = request.json.get('password')
            role = request.json.get('role')
            profile = Profile.verify_auth_token(username_or_token)
            if not profile:
                # try to authenticate with username/password
                user = User.query.filter_by(email = username_or_token).first()
                # user.hash_password(password)
                # print username_or_token, password, user.verify_password(password)
                if not user or not user.verify_password(password):
                    return dict(data="Not authorized"), 403
                else:
                    profile = user.get_profile(role)
                    return dict(data=dict(token=profile.generate_auth_token(), user=profile.info()))
            g.profile = profile
            return dict(data=dict(token=username_or_token, user=profile.info()))
        except Exception as error:
            message = repr(error)
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message=message), 403
