from flask import request, g, url_for, make_response, jsonify
from flask.ext import restful
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.restful import reqparse

import simplejson as json

from flc.models import User, Profile
from flc import settings


auth = HTTPBasicAuth()

@auth.error_handler
def auth_error():
    r = make_response(json.dumps({"data": "Authorization required"}), 403)
    r.mimetype = "application/json"
    return r

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    # print 'username_or_token, password', username_or_token, password, request.headers
    profile = Profile.verify_auth_token(username_or_token)
    print 'token->', username_or_token
    print 'user-->', profile
    if not profile:
        # try to authenticate with username/password
        user = User.query.filter_by(email = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.profile = profile
    return True

if not settings.LOCAL_INSTANCE:
	decorators = [auth.login_required]
else:
	decorators = []

class Resource(restful.Resource):
    decorators = decorators
    pass

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('User-Identifier', type=int, location='headers', dest="ident")
