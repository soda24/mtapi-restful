# -*- coding: utf-8 -*-

from .linkages import *

resources = [
    ('linkages/linkages', LinkageCollection),
    ('linkages/linkages/<link_hash>', LinkageResource),

    ('linkages/linkages/<link_hash>/accept', LinkageAcceptVerb),
    ('linkages/linkages/<link_hash>/reject', LinkageRejectVerb),
]
