# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import abort
from flask import request, url_for

from sqlalchemy import func, or_

from flc.models import *
from flc.core import db, limits
from flc.resources import Resource

import traceback 
import sys
import datetime

from flc import settings
from flc.tasks import teams as task_teams

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('linkages.' + __name__)
        super(Resource, self).__init__()


class LinkageCollection(Resource):

    def get(self, link_hash):
        pass

    def post(self, link_hash):
        pass

class LinkageResource(Resource):

    def get(self, link_hash):
        try:
            q = self.parser.parse_args()
            #Linking
            link_hash = base64.b64decode(link_hash)
            now = datetime.datetime.now()
            linkage = Linkage.query.filter_by(link_hash=link_hash).first()
            assert not linkage.expiration_date or linkage.expiration_date > now, \
            "El link ha expirado"
            assert linkage.is_link_accepted == None, "El link ya ha sido utilizado"
            return dict(status="ok", link=linkage)
        except Exception as error:
            if not linkage.expiration_date > now:
                linkage.link_status = 'expired'
                linkage.link_response = 'El link ha expirado'
                linkage.is_link_accepted = False
                linkage.save()
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(status="error", error=error.message, message=error.message), 403


    def post(self, link_hash):
        pass

class LinkageAcceptVerb(Resource):

    def post(self, link_hash):
        try:
            q = self.parser.parse_args()
            link_hash = base64.b64decode(link_hash)
            linkage = Linkage.query.filter_by(link_hash=link_hash).first()
            func = getattr(linkage, 'accept_%s' % linkage.link_reason)
            print 'accept_%s' % linkage.link_reason
            message = func(q.ident, link_hash)
            # assert linkage.receiver_profile_id == uuid.UUID(ident)
            return dict(status="ok", message=message)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(status="error", error=error.message, message=error.message), 403
        # return dict(users=users[0]['responsable_user_id'], default=APIEncoder)

class LinkageRejectVerb(Resource):

    def post(link_hash):
        try:
            q = self.parser.parse_args()
            #Linking
            link_hash = base64.b64decode(link_hash)
            linkage = Linkage.query.filter_by(link_hash=link_hash).first()
            func = getattr(linkage, 'reject_%s' % linkage.link_reason)
            print 'reject_%s' % linkage.link_reason
            message = func(q.ident, link_hash)
            # assert linkage.receiver_profile_id == uuid.UUID(ident)
            return dict(status="ok", message=message)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            error_message = error.message
            return dict(status="error", error=error_message, message=error.message), 403
        # return dict(users=users[0]['responsable_user_id'], default=APIEncoder)