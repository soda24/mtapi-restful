# -*- coding: utf-8 -*-
import logging

from flask import request, g

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource

from flc.utils.events import analyze

from sqlalchemy import func
from sqlalchemy.orm import aliased

from flc.tasks import matches as tasks
import flc.backend.tasks

import traceback 
import sys
import datetime

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()



class MatchCollection(Resource):

    def get(self):
        self.parser.add_argument('observation_status', type=str, location='args')
        q = self.parser.parse_args()
        try:
            if q.observation_status:
                team1 = aliased(Team)
                team2 = aliased(Team)
                rows = db.session.query(
                    Match,
                    team1,
                    team2,
                ).join(
                    MatchObserver
                ).join(
                    team1, team1.id==Match.home_team_id
                ).join(
                    team2, team2.id==Match.away_team_id
                ).filter(
                    Match.observation_status==q.observation_status
                ).filter(
                    MatchObserver.profile_id==g.profile.id
                ).all()
                matches = []
                for r in rows:
                    match = r[0].to_json()
                    match['home_team'] = {'name': r[1].name, 'logo_path': r[1].logo_path}
                    match['away_team'] = {'name': r[2].name, 'logo_path': r[2].logo_path}
                    match['name'] = u'{} vs {}'.format(r[1].name, r[2].name)
                    matches.append(match)
                return dict(
                    data=matches
                    )
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message=error.message, error=error.message), 500


class MatchResource(Resource):

    def get(self, match_id):
        try:
            print "----", match_id
            qmatch = Match.query.get(match_id)
            match = qmatch.to_json()
            match['name'] = u'{} vs {}'.format(qmatch.home_team.name, qmatch.away_team.name)
            if request.args.has_key('metadata'):
                match['home_team_meta'] = match['home_team_id'] and Team.query.get(match['home_team_id']).info()
                match['away_team_meta'] = match['away_team_id'] and Team.query.get(match['away_team_id']).info()

                if qmatch.referees:
                    match['referees'] = []
                    for referee in qmatch.referees:
                        match['referees'].append(referee.info())
                if qmatch.observers:
                    match['observers'] = []
                    for observer in qmatch.observers:
                        match['observers'].append(observer.info())
                if qmatch.sportcenter:
                    match['sportcenter'] = qmatch.sportcenter.info()
            return dict(data=match, message="ok")
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message=error.message, error=error.message), 500


    # def get(self, match_id):
    #     try:
    #         qmatch = Match.query.get(match_id)
    #         return dict(data=qmatch, message="ok")
    #     except Exception as error:
    #         err, detail, tb = sys.exc_info()
    #         print(traceback.format_exc(tb))
    #         return dict(message=error.message, error=error.message), 500


class MatchRefereeCollection(Resource):

    def get(self, match_id):
        return dict(data=MatchReferee.list_info(match_id))

    def post(self, match_id):
        q = self.parser.parse_args()
        profile_ids = request.json.get('profile_ids')
        profile_id = request.json.get('profile_id')
        if profile_id:
            tasks.set_resource(profile_id, match_id, 'referee', q.ident)
        if profile_ids:
            for profile_id in profile_ids.split(','):
                tasks.set_resource(profile_id, match_id, 'referee', q.ident)
        return dict(status="ok", message=u"el/los referee(s) se ha(n) asigando con éxito")

class MatchObserverCollection(Resource):

    def get(self, match_id):
        return dict(data=MatchObserver.list_info(match_id))

    def post(self, match_id):
        q = self.parser.parse_args()
        profile_id = request.json.get('profile_id')
        tasks.set_resource(profile_id, match_id, 'observer', q.ident)
        return dict(status="ok", message=u"el observador se ha asigando con éxito")


class MatchPlayerCollection(Resource):

    def get(self, match_id):
        q = self.parser.parse_args()
        match = Match.query.get(match_id)
        players = match.players_info()
        return dict(data=players)


class MatchTitularCollection(Resource):

    def get(self, match_id):
        self.parser.add_argument('team_id', type=int, location='args')
        self.parser.add_argument('team', type=str, location='args')
        q = self.parser.parse_args()
        try:
            def get_player_info_and_set_titular_flag(player, titular, goalkeeper, captain, position):
                player = player._asdict()
                player['titular'] = titular
                player['match_id'] = match_id
                player['goalkeeper'] = goalkeeper
                player['captain'] = captain
                player['position'] = position and int(position)
                return player
            
            match = Match.query.get(match_id)
            items = request.args.get('verbose', '').split(',')
            team_id = q.team_id
            if not team_id and q.team:
                team_id = getattr(match, '{}_team_id'.format(q.team))
            if team_id:
                team = Team.query.get(team_id)
                events = [m for m in MatchEvent.query.filter_by(match_id=match_id, team_id=team_id).all()]

                positions = dict([(e.profile_id, e.position) for e in events if e.event_type == 1])
                titulars = dict([(e.profile_id, e) for e in events if e.event_type == 1])
                captains = dict([(e.profile_id, e) for e in events if e.event_type == 2])
                goalkeepers = dict([(e.profile_id, e) for e in events if e.event_type == 3])

                players = [
                    get_player_info_and_set_titular_flag(
                        p,
                        p.profile_id in titulars,
                        p.profile_id in goalkeepers,
                        p.profile_id in captains,
                        positions.get(p.profile_id, None),
                        )
                    for p in team.players_info
                ]
                return dict(data=players), 200
            match_events = match.events
            home_events = filter(lambda x: x.team_id == match.home_team_id, match_events)
            away_events = filter(lambda x: x.team_id == match.away_team_id, match_events)
            home_team_titulars = dict([(m.profile_id, m.extra) for m in home_events if m.event_type == 1])
            away_team_titulars = dict([(m.profile_id, m.extra) for m in away_events if m.event_type == 1])

            home_team_goalkeepers = [m.profile_id for m in home_events if m.event_type == 3]
            away_team_goalkeepers = [m.profile_id for m in away_events if m.event_type == 3]

            home_team_captains = [m.profile_id for m in home_events if m.event_type == 2]
            away_team_captains = [m.profile_id for m in away_events if m.event_type == 2]

            home_team_titulars_set = len(home_team_titulars) == match.tournament.max_players_in_field
            away_team_titulars_set = len(away_team_titulars) == match.tournament.max_players_in_field

            home_players = [
                get_player_info_and_set_titular_flag(
                    player,
                    player.profile_id in home_team_titulars,
                    player.profile_id in home_team_goalkeepers,
                    player.profile_id in home_team_captains,
                    home_team_titulars.get(player.profile_id, None)
                )
                for player in match.home_team.players_info
            ]
            away_players = [
                get_player_info_and_set_titular_flag(
                    player,
                    player.profile_id in away_team_titulars,
                    player.profile_id in away_team_goalkeepers,
                    player.profile_id in away_team_captains,
                    away_team_titulars.get(player.profile_id, None)
                )
                for player in match.away_team.players_info
            ]

            return dict(
                data = dict(
                    home_titular_list_complete=home_team_titulars_set,
                    away_titular_list_complete=away_team_titulars_set,
                    home_team=home_players,
                    away_team=away_players
                )
            )
        except:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(status="error", message="problemas "), 500

    def post(self, match_id):
        self.parser.add_argument('team_id', type=int, location='args')
        q = self.parser.parse_args()
        try:
            players = request.json.get('players')
            players_dict = dict([(p['profile_id'], p) for p in players])
            match = Match.query.get(match_id)
            
            team_id = q.team_id
            team = Team.query.get(team_id)
            if not any([True for p in match.observers if p.profile_id == q.ident]): abort(400, **{"error": "usuario no autorizado para hacer modificaciones en el equipo"})
            

            team_events = [e for e in match.events if e.team_id == team_id]            
            #Separando los tipos de eventos
            positions = dict([(m.profile_id, m.position) for m in team_events if m.event_type == 1])
            team_titulars = dict([(m.profile_id, m) for m in team_events if m.event_type == 1])
            team_goalkeeper = dict([(m.profile_id, m) for m in team_events if m.event_type == 3])
            team_captain = dict([(m.profile_id, m) for m in team_events if m.event_type == 2])
            
            add_players = [player['profile_id'] for player in players if player['titular']]
            add_captain = [player['profile_id'] for player in players if player.get('captain') and player['profile_id'] not in team_captain]
            add_goalkeeper = [player['profile_id'] for player in players if player.get('goalkeeper') and player['profile_id'] not in team_goalkeeper]

            remove_players = [team_titulars[profile_id] for profile_id in team_titulars if profile_id not in players_dict]
            remove_captain = [team_captain[profile_id] for profile_id in team_captain if profile_id not in players_dict or not players_dict[profile_id].get('captain') ]
            remove_goalkeeper = [team_goalkeeper[profile_id] for profile_id in team_goalkeeper if profile_id not in players_dict or not players_dict[profile_id].get('goalkeeper') ]

            #First removing events
            for e in remove_players:
                e.delete()
            for e in remove_captain:
                e.delete()
            for e in remove_goalkeeper:
                e.delete()

            #First removing titular players
            for profile_id in add_players:
                p = players_dict.get(profile_id)
                if profile_id not in team_titulars:
                    event = MatchEvent(is_initial_event=True)
                    event.match_id = match.match_id
                    event.event_type = 1
                    event.team_id = team_id
                    event.profile_id = p['profile_id']
                    event.user_id    = p['user_id']
                    event.observer_id = q.ident
                    event.tournament_id = match.tournament_id
                    event.half_time = 1
                    event.time = 0
                    event.position = p['position']
                    event.match_date = match.match_date
                    event.created_at = datetime.datetime.now()
                    event.updated_by = q.ident
                    event.status = 'c'
                    event.save()
                else:
                    event = team_titulars[p['profile_id']]
                    event = MatchEvent.query.get(event.id)
                    event.position = p['position']
                    event.updated_by = q.ident
                    event.save()

            if add_captain:
                profile_id = add_captain[0]
                profile = Profile.query.get(profile_id)
                event = MatchEvent(is_initial_event=True)
                event.match_id      = match.match_id
                event.event_type    = 2
                event.team_id       = team_id
                event.profile_id    = profile.profile_id
                event.user_id       = profile.user_id
                event.observer_id   = q.ident
                event.tournament_id = match.tournament_id
                event.half_time     = 1
                event.time          = 0
                event.match_date    = match.match_date
                event.updated_by    = q.ident
                event.status        = 'c'
                event.save()

            if add_goalkeeper:
                profile_id = add_goalkeeper[0]
                profile = Profile.query.get(profile_id)
                event = MatchEvent(is_initial_event=True)
                event.match_id      = match.match_id
                event.event_type    = 3
                event.team_id       = team_id
                event.profile_id    = profile.profile_id
                event.user_id       = profile.user_id
                event.observer_id   = q.ident
                event.tournament_id = match.tournament_id
                event.half_time     = 1
                event.time          = 0
                event.match_date    = match.match_date
                event.updated_by    = q.ident
                event.status        = 'c'
                event.save()


            return dict(success=True), 200
        except:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(status="error", message="problemas "), 500

class MatchEventCollection(Resource):

    def get(self, match_id):
        return dict(data=MatchEvent.query.filter_by(match_id=match_id).all())

    def put(self, match_id):
        q = self.parser.parse_args()
        match = Match.query.get(match_id)
        if g.profile.profile_id not in [p.profile_id for p in match.observers]:
            return dict(error=u'No tienes autorizacion para hacer modificaciones en este partido'), 401
        events = match.events.all()
        sync_events = request.json
        sync_events = filter(lambda x: x['match_id'] == match_id, sync_events)
        sync_events.sort(key=lambda x: [not x.get('is_initial_event'), int(x['half_time']), int(x['time'])])
        remote_event_ids = [e.get('id') or e.get('event_id') for e in sync_events]
        errors = analyze(sync_events)
        if errors:
            return dict(data=errors), 403
        for event in sync_events:
            if not (event.has_key('event_id') or event.has_key('id')):
                print "Hacer algo si el evento no tiene event_id"
                continue
            if event.get('status') == 'd':
                ev = MatchEvent.query.filter(
                    MatchEvent.id==(event.get('event_id') or event.get('id')),
                    MatchEvent.match_id==match_id,
                ).first()
                ev and db.session.delete(ev)
            elif event.get('status') == 'i':
                record = MatchEvent()
                record.set_global(**event)
                record.status = 'o'
                record.observer_id = g.profile.profile_id
                record.created_by = g.profile.profile_id
                record.updated_by = g.profile.profile_id
                record.match_date = match.match_date
                record.tournament_id = match.tournament_id
                db.session.add(record)
            else:
                print 'else', event.get('event_id'), event.get('status')
                pass
        match.events_synchronized_at = datetime.datetime.now()
        db.session.add(match)
        db.session.commit()
        return dict(message="ok", success=True)

    def post(self, match_id):
        self.parser.add_argument('action', type=str, location='args')
        q = self.parser.parse_args()
        match = Match.query.get(match_id)
        assert match, "No existe un partido con ID {}".format(match_id)

        if not g.profile.id in [p.profile_id for p in match.observers]: abort(400, **{"error": "usuario no autorizado para hacer modificaciones en el partido"})

        if q.action == 'finish':
            match_events = MatchEvents(match_id)
            match_events.observed_by = q.ident
            match_events.finish_observation()
            flc.backend.tasks.update_position_table(match.tournament_id)
            return dict(message="ok", success=True)

class MatchCallToPlayVerb(Resource):

    def post(self, match_id):
        self.parser.add_argument('action', type=str, location='args')
        q = self.parser.parse_args()

        try:
            me = Profile.query.get(q.ident)
            args = request.args.to_dict()
            team = Team.get(team_id=args.get('team_id'))
            match = FixtureMatch.get(match_id=match_id)
            against_team = match.local_team_id == team.team_id and match.visitant_team or match.local_team
            if team.responsable_profile_id != ident:
                return dict(status="error", message="No tienes permiso para llamar a los jugadores de este equipo"), 500
            ptms = PlayerTeamMatch.list_all(team.team_id, match.match_id)
            print "ARREGLAR ACA ESTO QUE ES POCO PERFORMANTE... COMO NECESITO el objeto PLAYER y no un diccionario... lo vuelvo a buscar"
            for ptm in ptms:
                if not ptm['match']['is_blocked']:
                    player = Player.get(profile_id=ptm['profile_id'])
                    link_id = Linkage().link(
                        **dict(
                            link_reason = 'link_player_assist_confirmation',
                            petitioner_type = 'responsable',
                            petitioner_profile_id = team.responsable_profile_id,
                            receiver_type = 'player',
                            receiver_profile_id = player.profile_id,
                            link_object = 'player',
                            link_object_id = player.profile_id,
                            link_target = 'match',
                            link_target_id = match.match_id,
                            link_extra_information = {'team_id': str(team.team_id)},
                        )
                    )

                    messsage_id = Message().message__player_assist_confirmation(
                        me,
                        player,
                        team,
                        against_team,
                        match,
                        link_id,
                    )
            return dict(status="ok", message="ok")
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(status="error", message="problems con el call_to_play", error=error.message), 500
