# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource
from flc.tasks import tournaments as tournament_tasks

from sqlalchemy import func

import traceback 
import sys

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()

class ObserverCollection(Resource):
    def __init__(self):
        self.reqparse = restful.reqparse.RequestParser()
        self.reqparse.add_argument('id', type = int, required = True,
            help = 'No profile id provided', location = 'json')
        super(ObserverCollection, self).__init__()

    def get(self):
        self.parser.add_argument('tournament_ids', type=str, location='args')
        q = self.parser.parse_args()
        if not q.tournament_ids: abort(400, {'message':"solo puedes ver los observers que pertenecen a un torneo", 'status': 'error'})
        tournament_ids = map(int, q.tournament_ids.strip().split(','))
        query = Profile.query.join(TournamentObserver, Profile.id==TournamentObserver.profile_id).filter(TournamentObserver.tournament_id.in_(tournament_ids))
        return dict(data=query.all())

    def post(self):
        self.parser.add_argument('email', type=str, required=True, help="email del responsable es obligatorio", location='json')
        self.parser.add_argument('tournament_id', type=int, required=True, help="email del responsable es obligatorio", location='json')
        q = self.parser.parse_args()
        try:
            now = datetime.datetime.now()

            if not q.ident: abort(400, {'message':"usuario no autenticado", 'status': 'error'})
            organizer = Organizer.query.get(q.ident)
            if not organizer.role == 'organizer': abort(403, {'message':"perfil incorrecto, debes ser organizador", 'status': 'error'})
            
            email = q.email.strip().lower()
            user = User.query.filter(func.lower(User.email)==email).first()
            if not user:
                res = tournament_tasks.create_user(email, q.tournament_id, 'observer', q.ident)
                if res:
                    user_id, profile_id = res
                profile = Observer.qget(profile_id)
                if not profile: abort(400, {'message':"hubo un problema creando el usuario/perfil", 'status': 'error'})
                user = profile.user
            elif not user.get_profile('observer'):
                profile_id = tournament_tasks.create_profile(user.user_id, q.tournament_id, 'observer', q.ident)
                profile = Profile.query.get(profile_id)
            else:
                profile = user.get_profile('observer')
            return dict(data=profile, status=200, message=u"observer asignado al torneo con éxito")
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403


class ObserverResource(Resource):

    def get(self, profile_id):
        return Profile.query.filter_by(profile_id=profile_id, role='observer')


class ObserverTournamentCollection(Resource):

    def get(self, profile_id):
        tournaments = db.session.query(Tournament).join(TournamentObserver).filter(TournamentObserver.profile_id==profile_id).all()
        return dict(data=tournaments)

class ObserverNextMatchResource(Resource):

    def get(self, profile_id):
        try:
            match = db.session.query(Match).join(MatchObserver).filter(MatchObserver.profile_id==profile_id, MatchObserver.is_link_accepted==True).order_by('match_date').first()
            return dict(data=match)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403