# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource
from flc.tasks import tournaments as tournament_tasks

from sqlalchemy import func

import traceback 
import sys

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()

class ResponsableNextMatchResource(Resource):

    def get(self, profile_id):
        try:
            my_team_ids = [t.id for t in Team.query.filter_by(responsable_profile_id=profile_id, is_link_accepted=True).all()]
            match = db.session.query(Match).filter(Match.home_team_id.in_(my_team_ids), Match.status.in_(['p', 'i'])).order_by('match_date').first()
            return dict(data=match)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403