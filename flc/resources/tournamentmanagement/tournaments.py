# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse, abort
from flask import request

from sqlalchemy import func

from flc.models import *
from flc.core import db, limits
from flc.resources import Resource

import traceback 
import sys
import datetime

def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()

def save_media(tournament_id, file, filename, ident):
    content = file.read()
    file.seek(0)
    checksum = hashfile(file, hashlib.sha256())
    
    m = magic.Magic()
    content_type = m.from_buffer(content[:1024])
    content = bytearray(content)
    media_id = db.Tournament.store_media(tournament_id, {
        'filename': filename,
        'content_type': content_type,
        'content': content,
        'checksum': checksum,
        'created_by': ident,
        })
    return media_id

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()


class TournamentCollection(Resource):

    def get(self):
        self.parser.add_argument('profile_id', type=int, location='args')
        self.parser.add_argument('tournament_id', type=str, location='args')
        q = self.parser.parse_args()
        query = Tournament.query
        if q.ident:
            profile = db.session.query(Organizer).get(q.ident)
            if profile.role == 'organizer':
                query = query.filter_by(organizer_profile_id=q.ident)
        if q.tournament_id:
            ids = q.tournament_id.split(',')
            query = query.filter(Tournament.id.in_(ids))
        return dict(data=query.all())

    def post(self):
        try:
            now = datetime.datetime.now()
            tournament = Tournament()

            ident = int(request.headers.get('User-Identifier'))
            assert ident, "Usuario no autenticado"
            organizer = db.session.query(Organizer).get(ident)
            assert organizer.role == 'organizer', "Debes ser organizador para poder crear un torneo"

            post = request.json
            for key, value in post.items():
                if key == 'start_date':
                    if len(value) == 10:
                        value = datetime.datetime.strptime(value, '%Y-%m-%d')
                setattr(tournament, key, value)
            #Assertions - checks
            assert len(tournament.name) >= limits.MIN_TOURNAMENT_NAME_LENGHT, \
             "El nombre del torneo debe contener un mínimo de {} caracteres".format(
                limits.MIN_TOURNAMENT_NAME_LENGHT
            )

            tournament.organizer_profile_id = ident

            #Checking that a tournament doesn't already exists
            tournaments = db.session.query(Tournament).filter(func.lower(Tournament.name)).all()
            assert not tournaments, "El organizador no puede tener mas de un torneo con el mismo nombre"

            #Setting defaults
            tournament.is_active = True
            tournament.can_edit = True
            tournament.status = 'pending'
            tournament.created_by = ident
            tournament.created_at = now
            tournament.updated_by = ident
            tournament.last_updated_at = now
            db.session.add(tournament)
            db.session.commit()
            return dict(tournament=tournament)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403




class TournamentResource(Resource):

    def get(self, tournament_id, **kwargs):
        tournament = db.session.query(Tournament).get(tournament_id)
        assert tournament, "No existe un torneo con ID: {}".format(tournament_id)
        return dict(data=tournament), 200

    def put(self, tournament_id, *args, **kwargs):
        q = self.parser.parse_args()
        tournament = db.session.query(Tournament).get(tournament_id)
        assert tournament, "no existe un torneo con id: {}".format(tournament_id)
        assert tournament.organizer_profile_id == q.ident, "no tienes permiso para editar este torneo"
        put = request.json
        if tournament.is_setup_complete: abort(400, **{"error": "modificaciones no permitidas", "errors": {'other': 'este torneo no acepta mas modificaciones generales'}, "status": 400})
        if put['teams_amount'] != tournament.teams_amount and tournament.is_fixture_complete:
            abort(400, **{"error": "modificacion no permitida", "errors": {'teams_amount': 'fixture existente: no es posible cambiar la cantidad de equipos del torneo, pero es posible que puedas destruir el fixture'}, "status": 400})
        tournament.set_global(**put)
        db.session.add(tournament)
        db.session.commit()
        return dict(message="ok")


class TournamentStatusResource(Resource):

    def get(self, tournament_id):
        pass

class TournamentInitializationResource(Resource):

    def get(self, tournament_id):
        pass

class TournamentTermsCollection(Resource):

    def get(self, tournament_id):
        pass

class TournamentFairPlayResource(Resource):

    def get(self, tournament_id):
        fair = FairPlayTable.get(tournament_id=tournament_id, names=True)
        return dict(data=fair)

class TournamentScorerResource(Resource):

    def get(self, tournament_id):
        scorer_table = ScorerTable.get(tournament_id=tournament_id, names=True)
        return dict(data=scorer_table)

class TournamentPositionTableCollection(Resource):

    def get(self, tournament_id):
        round_id = 0
        # tournament = Tournament.get(tournament_id=tournament_id)
        position_table = TournamentPositionTable.get(tournament_id=tournament_id, vround=round_id)
        position_table = [row.to_json() for row in position_table]
        for row in position_table:
            row['team'] = Team.query.get(row['team_id']).info()
        return dict(data=position_table)


class TournamentPositionTableResource(Resource):

    def get(self, tournament_id, round_id):
        # tournament = Tournament.get(tournament_id=tournament_id)
        position_table = TournamentPositionTable.get(tournament_id=tournament_id, vround=round_id)
        position_table = [row.to_json() for row in position_table]
        for row in position_table:
            row['team'] = Team.query.get(row['team_id']).info()
            print row
        return dict(data=position_table)

class TournamentTeamCollection(Resource):

    def get(self, tournament_id):
        tournament = db.session.query(Tournament).get(tournament_id)
        assert tournament, "No existe ningun torneo con ID {}".format(tournament_id)
        tournament_teams = db.session.query(Team).filter(Team.tournament_id==tournament_id).all()
        return dict(data=tournament_teams)

class TournamentTeamResource(Resource):

    def get(self, tournament_id, team_id):
        pass

class TournamentRefereeCollection(Resource):

    def get(self, tournament_id):
        self.parser.add_argument('info', type=str, location='args')
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if q.info:
            query = db.session.query(
                Profile.id.label('profile_id'),
                Profile.is_active,
                Profile.role,
                User.id.label('user_id'),
                func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
                User.first_name,
                User.last_name,
                User.email,
                User.avatar_path,
                TournamentReferee.is_link_accepted
            ).join(User).join(TournamentReferee).filter(TournamentReferee.tournament_id==tournament_id)
            data = query.all()
        else:
            data = tournament.observers
        return dict(data=data)

class TournamentRefereeResource(Resource):

    def get(self, tournament_id):
        pass

    def delete(self, tournament_id, profile_id):
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if not tournament.organizer_profile_id == q.ident: abort(400, **{"error": "no tienes autorizacion para hacer modificaciones en este torneo", "status": 400}) # existing user
        referee = Referee.qget(profile_id)
        if not referee: abort(400, **{"error": "no existe un referee con ese id", "status": 400}) # existing user
        tr = TournamentReferee.query.filter_by(tournament_id=tournament_id, profile_id=profile_id).first()
        if not tr: abort(400, **{"error": "el referee no esta asociado a este torneo", "status": 400}) # existing user
        if not tournament.is_setup_complete:
            db.session.delete(tr)
            db.session.commit()
        else:
            tr.is_active = False
            db.session.add(tr)
            db.session.commit()
        return dict(status='ok', message='el referee fue desvinculado del torneo')


class TournamentObserverCollection(Resource):

    def get(self, tournament_id):
        self.parser.add_argument('info', type=str, location='args')
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if q.info:
            query = db.session.query(
                Profile.id.label('profile_id'),
                Profile.is_active,
                Profile.role,
                User.id.label('user_id'),
                func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
                User.first_name,
                User.last_name,
                User.email,
                User.avatar_path,
                TournamentObserver.is_link_accepted
            ).join(User).join(TournamentObserver).filter(TournamentObserver.tournament_id==tournament_id)
            data = query.all()
        else:
            data = tournament.observers
        return dict(data=data)

class TournamentObserverResource(Resource):

    def get(self, tournament_id):
        pass

    def delete(self, tournament_id, profile_id):
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if not tournament.organizer_profile_id == q.ident: abort(400, **{"error": "no tienes autorizacion para hacer modificaciones en este torneo", "status": 400}) # existing user
        observer = Observer.qget(profile_id)
        if not observer: abort(400, **{"error": "no existe un observer con ese id", "status": 400}) # existing user
        tr = TournamentObserver.query.filter_by(tournament_id=tournament_id, profile_id=profile_id).first()
        if not tr: abort(400, **{"error": "el observer no esta asociado a este torneo", "status": 400}) # existing user
        if not tournament.is_setup_complete:
            db.session.delete(tr)
            db.session.commit()
        else:
            tr.is_active = False
            db.session.add(tr)
            db.session.commit()
        return dict(status='ok', message='el observer fue desvinculado del torneo')


class TournamentSportcenterCollection(Resource):

    def get(self, tournament_id):
        tournament = db.session.query(Tournament).get(tournament_id)
        assert tournament, "No existe ningun torneo con ID {}".format(tournament_id)
        sportcenters = tournament.sportcenters
        return dict(data=sportcenters)

class TournamentSportcenterResource(Resource):

    def get(self, tournament_id):
        pass

    def delete(self, tournament_id, sportcenter_id):
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if not tournament.organizer_profile_id == q.ident: abort(400, **{"error": "no tienes autorizacion para hacer modificaciones en este torneo", "status": 400}) # existing user
        sportcenter = Sportcenter.query.get(sportcenter_id)
        if not sportcenter: abort(400, **{"error": "no existe un centro deportivo con ese id", "status": 400}) # existing user
        ts = TournamentSportcenter.query.filter_by(tournament_id=tournament_id, sportcenter_id=sportcenter_id).first()
        if not ts: abort(400, **{"error": "el centro deportivo no esta asociado a este torneo", "status": 400}) # existing user
        db.session.delete(ts)
        db.session.commit()
        return dict(status='ok', message='el centro deportivo fue desvinculado del torneo')

class TournamentFixtureResource(Resource):

    def get(self, tournament_id):
        rows = Match.query.filter_by(tournament_id=tournament_id).order_by(Match.round, Match.match).all()
        rounds = []
        matches = []
        rnd = 0
        mch = 0
        for row in rows:
            if rnd != row.round:
                # pprint(matches)
                rounds.append(matches)
                matches = []
            matches.append(dict(match=row.match, round=row.round, match_id=row.id, id=row.id, round_date=row.round_date, match_date=row.match_date, home_team_id=row.home_team_id, away_team_id=row.away_team_id))
            rnd = row.round
            mch = row.match
        rounds.append(matches)
        return dict(data=rows and rounds)

    def post(self, tournament_id):
        q = self.parser.parse_args()
        data = request.json
        tournament = Tournament.query.get(tournament_id)
        if not tournament.organizer_profile_id == q.ident: abort(400, **{"error": "no tienes autorizacion para hacer modificaciones en este torneo", "status": 400}) # existing user
        data['created_by'] = q.ident

        if tournament.is_setup_complete:
            abort(400, **{"error": "no se pueden hacer modificaciones del fixture, el torneo ya ha sido configurado", "status": 400}) # existing user
        fixture = Fixture.create(tournament_id, data)
        return dict(status='ok', message=u'el fixture se creo con éxito')

    def delete(self, tournament_id):
        q = self.parser.parse_args()
        tournament = Tournament.query.get(tournament_id)
        if not tournament.is_active: abort(400, **{"error": "no puedes modificar un torneo inactivo", "status": 400})
        if tournament.is_setup_complete: abort(400, **{"error": "no puedes eliminar el fixture debido a que la configuracion del torneo ha finalizado", "status": 400})
        if tournament.organizer_profile_id != q.ident: abort(400, **{"error": "no tienes autorizacion para hacer modificaciones en este torneo", "status": 400})
        db.session.query(PositionTable).filter_by(tournament_id=tournament_id).delete()
        db.session.query(Match).filter_by(tournament_id=tournament_id).delete()
        tournament.is_setup_complete = False
        tournament.is_fixture_complete = False
        tournament.save()
        return dict(status="ok", message="el fixture se ha eliminado con exito")


class Tournament_SponsorCollection(Resource):

    def get(self):
        pass

class Tournament_SponsorResource(Resource):

    def get(self):
        pass

def fixture_restruct(fixture, metadata_flag, indexed_flag, resources_metadata_flag=False):
    for n, match in enumerate(fixture):
        match = match.to_json()
        fixture[n] = match

    if metadata_flag:
        teams = {}
        for n, match in enumerate(fixture):
            if match['home_team_id'] in teams:
                team_one = teams.get(match['home_team_id'])
            else:
                team_one = Team.query.get(match['home_team_id'])
                teams[match['home_team_id']] = team_one

            if match['away_team_id'] in teams:
                team_two = teams.query.get(match['away_team_id'])
            else:
                team_two = Team.query.get(match['away_team_id'])
                teams[match['away_team_id']] = team_two
            match['home_team_meta'] = team_one
            match['away_team_meta'] = team_two
            fixture[n] = match

    if resources_metadata_flag:
        referees = {}
        observers = {}
        sportcenters = {}
        for n, match in enumerate(fixture):
            match['referees'] = []
            match['observer'] = None
            match['sportcenter'] = None
            # if match['observer_id']:
            #     if match['observer_id'] not in observers:
            #         observers[match['observer_id']] = observers[match['observer_id']] = Observer.get(profile_id=match['observer_id']).user_profile
            #     match['observer'] = observers.get(match['observer_id'])
            if match['sportcenter_id']:
                if match['sportcenter_id'] not in sportcenters:
                    sportcenter = Sportcenter.get(sportcenter_id=match['sportcenter_id']).info()
                    sportcenters[match['sportcenter_id']] = sportcenter
                match['sportcenter'] = sportcenters.get(match['sportcenter_id'])

            fixture[n] = match


    if indexed_flag:
        indexed = {}
        for match in sorted(fixture, key=lambda x: (x['tournament_id'], x['round'], x['match'])):
            if not match['tournament_id'] in indexed:
                indexed[match['tournament_id']] = {}
            if not match['round'] in indexed[match['tournament_id']]:
                indexed[match['tournament_id']][match['round']] = []
            indexed[match['tournament_id']][match['round']].append(match)

        indexed_list = []
        for il_t, (t_idx, rounds) in enumerate(sorted(indexed.items())):
            if il_t+1 > len(indexed_list):
                indexed_list.append([])
            for il_r, (r_idx, matches) in enumerate(sorted(rounds.items())):
                if il_r+1 > len(indexed_list[il_t]):
                    indexed_list[il_t].append([])
                for il_m, pending in enumerate(sorted(matches)):
                    indexed_list[il_t][il_r].append(pending)
        return indexed_list
    else:
        return fixture

class TournamentNextRoundResource(Resource):

    def get(self, tournament_id):
        try:
            vround = Fixture.get(tournament_id).get_next_round()
            if not vround:
                return dict(round=[], message=u"No se encontró información para la siguiente fecha del torneo"), 403
            vround = fixture_restruct(vround, True, True, True)
            return dict(round=vround[0])
        except Exception as error:
            print error
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(round=[], error=error.message, status="error"), 500

class TournamentRoundResource(Resource):

    def get(self, tournament_id, round_id):
        try:
            vround = FixtureRound.get(tournament_id, round_id)
            vround = fixture_restruct(vround, True, True, True)
            return dict(round=vround[0])
        except Exception as error:
            print error
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message, status="error"), 403


