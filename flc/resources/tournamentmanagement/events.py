# -*- coding: utf-8 -*-
import logging

from flask import request, json, g
from flask_login import current_user

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource

from sqlalchemy import func

import traceback 
import sys
import datetime

from flc.tasks import matches as tasks

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()



class EventCollection(Resource):

    def post(self):
        try:
            # import pdb; pdb.set_trace()
            self.parser.add_argument('event_type', type=str, location='json', required=True, help = 'event_type es obligatorio',)
            self.parser.add_argument('profile_id', type=int, location='json', required=True, help = 'profile_id es obligatorio',)
            self.parser.add_argument('team_id',    type=int, location='json', required=True, help = 'team_id es obligatorio',)
            self.parser.add_argument('match_id',   type=int, location='json', required=True, help = 'match_id es obligatorio',)
            self.parser.add_argument('half_time',  type=int, location='json', required=True, help = 'half_time es obligatorio',)
            self.parser.add_argument('time',       type=int, location='json', required=True, help = 'time es obligatorio',)
            self.parser.add_argument('ref_event_id', location='json', required=False, help="falla con rev_event_id anda a saber porque")
            q = self.parser.parse_args()
            now = datetime.datetime.now()            
            post = request.json

            match = Match.query.get(q.match_id)
            events = match.events.all()
            event_types = dict([[et.name, et.id] for et in MatchEventType.query.all()])
            event_type = event_types.get(q.event_type)
            if not event_type: abort(400, **{'error': u'event_type {} no es un tipo de evento válido'.format(q.event_type)})
            event = MatchEvent(
                match_id    = q.match_id,
                event_type  = event_type,
                time        = q.time,
                team_id     = q.team_id,
                profile_id  = q.profile_id,
                half_time   = q.half_time,
                position    = event_type == 4 and post.get('position') or None,
                created_by  = g.profile.id,
                updated_by  = g.profile.id,
                observer_id = g.profile.id,
                status      = 'i',
                ref_event_id = q.ref_event_id or None
                 )
            event.do_complete()
            try:
                event.check()
            except Exception as error:
                abort(400, **{'error': error.message})
            else:
                event.save()
                if event.ref_event_id:
                    ev2 = MatchEvent.query.get(event.ref_event_id)
                    if ev2:
                        ev2.ref_event_id = event.id
                        ev2.save()
            if q.event_type in ('goal', 'goal_against', 'yellow_card', 'red_card', 'blue_card', 'green_card'):
                #Increment metrics
                PlayerTeamMetric.increment(event)
                TeamMetric.increment(event)
                PlayerMetric.increment(event)
            return dict(status='ok', data=event)
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403

    def get(self):
        self.parser.add_argument('event_type', type=str, location='args', )
        self.parser.add_argument('profile_id', type=int, location='args', )
        self.parser.add_argument('team_id',    type=int, location='args', )
        self.parser.add_argument('match_id',   type=int, location='args', )
        self.parser.add_argument('half_time',  type=int, location='args', )
        self.parser.add_argument('time',       type=int, location='args', )
        self.parser.add_argument('ref_event_id', location='json', required=False, help="falla con rev_event_id anda a saber porque")
        q = self.parser.parse_args()
        query = db.session.query(MatchEvent)
        if q.event_type:
            query = query.filter_by(event_type=q.event_type)
        if q.match_id:
            query = query.filter_by(match_id=q.match_id)
        return dict(data=query.all())

class EventResource(Resource):

    def get(self):
        q = self.parser.parse_args()

    def delete(self, event_id):
        q = self.parser.parse_args()
        try:
            event = MatchEvent.query.get(event_id)
            event_type_label = event.event_type_label
            event.delete()
            if event_type_label in ('goal', 'goal_against', 'yellow_card', 'red_card', 'blue_card', 'green_card'):
                PlayerTeamMetric.decrement(event)
                TeamMetric.decrement(event)
                PlayerMetric.decrement(event)

            return dict(status=200, deleted=True, message='ok')
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403

class MatchTeamTitularEventColletion(Resource):

    def put(self, match_id, team_id):
        data = request.json
        match = Match.query.get(match_id)
        events = MatchEvent.query.filter_by(event_type=1, team_id=team_id, match_id=match_id).all()
        players = data.get('players')
        for ev in events:
            if ev.profile_id not in players:
                db.session.delete(ev)
            else:
                players.remove(ev.profile_id)
        for profile_id in players:
            player = Profile.query.get(profile_id)
            event = MatchEvent(
                event_type=1,
                team_id=team_id,
                is_initial_event=True,
                half_time=0,
                time=0,
                match_id=match.match_id,
                match_date=match.match_date,
                created_by=g.profile.id,
                created_at=datetime.datetime.now(),
                profile_id=profile_id,
                user_id=player.user.id,
                observer_id=g.profile.id,
                status='c',
                tournament_id=match.tournament_id
            )
            db.session.add(event)
        db.session.commit()
        return dict(data=dict(message="ok")), 200

    def get(self):
        pass