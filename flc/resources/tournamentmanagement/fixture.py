# -*- coding: utf-8 -*-
import logging

from flask import request

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource
from flc.tasks import tournaments as tournament_tasks

from sqlalchemy import func

import traceback 
import sys

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()


def fixture_restruct(fixture, metadata_flag, indexed_flag, resources_metadata_flag=False):
    for n, match in enumerate(fixture):
        match = match.to_json()
        fixture[n] = match

    if metadata_flag:
        teams = {}
        for n, match in enumerate(fixture):
            if match['home_team_id'] in teams:
                team_one = teams.get(match['home_team_id'])
            else:
                team_one = Team.get(team_id=match['home_team_id'])
                teams[match['home_team_id']] = team_one

            if match['away_team_id'] in teams:
                team_two = teams.get(match['away_team_id'])
            else:
                team_two = Team.get(team_id=match['away_team_id'])
                teams[match['away_team_id']] = team_two
            match['home_team_meta'] = team_one
            match['away_team_meta'] = team_two
            fixture[n] = match

    if resources_metadata_flag:
        referees = {}
        observers = {}
        sportcenters = {}
        for n, match in enumerate(fixture):
            match['referees'] = []
            match['observer'] = None
            match['sportcenter'] = None

            if match['referee_id']:
                for profile_id in match['referee_id']:
                    if profile_id in referees:
                        referee = referees.get(profile_id)
                    else:
                        referee = Referee.get(profile_id=profile_id).user_profile
                        referees[profile_id] = referee
                    match['referees'].append(referee)
            if match['observer_id']:
                if match['observer_id'] not in observers:
                    observers[match['observer_id']] = observers[match['observer_id']] = Observer.get(profile_id=match['observer_id']).user_profile
                match['observer'] = observers.get(match['observer_id'])
            if match['sportcenter_id']:
                if match['sportcenter_id'] not in sportcenters:
                    sportcenter = Sportcenter.get(sportcenter_id=match['sportcenter_id']).info()
                    sportcenters[match['sportcenter_id']] = sportcenter
                match['sportcenter'] = sportcenters.get(match['sportcenter_id'])

            fixture[n] = match


    if indexed_flag:
        indexed = {}
        for match in sorted(fixture, key=lambda x: (x['tournament_id'], x['round'], x['match'])):
            if not match['tournament_id'] in indexed:
                indexed[match['tournament_id']] = {}
            if not match['round'] in indexed[match['tournament_id']]:
                indexed[match['tournament_id']][match['round']] = []
            indexed[match['tournament_id']][match['round']].append(match)

        indexed_list = []
        for il_t, (t_idx, rounds) in enumerate(sorted(indexed.items())):
            if il_t+1 > len(indexed_list):
                indexed_list.append([])
            for il_r, (r_idx, matches) in enumerate(sorted(rounds.items())):
                if il_r+1 > len(indexed_list[il_t]):
                    indexed_list[il_t].append([])
                for il_m, pending in enumerate(sorted(matches)):
                    indexed_list[il_t][il_r].append(pending)
        return indexed_list
    else:
        return fixture


class FixtureCollection(Resource):

    def get(self):
        pass

class FixtureRoundStatusesResource(Resource):

    def get(self, tournament_id, round_id):
        try:
            statuses = FixtureRound.get(tournament_id=tournament_id, vround=round_id).round_status()
            return dict(statuses=statuses, message="ok")
        except:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message="La fecha no existe"), 500

class FixtureLastRoundInfoResource(Resource):

    def get(self, tournament_id):
        try:
            last_round = FixtureRound.get_last_round(tournament_id)
            return dict(data=last_round.info(), message="ok")
        except:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(message="no hay una ultima fecha"), 500