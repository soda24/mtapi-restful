# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource
from flc.tasks import tournaments as tournament_tasks

from sqlalchemy import func, or_, and_
from collections import Counter

import traceback 
import sys

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()

class PlayerResource(Resource):

    def get(self, profile_id):
        pass

    def put(self, profile_id):
        pass

class PlayerCollection(Resource):

    def get(self):
        self.parser.add_argument('match_id', type=int, location='args')
        self.parser.add_argument('team', type=str, location='args')
        q = self.parser.parse_args()
        if q.match_id and q.team:
            match = Match.query.get(q.match_id)
            team_id = getattr(match, '{}_team_id'.format(q.team))
            print team_id


    def put(self, profile_id):
        pass

class PlayerTeamCollection(Resource):

    def get(self, profile_id):
        teams = db.session.query(Team).join(TeamPlayer, TeamPlayer.team_id==Team.id).filter(TeamPlayer.profile_id==profile_id).all()
        return dict(data=teams, status="ok")

class PlayerTeamResource(Resource):

    def get(self, profile_id, team_id):
        pass

class MatchTeamPlayerInfoResource(Resource):

    def get(self, profile_id, team_id, match_id):
        data = MatchTeamPlayer.query.filter(
            MatchTeamPlayer.match_id==match_id, 
            MatchTeamPlayer.team_id==team_id,
            MatchTeamPlayer.profile_id==profile_id
        ).first()
        return dict(data=data)

class PlayerNextMatchResource(Resource):

    def get(self, profile_id):
        q = self.parser.parse_args()

class PlayerTeamMetricResource(Resource):

    def get(self, profile_id, team_id):
        query = PlayerTeamMetric.query.filter(PlayerTeamMetric.profile_id==profile_id, PlayerTeamMetric.team_id==team_id)
        return dict(data=query.first())

class PlayerTeamGoalMetricResource(Resource):

    def get(self, profile_id, team_id):
        events = MatchEvent.query.filter(MatchEvent.profile_id==profile_id, MatchEvent.team_id==team_id, MatchEvent.event_type == 6).all()
        events = Counter([(e.match.round+1, e.team2.name, e.match.match_date) for e in events])
        events = [{'round': e[0], 'team': e[1], 'date': e[2].strftime('%Y-%m-%d'), 'goals': goals} for (e, goals) in events.items()]
        return dict(data=events)