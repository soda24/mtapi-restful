# -*- coding: utf-8 -*-

from .tournaments import *
from .teams import *
from .fixture import *
from .observers import *
from .players import *
from .referees import *
from .responsables import *
from .sportcenters import *
from .matches import *
from .events import *

resources = [
    ('tournamentmanagement/tournaments', TournamentCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>', TournamentResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/status', TournamentStatusResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/initialization', TournamentInitializationResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/terms_and_conditions', TournamentTermsCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/fair_play_table', TournamentFairPlayResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/scorer_table', TournamentScorerResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/position_tables', TournamentPositionTableCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/position_tables/<int:round_id>', TournamentPositionTableResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/teams', TournamentTeamCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/teams/<int:team_id>', TournamentTeamResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/referees', TournamentRefereeCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/referees/<int:profile_id>', TournamentRefereeResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/observers', TournamentObserverCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/observers/<int:profile_id>', TournamentObserverResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/sportcenters', TournamentSportcenterCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/sportcenters/<int:sportcenter_id>', TournamentSportcenterResource),
    
    ('tournamentmanagement/tournaments/<int:tournament_id>/next_round', TournamentNextRoundResource),
    ('tournamentmanagement/tournaments/<int:tournament_id>/round/<int:round_id>', TournamentRoundResource),

    ('tournamentmanagement/tournaments/<int:tournament_id>/fixture', TournamentFixtureResource),
    ('tournamentmanagement/fixtures/<int:tournament_id>/round/<int:round_id>/statuses', FixtureRoundStatusesResource),
    ('tournamentmanagement/fixtures/<int:tournament_id>/last_round_info', FixtureLastRoundInfoResource),

    ('tournamentmanagement/tournaments/<int:tournament_id>/sponsors', Tournament_SponsorCollection),
    ('tournamentmanagement/tournaments/<int:tournament_id>/sponsors/<sponsor_id>', Tournament_SponsorResource),

    #Fixture
    ('tournamentmanagement/matches', MatchCollection),
    ('tournamentmanagement/matches/<int:match_id>', MatchResource),
    
    ('tournamentmanagement/matches/<int:match_id>/referees', MatchRefereeCollection),
    ('tournamentmanagement/matches/<int:match_id>/observers', MatchObserverCollection),
    ('tournamentmanagement/matches/<int:match_id>/titulars', MatchTitularCollection),
    ('tournamentmanagement/matches/<int:match_id>/players', MatchPlayerCollection),
    ('tournamentmanagement/matches/<int:match_id>/events', MatchEventCollection),
    ('tournamentmanagement/matches/<int:match_id>/call_to_play', MatchCallToPlayVerb),

    #events
    ('tournamentmanagement/events', EventCollection),
    ('tournamentmanagement/events/<int:event_id>', EventResource),
    ('tournamentmanagement/matches/<int:match_id>/teams/<int:team_id>/titulars', MatchTeamTitularEventColletion),

    #Teams
    ('tournamentmanagement/teams', TeamCollection),
    ('tournamentmanagement/teams/<int:team_id>', TeamResource),
    ('tournamentmanagement/teams/<int:team_id>/position_tables', TeamPositionTableCollection),
    ('tournamentmanagement/teams/<int:team_id>/matches', TeamMatchCollection),
    ('tournamentmanagement/teams/<int:team_id>/matches/<int:round_number>', TeamMatchResource),
    ('tournamentmanagement/teams/<int:team_id>/players', TeamPlayerCollection),
    ('tournamentmanagement/teams/<int:team_id>/players/<int:profile_id>', TeamPlayerResource),
    ('tournamentmanagement/teams/<int:team_id>/validate', TeamValidateVerb),
    ('tournamentmanagement/teams/<int:team_id>/next_match', TeamNextMatchResource),

    #Referees
    ('tournamentmanagement/referees', RefereeCollection),
    ('tournamentmanagement/referees/<int:profile_id>', RefereeResource),
    ('tournamentmanagement/referees/<int:profile_id>/tournaments', RefereeTournamentCollection),

    #Observers
    ('tournamentmanagement/observers', ObserverCollection),
    ('tournamentmanagement/observers/<int:profile_id>', ObserverResource),
    ('tournamentmanagement/observers/<int:profile_id>/tournaments', ObserverTournamentCollection),
    ('tournamentmanagement/observers/<int:profile_id>/next_match', ObserverNextMatchResource),

    #Players
    ('tournamentmanagement/players', PlayerCollection),
    ('tournamentmanagement/players/<int:profile_id>', PlayerResource),
    ('tournamentmanagement/players/<int:profile_id>/teams', PlayerTeamCollection),
    ('tournamentmanagement/players/<int:profile_id>/teams/<int:team_id>', PlayerTeamResource),
    ('tournamentmanagement/players/<int:profile_id>/teams/<int:team_id>/metrics', PlayerTeamMetricResource),
    ('tournamentmanagement/players/<int:profile_id>/teams/<int:team_id>/metrics/goals', PlayerTeamGoalMetricResource),
    ('tournamentmanagement/players/<int:profile_id>/next_match', PlayerNextMatchResource),
    ('tournamentmanagement/players/<int:profile_id>/teams/<int:team_id>/matches/<int:match_id>/info', MatchTeamPlayerInfoResource),
    # ('tournamentmanagement/players/<int:profile_id>/metrics/goals', PlayerGoalMetricResource),

    #Sportcenters
    ('tournamentmanagement/sportcenters', SportcenterCollection),
    ('tournamentmanagement/sportcenters/<int:sportcenter_id>', SportcenterResource),

    #responsables
    ('tournamentmanagement/responsables/<int:profile_id>/next_match', ResponsableNextMatchResource),
]
