# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import abort
from flask import request, url_for

from sqlalchemy import func, or_

from flc.models import *
from flc.core import db, limits
from flc.resources import Resource

import traceback 
import sys
import datetime

from flc import settings
from flc.tasks import teams as task_teams

def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()

def save_media(tournament_id, file, filename, ident):
    content = file.read()
    file.seek(0)
    checksum = hashfile(file, hashlib.sha256())
    
    m = magic.Magic()
    content_type = m.from_buffer(content[:1024])
    content = bytearray(content)
    media_id = db.Team.store_media(tournament_id, {
        'filename': filename,
        'content_type': content_type,
        'content': content,
        'checksum': checksum,
        'created_by': ident,
        })
    return media_id

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()


class TeamCollection(Resource):

    def get(self):
        self.parser.add_argument('profile_id', type=int, location='args')
        self.parser.add_argument('tournament_id', type=str, location='args')
        q = self.parser.parse_args()
        query = Team.query
        if q.ident:
            profile = Responsable.query.get(q.ident)
            if profile.role == 'responsable':
                query = query.filter_by(responsable_profile_id=q.ident, is_link_accepted=True)
        if q.tournament_id:
            ids = q.tournament_id.split(',')
            query = query.filter(Team.tournament_id.in_(ids))
        return dict(data=query.all())

    def post(self):
        self.parser.add_argument('email', type=str, required=True, help="email del responsable es obligatorio")
        q = self.parser.parse_args()
        try:
            now = datetime.datetime.now()
            team = Team()

            assert q.ident, "Usuario no autenticado"
            organizer = Organizer.query.get(q.ident)
            assert organizer.role == 'organizer', "Debes ser organizador para poder crear un torneo"
            post = request.json
            assert post.get('email'), "campo email obligatorio"
            assert post.get('tournament_id'), "tournament_id obligatorio"
            for key, value in post.items():
                if key == 'start_date':
                    if len(value) == 10:
                        value = datetime.datetime.strptime(value, '%Y-%m-%d')
                setattr(team, key, value)


            team.responsable_profile_id = q.ident

            #Checking that a team doesn't already exists
            teams = Team.query.filter(func.lower(Team.name)==team.name.lower(), Team.tournament_id==team.tournament_id).all()
            if teams: return dict(error="no se pudo crear el torneo", data={'name': "un equipo con el mismo nombre ya existe en este torneo"}, status="error"), 403

            #Setting defaults
            team.is_active = True
            team.status = 'pending'
            team.created_by = q.ident
            team.created_at = now
            team.updated_by = q.ident
            team.updated_at = now
            db.session.add(team)
            db.session.commit()

            email = q.email.strip().lower()
            user = User.query.filter_by(email=email).first()
            if not user:
                user_id, profile_id = task_teams.create_user(q.email, team.team_id, 'responsable', q.ident)
            elif not user.get_profile('responsable'):
                profile_id = task_teams.create_profile(user.user_id, team.team_id, 'responsable', q.ident)

            return dict(data=dict(id=team.team_id), status=200, message="equipo creado con exito"), 201, {'Location': url_for('teamresource', team_id = team.id, _external = True)}
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403

class TeamMatchResource(Resource):

    def get(self, team_id, round_number):
        match = Match.query.filter(Match.round==round_number, or_(Match.home_team_id==team_id, Match.away_team_id==team_id)).first()
        return dict(data=match)

class TeamResource(Resource):

    def get(self, team_id, **kwargs):
        team = db.session.query(Team).get(team_id)
        assert team, "No existe un equipo con ID: {}".format(team_id)
        return dict(data=team), 200

    def put(self, team_id, *args, **kwargs):
        self.parser.add_argument('team_id', type=str, required=True, help='team_id es mandatorio')
        q = self.parser.parse_args()
        team = Team.query.get(team_id)
        assert team, "no existe un equipo con id: {}".format(team_id)
        assert team.responsable_profile_id == q.ident or team.tournament.organizer_profile_id==q.ident, "no tienes permiso para editar este torneo"
        put = request.json
        team.set_global(**put)
        db.session.add(team)
        db.session.commit()
        return dict(message="ok")

    def post(self):
        pass

    def delete(self, team_id):
        q = self.parser.parse_args()
        team = Team.query.get(team_id)
        assert team
        tournament = team.tournament
        if not tournament.organizer_profile_id == q.ident: raise Exception('no tienes permiso para eliminar este equipo')
        if tournament.start_date <= datetime.date.today() and tournament.is_setup_complete: raise Exception('no puedes eliminar un equipo de un torneo comenzado')
        db.session.delete(team)
        db.session.commit()
        return dict(data=dict(id=team_id), status="ok", message="el equipo fue eliminado con exito")


class TeamPositionTableCollection(Resource):

    def get(self, team_id):
        self.parser.add_argument('round', type=int, location='args')
        q = self.parser.parse_args()
        pt = PositionTable.query.filter_by(team_id=team_id, round=q.round).first()
        return dict(data=pt)

class TeamMatchCollection(Resource):

    def get(self, team_id):
        self.parser.add_argument('round', type=int, location='args')
        q = self.parser.parse_args()
        matches = Match.query.filter(or_(Match.home_team_id==team_id, Match.away_team_id==team_id)).all()
        return dict(data=matches)

class TeamPlayerCollection(Resource):

    def get(self, team_id):
        self.parser.add_argument('info', type=str, location='args')
        q = self.parser.parse_args()
        team = Team.query.get(team_id)
        if hasattr(q ,'info'):
            query = db.session.query(
                Profile.id.label('profile_id'),
                Profile.is_active,
                Profile.role,
                User.id.label('user_id'),
                func.IF(User.first_name is not None and User.first_name is not None, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
                User.email,
                User.avatar_path,
                TeamPlayer.team_id,
                TeamPlayer.is_link_accepted
            ).join(User).join(TeamPlayer, Profile.id==TeamPlayer.profile_id).filter(TeamPlayer.team_id==team_id, TeamPlayer.is_active==True)
            data = query.all()
        else:
            data = team.players
        return dict(data=data)

    def post(self, team_id):
        try:
            self.parser.add_argument('email', type=str, location='json', required=True, help="email del jugador es obligatorio")
            q = self.parser.parse_args()
            assert q.ident, "Usuario no autenticado"
            responsable = Responsable.query.get(q.ident)
            if not responsable or responsable.role != 'responsable': abort(400, {'error': u'no tienes autorizaciónn para manejar recursos en este equipo'})
            post = request.json
            team = Team.query.get(team_id)
            if not team.responsable_profile_id == q.ident: abort(400, {'error': u'no tienes autorizaciónn para manejar recursos en este equipo'})

            email = q.email.strip().lower()
            user = User.query.filter_by(email=email).first()
            if not user:
                user_id, profile_id = task_teams.create_user(q.email, team.team_id, 'player', q.ident)
            elif not user.get_profile('player'):
                profile_id = task_teams.create_profile(user.user_id, team.team_id, 'player', q.ident)
            else:
                profile = user.get_profile('player')
                profile_id = profile.profile_id
                task_teams.assign_profile(profile.profile_id, team.team_id, 'player', q.ident)
            data = TeamPlayer.query_info(team_id, profile_id)
            return dict(data=data, status=200, message="jugador cargado con exito")
        except Exception as error:
            err, detail, tb = sys.exc_info()
            print(traceback.format_exc(tb))
            return dict(error=error.message), 403

    def put(self, team_id):
        q = self.parser.parse_args()
        pass


class TeamPlayerResource(Resource):

    def delete(self, team_id, profile_id):
        q = self.parser.parse_args()
        team = Team.query.get(team_id)
        if not team.responsable_profile_id == q.ident: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este equipo'})

        tp = TeamPlayer.query.filter_by(team_id=team_id, profile_id=profile_id).first()
        print "TODO: Informar al jugador que se lo va a desvincular del equipo"
        if not team.tournament.is_setup_complete or not team.is_setup_complete:
            db.session.delete(tp); db.session.commit()
        else:
            tp.is_active = False
            tp.save()
        return dict(message='el jugador fue desvinculado del equipo', status='ok')

class TeamValidateVerb(Resource):

    def put(self, team_id):
        q = self.parser.parse_args()
        team = Team.query.get(team_id)
        if not team.responsable_profile_id == q.ident: abort(400, {'message': u'no tienes autorizaciónn para manejar recursos en este equipo'})
        if team.is_setup_complete:  abort(400, {'message': u'el equipo ya fue validado'})
        confirmed_players = TeamPlayer.query.filter_by(team_id=team_id, is_link_accepted=True).all()
        n = len(confirmed_players)
        if n > team.tournament.max_players:
            abort(400, **{'error': u'has excedido la cantidad de jugadores "confirmados" máxima que permite el torneo ({}/{})'.format(n, team.tournament.max_players)})
        elif n < team.tournament.min_players:
            abort(400, **{'error': u'debes cumplir con la cantidad de jugadores "confirmados" mínima que requiere el torneo ({}/{})'.format(n, team.tournament.max_players)})
        else:
            team.is_setup_complete = True
            team.save()
            return dict(message=u'el torneo ha sido validado con éxito', status='ok')

class TeamNextMatchResource(Resource):

    def get(self, team_id):
        team = Team.query.get(team_id)
        match = Match.query.filter(or_(Match.home_team_id==team_id, Match.away_team_id==team_id, Match.is_finished!=True)).order_by(Match.match_date.asc()).first()
        return dict(data=match)