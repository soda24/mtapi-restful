# -*- coding: utf-8 -*-
import logging

from flask import request

from flask.ext import restful
from flask.ext.restful import reqparse, abort

from flc.models import *
from flc.core import db
from flc.resources import Resource
from flc.tasks import tournaments as tournament_tasks

from sqlalchemy import func

import traceback 
import sys

class Resource(Resource):
    pass

    def __init__(self):
        self.logger = logging.getLogger('tournamentmanagement.' + __name__)
        super(Resource, self).__init__()


class SportcenterCollection(Resource):

    def get(self):
        self.parser.add_argument('tournament_ids', type=str, location='args')
        q = self.parser.parse_args()

        if not q.tournament_ids: abort(400, {'error':"solo puedes ver los observers que pertenecen a un torneo", 'status': 'error'})
        tournament_ids = map(int, q.tournament_ids.strip().split(','))
        query = Sportcenter.query.join(TournamentSportcenter, Sportcenter.id==TournamentSportcenter.sportcenter_id).filter(TournamentSportcenter.tournament_id.in_(tournament_ids))
        return dict(data=query.all())

    def post(self):
        self.parser.add_argument('tournament_id', type=int, location='json')
        q = self.parser.parse_args()
        profile = Profile.query.get(q.ident)
        if not profile.role == 'organizer': abort(400, {'error': 'solo un organizador puede crear un centro deportivo', 'status': 'error'})
        sportcenter = Sportcenter()
        sportcenter.set_globals(**request.json)
        sportcenter.self_check()
        sportcenter.created_by = q.ident
        sportcenter.save()
        if q.tournament_id:
            TournamentSportcenter(sportcenter_id=sportcenter.sportcenter_id, tournament_id=q.tournament_id).save()
        return dict(data=sportcenter, status="ok")

class SportcenterResource(Resource):

    def get(self):
        pass
