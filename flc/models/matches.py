# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta
from sqlalchemy import func, or_
import sqlalchemy as sa
import datetime

def round_robin(units, sets = None):
        """ Generates a schedule of "fair" pairings from a list of units """
        count = len(units)
        sets = sets or (count - 1)
        half = count / 2
        for turn in range(sets):
            left = units[:half]
            right = units[count - half - 1 + 1:][::-1]
            pairings = zip(left, right)
            if turn % 2 == 1:
                pairings = [(y, x) for (x, y) in pairings]
            units.insert(1, units.pop())
            yield pairings


class MatchJsonSerializer(JsonSerializer):
    __json_public__ = [
        'id', 'tournament_id', 'round', 'match', 'home_team_id', 'away_team_id',
        'home_goals', 'home_yellow_cards', 'home_red_cards', 'away_goals',
        'away_yellow_cards', 'away_red_cards', 'round_date', 'match_date',
        'sportcenter_id', 'sportcenter_field', 'updated_at',
        'updated_by', 'created_at', 'created_by', 'review_status',
        'observation_status', 'status', 'is_finished', 'match_id',
        'events_synchronized_at',
    ]

class Match(MatchJsonSerializer, BaseModel):

    __tablename__ = 'matches'
    __schema__    = 'tournament_management'

    id                    = sa.Column('match_id', sa.Integer, primary_key=True)
    tournament_id         = sa.Column(sa.Integer)
    round                 = sa.Column(sa.Integer)
    match                 = sa.Column(sa.Integer)

    home_team_id          = sa.Column(sa.Integer)
    away_team_id          = sa.Column(sa.Integer)

    home_goals            = sa.Column(sa.Integer, default=0)
    home_yellow_cards     = sa.Column(sa.Integer, default=0)
    home_red_cards        = sa.Column(sa.Integer, default=0)
    away_goals            = sa.Column(sa.Integer, default=0)
    away_yellow_cards     = sa.Column(sa.Integer, default=0)
    away_red_cards        = sa.Column(sa.Integer, default=0)

    round_date            = sa.Column(sa.DateTime)
    match_date            = sa.Column(sa.DateTime)
    sportcenter_id        = sa.Column(sa.Integer)

    sportcenter_field     = sa.Column(sa.Integer)

    events_synchronized_at = sa.Column(sa.DateTime)
    updated_at            = sa.Column(sa.DateTime, default=datetime.datetime.now,  onupdate=datetime.datetime.now)
    updated_by            = sa.Column(sa.Integer)
    created_at            = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by            = sa.Column(sa.Integer)

    review_status         = sa.Column(sa.CHAR(1))
    observation_status    = sa.Column(sa.CHAR(1))
    status                = sa.Column(sa.CHAR(1))

    is_finished           = sa.Column(sa.Boolean)

    referees              = sa.orm.relationship('Profile', secondary='matches_referees')
    observers             = sa.orm.relationship('Profile', secondary='matches_observers')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def match_id(self):
        return self.id

    @property
    def tournament(self):
        if self.tournament_id:
            return self.db.Tournament.query.get(self.tournament_id)

    @property
    def home_team(self):
        if self.home_team_id:
            return self.db.Team.query.get(self.home_team_id)

    @property
    def away_team(self):
        if self.away_team_id:
            return self.db.Team.query.get(self.away_team_id)

    def players_info(self):
        home = []
        away = []
        for p in self.home_team.players_info:
            p = p._asdict()
            p['local'] = 'home'
            p['match_id'] = self.id
            home.append(p)
        for p in self.away_team.players_info:
            p = p._asdict()
            p['local'] = 'away'
            p['match_id'] = self.id
            away.append(p)
        return home+away

    @property
    def observer(self):
        if self.observer_id:
            return self.db.Observer.qget(self.observer_id)

    @property
    def sportcenter(self):
        if self.sportcenter_id:
            return self.db.Sportcenter.query.get(self.sportcenter_id)


class Fixture(list):

    def __init__(self):
        self.tournament_id = None
        self.round = None

    @property
    def tournament(self):
        if self.tournament_id:
            return self.db.Tournament.query.get(self.tournament_id)

    @classmethod
    def get(cls, tournament_id=None):
        assert tournament_id, "tournament_id not provided"
        f = cls()
        f.tournament_id = tournament_id

        for n in range(len(f.tournament.teams.all())):
            f.append(FixtureRound.get(tournament_id, n))

        f.sort(key=lambda x: (x.round)) #sorted by match number
        return f

    @property
    def all_pending_matches(self):
        """PENDING """
        now = datetime.datetime.now().date()
        tm = [ match
            for fr in self
            for match in fr
            if match.round_date and now > match.round_date.date()
            and match.observation_status != 'complete'
        ]
        # tm = filter(lambda x: x.round_date and now > x.round_date.date() and x.observation_status != 'complete', self) or [] #pending_matches
        if not tm:
            return []
        tm_sorted = sorted(tm, key=lambda x: x.match) #sorted by match number
        return tm_sorted

    @classmethod
    def create(cls, tournament_id, data):
        #Creacion de un fixture
        f = cls()
        f.tournament_id = tournament_id
        # fixture['tournament_id'] = tournament_id
        tournament = f.tournament

        assert tournament, "El torneo no existe"
        if tournament.is_fixture_complete: raise Exception("El torneo ya tiene asociado un " \
        "fixture, para generar uno nuevo debe destruir el anterior")

        fixture = data['fixture']
        now = datetime.datetime.now()
        if isinstance(tournament.start_date, basestring):
            tournament.start_date = datetime.date(*map(int, tournament.start_date.split('-')))
        match_date = tournament.start_date + \
            relativedelta(weekday=tournament.match_day_of_week)
        round_date = match_date
        for rnd_idx, rnd in enumerate(fixture):
            for match_idx, m in enumerate(rnd):
                match = Match()
                match.tournament_id      = tournament_id
                match.created_at         = now
                match.last_updated_at    = now
                match.status             = 'p'
                match.review_status      = 'p'
                match.observation_status = 'p'

                round_number = rnd_idx
                match_number = match_idx

                match.match_date       = match_date
                match.round_date       = round_date
                match.round            = round_number
                match.match            = match_number
                match.home_team_id     = m['home']['team_id']
                match.away_team_id     = m['away']['team_id']
                print "RECORDAR DE HABILITAR ESTO: match.is_finished = False models/matches.py"
                # match.is_finished = False

                match.save()

            match_date = match_date+relativedelta(days=7)
            round_date = match_date+relativedelta(days=7)

        #Initialize Position Table
        cls.db.TournamentPositionTable.initialize_position_table(tournament_id)

        tournament.is_fixture_complete = True
        tournament.save()

        return tournament.tournament_id

    @classmethod
    def generate(cls, tournament_id):
        tournament = cls.db.Tournament.query.get(tournament_id)
        fixture = Fixture()
        teams = [team.id for team in tournament.teams]
        #We shuffle the list of teams
        from random import shuffle
        shuffle(teams)
        #If teams are not multiple of 2 we add a wildcard team
        if len(teams) % 2 != 0:
            teams.append(None)
        fixture.extend(list(round_robin(teams, sets = len(teams) * 2 - 2)))
        first_half = fixture[0:len(fixture)/2]
        second_half = fixture[len(fixture)/2:]
        new_fix = []
        for r, rounds in enumerate(fixture):
            # print "rounds: ", rounds
            new_rounds = []
            for m, match in enumerate(rounds):
                new_match = dict(round=r, match=m, home=dict(team_id=match[0]), away=dict(team_id=match[1]))
                new_rounds.append(new_match)
            new_fix.append(new_rounds)
        return new_fix
        # if tournament.match_rematch:
        #     return fixture
        # else:
        #     return first_half

    def get_next_round(self):
        for vround in self:
            if not vround.is_closed():
                return vround

class MatchReferee(BaseModel):

    __tablename__ = 'matches_referees'
    __schema__ = 'tournament_management'

    match_id       = sa.Column(sa.Integer, sa.ForeignKey('matches.match_id'), primary_key=True)
    profile_id     = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'), primary_key=True)
    is_link_accepted = sa.Column(sa.Boolean)
    link_id        = sa.Column(sa.Integer)
    created_at     = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by     = sa.Column(sa.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def list_info(self, match_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
            User.first_name,
            User.last_name,
            User.email,
            User.avatar_path,
            MatchReferee.is_link_accepted
        ).join(User).join(MatchReferee, Profile.id==MatchReferee.profile_id).filter(MatchReferee.match_id==match_id)
        return query.all()

    @classmethod
    def query_info(self, match_id, profile_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
            User.first_name,
            User.last_name,
            User.email,
            User.avatar_path,
            MatchReferee.is_link_accepted
        ).join(User).join(MatchReferee, Profile.id==MatchReferee.profile_id).filter(MatchReferee.match_id==match_id, MatchReferee.profile_id==profile_id)
        return query.first()

class MatchObserver(BaseModel):

    __tablename__ = 'matches_observers'
    __schema__ = 'tournament_management'

    match_id       = sa.Column(sa.Integer, sa.ForeignKey('matches.match_id'), primary_key=True)
    profile_id     = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'), primary_key=True)
    is_link_accepted = sa.Column(sa.Boolean)
    link_id        = sa.Column(sa.Integer)
    created_at     = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by     = sa.Column(sa.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def list_info(self, match_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
            User.first_name,
            User.last_name,
            User.email,
            User.avatar_path,
            MatchObserver.is_link_accepted
        ).join(User).join(MatchObserver, Profile.id==MatchObserver.profile_id).filter(MatchObserver.match_id==match_id)
        return query.all()

    @classmethod
    def query_info(self, match_id, profile_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
            User.first_name,
            User.last_name,
            User.email,
            User.avatar_path,
            MatchObserver.is_link_accepted
        ).join(User).join(MatchObserver, Profile.id==MatchObserver.profile_id).filter(MatchObserver.match_id==match_id, MatchObserver.profile_id==profile_id)
        return query.first()

class FixtureRound(list):

    def __init__(self):
        self.tournament_id = None
        self.round = None

    @property
    def tournament(self):
        if self.tournament_id:
            return self.db.Tournament.get(tournament_id=self.tournament_id)

    @classmethod
    def get(cls, tournament_id=None, vround=None):
        assert tournament_id, "tournament_id not provided"
        fr = cls()
        fr.round = vround
        fr.tournament_id = tournament_id

        if vround is not None:
            query = Match.query.filter_by(tournament_id=tournament_id, round=vround)
            for fm in query.all():
                fr.append(fm)
        fr.sort(key=lambda x: (x.match)) #sorted by match number
        return fr

    @classmethod
    def get_next_playable_round(cls, tournament_id=None):
        assert tournament_id, "tournament_id not provided"
        fixture_round = cls()
        fixture_round.round = None
        fixture_round.tournament_id = tournament_id

        query = Match.query.filter_by(tournament_id=tournament_id)

        #Traigo TODA los TournamentTeamRoundPosition
        whole_table = [fm for fm in query.all()]
        if not whole_table:
            return fixture_round

        fr = filter(lambda x: x.status != "complete", whole_table)
        #Si no hay ninguna marcada como completa, devolvemos la primer fecha
        if not fr:
            vround = 0
            fr = filter(lambda x: x.round == vround, whole_table)
        else:
            vround = max([x.round for x in fr])
            fr = filter(lambda x: x.round == vround, fr) #filter last round

        fixture_round.round = vround

        for r in fr:
            fixture_round.append(r)
        fixture_round.sort(key=lambda x: (x.match)) #sorted by match number
        return fixture_round

    @classmethod
    def get_last_round(cls, tournament_id):
        """Devuelve la última fecha que se haya jugado. Para eso se fija 
        cuales son todos los partidos de una fecha que tiene fecha a futuro 
        y devuelve la anterior"""

        assert tournament_id, "tournament_id not provided"
        fixture_round = cls()
        fixture_round.round = None
        fixture_round.tournament_id = tournament_id
        
        query = Match.query.filter_by(tournament_id=tournament_id)

        now = datetime.datetime.now().date()

        #Traigo TODA los TournamentTeamRoundPosition
        whole_table = [fm for fm in query.all()]

        fr = filter(lambda x: x.round_date and now > x.round_date.date(), whole_table) or [] #pending_matches
        max_date = max([x.round_date.date() for x in fr])
        fr = filter(lambda x: x.round_date.date() == max_date, whole_table) or [] #pending_matches

        for r in fr:
            fixture_round.append(r)
        fixture_round.sort(key=lambda x: (x.match)) #sorted by match number
        return fixture_round

    @property
    def next_round(self):
        return Match.get(tournament_id=self.tournament_id, vround=self.round+1)

    @property
    def prev_round(self):
        if self.round > 0:
            return Match.get(tournament_id=self.tournament_id, vround=self.round-1)

    def is_closed(self):
        status = self.round_status()['status']
        if (status['complete'] + status['delayed']) == status['total']:
            return True

    def round_status(self):
        "Hace un counteo de todos los estados de cada ronda de un torneo"
        "Con esto se puede sacar el porcentaje de completion de un torneo"
        assert self.round is not None, "FixtureRound no inicializado"
        
        status = {
            'complete': 0,
            'pending': 0,
            'inprogress': 0,
            'delayed': 0,
            'total': len(self),
        }
        round_status = {
            'status': status.copy(),
            'review_status': status.copy(),
            'observation_status': status.copy(),
            'round_date': self[0].round_date,
            'match_count': len(self)
        }

        for match in self:
            try:
                round_status['status'][match.status] += 1
            except:
                round_status['status'][match.status] = 1
            try:
                round_status['review_status'][match.review_status] += 1
            except:
                round_status['review_status'][match.review_status] = 1
            try:
                round_status['observation_status'][match.observation_status] += 1
            except:
                round_status['observation_status'][match.observation_status] = 1

        round_status['status']['completion'] = round_status['status']['complete'] / round_status['match_count'] * 100
        round_status['review_status']['completion'] = round_status['review_status']['complete'] / round_status['match_count'] * 100
        round_status['observation_status']['completion'] = round_status['observation_status']['complete'] / round_status['match_count'] * 100
        return round_status

    def info(self):
        any_match = self[0]
        return {'round': any_match.round, 'round_date': any_match.round_date}

    def is_complete(self):
        return True

class MatchTeamPlayerJsonSerializer(JsonSerializer):
    __json_public__ = [
        'match_id', 'team_id', 'profile_id',
    ]

class MatchTeamPlayer(MatchTeamPlayerJsonSerializer, BaseModel):

    __table_name__ = 'matches_teams_players'
    __schema__ = 'tournament_management'

    match_id         = sa.Column(sa.Integer, primary_key=True)
    team_id          = sa.Column(sa.Integer, primary_key=True)
    profile_id       = sa.Column(sa.Integer, primary_key=True)
    tournament_id    = sa.Column(sa.Integer)
    is_link_accepted = sa.Column(sa.Boolean)
    is_blocked       = sa.Column(sa.Boolean)
    link_id          = sa.Column(sa.Integer)
    blocked_reason   = sa.Column(sa.Text)
    created_at       = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by       = sa.Column(sa.Integer)
    updated_at       = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
