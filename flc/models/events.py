# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta
from sqlalchemy import func, or_
import sqlalchemy as sa
import datetime

event_types = {
    1:  'titular',
    2:  'captain',
    3:  'goalkeeper',
    4:  'player_in',
    5:  'player_out',
    6:  'goal',
    7:  'goal_against',
    8:  'yellow_card',
    9:  'green_card',
    10: 'blue_card',
    11: 'red_card'
}

class MatchEventJsonSerializer(JsonSerializer):
    __json_public__ = [
    'id', 'ref_event_id', 'event_type', 'tournament_id', 'match_id',
    'team_id', 'profile_id', 'user_id', 'observer_id', 'half_time', 'time',
    'extra', 'position', 'match_date', 'status', 'is_initial_event',
    'event_type_label', 'event_id', 'external_event_id'
    ]

class MatchEvent(MatchEventJsonSerializer, BaseModel):

    __tablename__ = 'events'
    __schema__    = 'tournament_management'

    id                    = sa.Column('event_id', sa.Integer, primary_key=True)
    ref_event_id          = sa.Column(sa.Integer())
    external_event_id     = sa.Column(sa.String(300))
    event_type            = sa.Column(sa.Integer)
    is_initial_event      = sa.Column(sa.Boolean)
    tournament_id         = sa.Column(sa.Integer)
    match_id              = sa.Column(sa.Integer, sa.ForeignKey('matches.match_id'))
    match                 = sa.orm.relationship('Match', backref=sa.orm.backref('events', lazy='dynamic'), foreign_keys=[match_id])
    team_id               = sa.Column(sa.Integer, sa.ForeignKey('teams.team_id'))
    team                  = sa.orm.relationship('Team', backref=sa.orm.backref('events', lazy='dynamic'), foreign_keys=[team_id])
    profile_id            = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    profile               = sa.orm.relationship('Profile', foreign_keys=[profile_id])
    user_id               = sa.Column(sa.Integer)
    observer_id           = sa.Column(sa.Integer)
    half_time             = sa.Column(sa.Integer())
    time                  = sa.Column(sa.Integer())
    extra                 = sa.Column(sa.Text())
    position              = sa.Column(sa.Integer())
    match_date            = sa.Column(sa.DateTime())
    status                = sa.Column(sa.CHAR(1))
    created_by            = sa.Column(sa.Integer())
    created_at            = sa.Column(sa.DateTime(), default=datetime.datetime.now)
    updated_by            = sa.Column(sa.Integer())
    updated_at            = sa.Column(sa.DateTime(), default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def event_id(self):
        return self.id

    def set_global(self, **kwargs):
        for key, value in kwargs.items():
            if key not in self.__json_public__ or key in ('id', 'event_id', 'created_by', 'created_at', 'updated_by', 'updated_at',):
                continue
            setattr(self, key, value)

    @property
    def event_type_label(self):
        return event_types.get(self.event_type)

    def check(self):
        pass

    def do_complete(self):
        match = self.db.Match.query.get(self.match_id)
        self.match_date = match.match_date

        assert match.status in ('i', 'c', None, 'p'), \
            "El estado actual del partido no permite la carga de eventos"

        team  = self.db.Team.query.get(self.team_id)
        self.tournament_id = team.tournament_id
        
        profile = self.db.Profile.query.get(self.profile_id)
        self.user_id = profile.user_id
        
        if self.event_type == 'change':
            assert self.player2_id, "player2_id mandatorio cuando el tipo de evento es 'cambio'"
            player2 = self.db.Player.get(profile_id=self.player2_id)
            self.user2_id = player2.user_id

    @property
    def team2(self):
        match = self.match
        if self.team_id == match.home_team_id:
            return match.away_team
        else:
            return match.home_team

class MatchEventTypeJsonSerializer(JsonSerializer):
    __json_public__ = [
    'id', 'name', 'description'
    ]

class MatchEventType(MatchEventTypeJsonSerializer, BaseModel):

    __tablename__ = 'event_types'
    __schema__    = 'tournament_management'

    id          = sa.Column('event_type_id', sa.Integer, primary_key=True)
    name        = sa.Column(sa.String(100))
    description = sa.Column(sa.Text)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def event_type_id(self):
        return self.id


class MatchEvents(list):

    def __init__(self, match_id):
        self.tournament_id = None
        self.match_id = match_id
        self.observed_by = None
        self.match = self.db.Match.query.get(self.match_id)
        self.extend(self.match.events.all())

    def reduce(self):
        team_events = {}
        result = {}
        for x, event in enumerate(self):
            key = event.team_id
            goal = (event.event_type_label == 'goal' and 1) or 0
            goal_against = (event.event_type_label == 'goal_against' and 1) or 0
            yellow_card = (event.event_type_label == 'yellow_card' and 1) or 0
            red_card = (event.event_type_label == 'red_card' and 1) or 0
            change = (event.event_type_label == 'change' and 1) or 0

            if key not in result:
                result[key] = [0, 0, 0, 0, 0]
            r = result[key]

            result[key] = [r[0]+goal, r[1]+goal_against, r[2]+yellow_card, r[3]+red_card, r[4]+change]
        return result

    def finish_observation(self):
        assert self.observed_by, "observer_by en MatchEvents es mandatorio"
        match = self.match
        now = datetime.datetime.now()
        for event in self:
            event.updated_by = self.observed_by
            event.updated_at = now
            event.event_status = 'complete'
            event.save()


        data = self.reduce()

        """
            idxs:
            0: goals
            1: goals_against
            2: yellow_cards
            3: red_cards
            4: changes
        """

        home_team_data = data.get(self.match.home_team_id) or [0, 0, 0, 0, 0]
        away_team_data = data.get(self.match.away_team_id) or [0, 0, 0, 0, 0]

        #Compute the goals against
        home_wrong_goals    = home_team_data[1]
        away_wrong_goals    = away_team_data[1]
        home_team_data[1]  += away_team_data[0]
        away_team_data[1]  += home_team_data[0]

        #Compute goals against a goals in favor of the other team
        home_team_data[0]  += away_wrong_goals
        away_team_data[0]  += home_wrong_goals

        #Actualiza el partido con los resultados
        match.observation_status = 'complete'
        match.home_goals = home_team_data[0]
        match.home_yellow_cards = home_team_data[2]
        match.home_red_cards = home_team_data[3]

        match.away_goals = away_team_data[0]
        match.away_yellow_cards = away_team_data[2]
        match.away_red_cards = away_team_data[3]

        match.updated_at = now
        match.updated_by = self.observed_by
        match.save()

        return True
