# -*- coding: utf-8 -*-
import logging

from flask.ext import restful
from flask.ext.restful import reqparse

from flc.models import *
from flc.core import db, BaseModel

import traceback 
import sys

from sqlalchemy import func, or_


class TeamJsonSerializer(JsonSerializer):
    __json_public__ = [
        'id', 'team_id', 'team_line_id', 'tournament_id', 'name',
        'description', 'logo_path', 'responsable_profile_id', 'is_active',
        'created_at', 'created_by', 'updated_at', 'updated_by', 'status',
    ]

class Team(TeamJsonSerializer, BaseModel):

    __tablename__ = 'teams'
    __schema__ = 'tournament_management'

    id                     = sa.Column('team_id', sa.Integer, primary_key=True)
    team_line_id           = sa.Column(sa.Integer)
    tournament_id          = sa.Column(sa.Integer, sa.ForeignKey('tournaments.tournament_id'))
    tournament             = sa.orm.relationship('Tournament', backref=sa.orm.backref('teams', lazy='dynamic'), foreign_keys=[tournament_id])

    name                   = sa.Column(sa.String(500))
    description            = sa.Column(sa.String(500))
    logo_path              = sa.Column(sa.String(500))
    responsable_profile_id = sa.Column(sa.Integer)
    is_active              = sa.Column(sa.Boolean)
    is_link_accepted       = sa.Column(sa.Boolean)
    link_id                = sa.Column(sa.Integer)
    is_setup_complete      = sa.Column(sa.Boolean)
    created_at             = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by             = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    updated_at             = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    updated_by             = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    status                 = sa.Column(sa.String(20))

    # def __str__(self):
    #     return 'Team <team_id={} - name={}>'.format(self.team_id, self.name)

    # def __unicode__(self):
    #     return u'Team <team_id={} - name={}>'.format(self.team_id, self.name)

    @property
    def team_id(self):
        return self.id

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


    def set_global(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__json_public__:
                if key in ('id', 'team_id'):
                    continue
                if value and key in ('end_date', 'start_date', 'created_at', 'updated_at'):
                    if 'T' in value:
                        value = datetime.datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                setattr(self, key, value)

    def info(self):
        info = dict(
            team_id = self.team_id,
            team_line_id = self.team_line_id,
            name = self.name,
            status = self.status,
            is_active = self.is_active,
            team_line = self.team_line and self.team_line.name
            )
        return info

    @property
    def team_line(self):
        if self.team_line_id:
            return self.db.TeamLine.get(team_line_id=self.team_line_id)

    @property
    def players(self):
        raise Exception('not implemented')

    @property
    def players_info(self):
        return TeamPlayer.list_info(self.team_id)

    @property
    def team_players(self):
        return self.db.TeamPlayers.get(team_id=self.team_id)

    @property
    def players_team_info(self):
        player_team_info = lambda x: self.db.PlayerTeam.get(profile_id=x, team_id=self.team_id)
        return [player_team_info(profile_id) for profile_id in self.player_id]

    @property
    def dt(self):
        if self.dt_id:
            return self.db.DT.get(profile_id=self.dt_id)

    @property
    def dt_link(self):
        return self.db.DtTeam.get(profile_id=self.dt_id, team_id=self.team_id)

    @property
    def responsable(self):
        if self.responsable_profile_id:
            return self.db.Responsable.get(profile_id=self.responsable_profile_id)

    @property
    def pending_matches(self):
        now = datetime.datetime.now()
        return [
            m for m in self.tournament.matches if self.team_id in (m.home_team_id, m.away_team_id)
            and m.status == 'pending' and m.match_date > now
            # and m.match_date > now
        ]

    @property
    def next_pending_match(self):
        if self.pending_matches:
            return sorted(self.pending_matches, key=lambda x: x.match_date)[0]

    @property
    def last_match(self):
        roundxx = FixtureRound.get_last_round(self.tournament_id)
        match = filter(lambda x: self.team_id in (x.home_team_id, x.away_team_id), roundxx)
        match = match[0]
        return match

    @property
    def last_position(self):
        return PositionTable.query.filter_by(tournament_id=self.tournament_id, team_id=self.team_id).order_by(PositionTable.round.desc()).first()

    def get_linkages(self):
        object_linkages_q = Linkage.objects.filter(link_object_id=self.team_id)
        tgt_linkages_q = Linkage.objects.filter(link_target_id=self.team_id)

        return {'object': [l for l in object_linkages_q.all()],
                'tgt': [l for l in tgt_linkages_q.all()]}

    def set_responsable(self, profile_id, link_id=None):
        responsable = self.db.Profile.query.get(profile_id)
        assert responsable.is_active
        assert responsable.user.is_active
        assert responsable.role == 'responsable'
        self.is_link_accepted = True
        self.link_id = link_id
        self.save()

    def do_validate(self):
        now = datetime.datetime.now()
        assert self.status != 'complete'

        # assert self.dt_id, "El equipo no tiene un DT"
        # dt_team = self.dt_link

        # #Verifico que exista un DT
        # assert self.dt_id, "No hay un DT asignado"
        # assert dt_team.link != "rejected", "El dt no aceptó dirigir el equipo"
        # assert dt_team.link == "accepted", "Falta confirmación del DT"

        tournament = self.tournament


        def min_max_allowed(players, t_min, t_max):
            assert players and \
            len(players) >= t_min and \
            len(players) <= t_max, \
            u"Necesita contar con la cantidad min/max ({}/{}) de jugadores \"confirmados\" para poder validar al equipo".format(
                str(t_min),
                str(t_max)
            )

        assert self.team_id in tournament.team_id, \
        "inconsistencia: el equipo %s no esta incluido en el torneo %s" % \
        (str(self.team_id), str(self.tournament_id))
        assert tournament.start_date > now

        #validar que la cantidad de jugadores que aceptaron estar en el
        #equipo, este dentro del min y max del torneo
        assert self.player_id, "No hay jugadores cargados en el equipo"

        players = self.players_team_info
        players = filter(lambda x: x.link == 'accepted', players)
        min_max_allowed(players, tournament.min_players, tournament.max_players)

        #Verificar que la edad de los jugadores coincide con la edad permitida
        #por el torneo
        print "TODO: implementar y verificar la edad de los jugadores"

        self.status = 'complete'
        self.last_updated_at = now
        self.save()
        return True


class BasicTeam(object):

    @classmethod
    def create(cls, **kwargs):
        team = Team()
        team.set_global(kwargs)

        responsable = team.db.Profile.get(profile_id=kwargs['responsable_profile_id'])
        requester = team.db.Profile.get(profile_id=kwargs['requester_profile_id'])

        if requester.role == 'organizer':
            team['responsable_profile_id'] = requester.profile_id

        now = datetime.datetime.now()
        #Checking that a team doesn't already exists
        assert requester.user.is_active
        assert requester.role in ('organizer', 'responsable')
        
        tournament_id = team.tournament_id
        tournament = Tournament.get(tournament_id=team.tournament_id)
        assert tournament, "El torneo {0} no existe".format(str(tournament_id))
        #Assert que el torneo este activo
        assert tournament.is_active
        #Assert que el torneo no haya comenzado
        assert not tournament.start_date or tournament.start_date > now, \
            u"No puedes agregar equipos a un torneo que ya ha comenzado"
        #Assert que el que pidio agregar el equipo al torneo
        #Sea el mismo organizador del torneo y no otros
        assert tournament.organizer_profile_id == \
                requester.profile_id
        
        if tournament.team_id and tournament.teams:
            for tournament_team in tournament.teams:
                assert tournament_team.name.lower() != team.name.lower(), \
                    "Ya existe un equipo con el mismo nombre jugando en el torneo {}".format(
                        tournament.name
                        )

        #Assertions - checks
        assert len(team.name) >= limits.MIN_TEAM_NAME_LENGHT, \
            "El nombre del equipo debe tener al menos {0} caracteres".format(
                limits.MIN_TEAM_NAME_LENGHT
            )
        assert team.responsable_profile_id

        #Assert responsable
        assert responsable.role in ('responsable', 'organizer')
        if responsable.user.validated:
            assert responsable.user.is_active
            assert responsable.is_active

        team.is_active = True
        team.created_at = now
        team.last_updated_at = now
        team.status = 'editing'
        if not team.logo_path:
            team.logo_path = 'team/no_escudo.png'

        team.save()

        if team.team_id not in tournament.team_id:
            tournament.team_id.add(team.team_id)
            tournament.save()

        link_hash = None

        if requester.role == 'organizer':
            # self.send_message('Avisarle al responsable que se creo un torneo y un organizador se lo asigno')
            link_hash = Linkage.link_team_responsable(
                    petitioner_type = requester.role,
                    petitioner_profile_id = requester.profile_id,
                    receiver_type = responsable.role,
                    receiver_profile_id = responsable.profile_id,
                    link_object = 'responsable',
                    link_object_id = responsable.profile_id,
                    link_target = 'team',
                    link_target_id = team.team_id,
                    link_status = 'pending'
                )
        return team, link_hash


class TeamPlayerJsonSerializer(JsonSerializer):
    __json_public__ = [
    'team_id', 'profile_id', 'tournament_id', 'position_number', 'is_active',
    ]


class TeamPlayer(TeamPlayerJsonSerializer, BaseModel):

    __tablename__ = 'teams_players'
    __schema__ = 'tournament_management'

    team_id          = sa.Column(sa.Integer, primary_key=True)
    profile_id       = sa.Column(sa.Integer, primary_key=True)
    tournament_id    = sa.Column(sa.Integer)
    position_number  = sa.Column(sa.Integer)
    created_at       = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by       = sa.Column(sa.Integer)
    updated_at       = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    updated_by       = sa.Column(sa.Integer)
    is_active        = sa.Column(sa.Boolean, default=True)
    is_link_accepted = sa.Column(sa.Boolean)
    link_id          = sa.Column(sa.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def player(self):
        if self.profile_id:
            return self.db.Player.get(profile_id=self.profile_id)

    @property
    def player_team_info(self):
        player_info = self.player.info()

        player_info['team_id'] = self.team_id
        player_info['team_line_id'] = self.team_line_id
        player_info['team_link'] = self.link
        player_info['position'] = self.position
        player_info['card'] = self.card
        player_info['skip_match'] = self.skip_match
        player_info['goals'] = self.goals
        
        return player_info

    @property
    def team(self):
        if self.team_id:
            return self.db.Team.get(team_id=self.team_id)

    @classmethod
    def list_info(self, team_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('id'),
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(
                (User.first_name is not None and User.last_name is not None), func.concat(User.first_name, ' ', User.last_name), User.email
                ).label('name'),
            User.email,
            User.avatar_path,
            TeamPlayer.team_id,
            TeamPlayer.is_link_accepted
        ).join(User).join(TeamPlayer, Profile.id==TeamPlayer.profile_id).filter(TeamPlayer.team_id==team_id)
        return query.all()

    @classmethod
    def query_info(self, team_id, profile_id):
        User = self.db.User
        Profile = self.db.Profile
        query = db.session.query(
            Profile.id.label('profile_id'),
            Profile.is_active,
            Profile.role,
            User.id.label('user_id'),
            func.IF(User.first_name and User.first_name, func.concat(User.first_name, ' ', User.last_name), User.email).label('name'),
            User.first_name,
            User.last_name,
            User.email,
            User.avatar_path,
            TeamPlayer.team_id,
            TeamPlayer.is_link_accepted
        ).join(User).join(TeamPlayer, Profile.id==TeamPlayer.profile_id).filter(TeamPlayer.team_id==team_id, TeamPlayer.profile_id==profile_id)
        return query.first()

class TeamPlayers(list):

    def __init__(self):
        self.tournament_id = None
        self.team_id = None

    @classmethod
    def get(cls, team_id=None):
        assert team_id, "tournament_id not provided"
        tps = cls()
        team = tps.db.Team.get(team_id=team_id)
        tps.team_id = team.team_id
        tps.tournament_id = team.tournament_id
        if team and team.player_id:
            for player_id in team.player_id:
                player = tps.db.Player.get(profile_id=player_id)
                tps.append(player)
        return tps

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def players_info(self):
        return [p.info() for p in self]

