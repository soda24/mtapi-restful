# -*- coding: utf-8 -*-
from flask_security import UserMixin, RoleMixin
from flask import current_app as app

import sqlalchemy as sa
from sqlalchemy.orm.session import object_session

from flc.core import db, BaseModel, limits
from flc.helpers import JsonSerializer
from flc import settings
# from flc.db.connector import BaseModel

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature

from passlib.apps import custom_app_context as pwd_context
import base64
import hashlib
import hmac

import uuid

import datetime
import arrow

from dateutil.tz import tzlocal
from dateutil.relativedelta import relativedelta

def get_hmac(password):
    h = hmac.new(settings.SECURITY_PASSWORD_SALT, password.encode('utf-8'), hashlib.sha512)
    return base64.b64encode(h.digest())

class LinkageJsonSerializer(JsonSerializer):
    __json_public__ = [
    'link_hash', 'link_reason', 'petitioner_type', 'petitioner_profile_id',
    'receiver_type', 'receiver_profile_id', 'link_object', 'link_object_id',
    'link_target', 'link_target_id', 'link_status', 'link_response', 
    'link_extra_information', 'link_responded_at', 'created_at', 'updated_at',
    'expiration_date', 'legend'
    ]

class Linkage(LinkageJsonSerializer, BaseModel):

    __tablename__ = 'linkages'
    __schema__ = 'tournament_management'

    id                     = sa.Column(sa.Integer, primary_key=True)
    link_hash              = sa.Column(sa.String(100))
    link_reason            = sa.Column(sa.String(100))
    petitioner_type        = sa.Column(sa.String(100))
    petitioner_profile_id  = sa.Column(sa.Integer)
    receiver_type          = sa.Column(sa.String(100))
    receiver_profile_id    = sa.Column(sa.Integer)
    link_object            = sa.Column(sa.String(100))
    link_object_id         = sa.Column(sa.Integer)
    link_target            = sa.Column(sa.String(100))
    link_target_id         = sa.Column(sa.Integer)
    is_link_accepted       = sa.Column(sa.Boolean)
    link_status            = sa.Column(sa.String(100))
    link_response          = sa.Column(sa.String(100))
    link_extra_information = sa.Column(sa.Text())
    link_responded_at      = sa.Column(sa.DateTime)
    created_at             = sa.Column(sa.DateTime)
    updated_at             = sa.Column(sa.DateTime)
    expiration_date        = sa.Column(sa.DateTime)

    @property
    def legend(self):
        if self.link_reason == 'link_team_responsable':
            organizer = self.db.Organizer.qget(profile_id=self.petitioner_profile_id)
            responsable = self.db.Responsable.qget(profile_id=self.petitioner_profile_id)
            team = self.db.Team.query.get(self.link_target_id)
            return u'Responsable del equipo "{}" en torneo "{}"'.format(team.name, team.tournament.name)
        return self.link_reason

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def link(self, 
            link_reason = None,
            petitioner_type = None,
            petitioner_profile_id = None,
            receiver_type = None,
            receiver_profile_id = None,
            link_object = None,
            link_object_id = None,
            link_target = None,
            link_target_id = None,
            link_status = 'pending',
            link_extra_information = None,
            link_response = None,
            link_responded_at = None,
            created_at = None,
            updated_at = None,
            expiration_date = None
        ):
        if not expiration_date:
            self.expiration_date    = datetime.datetime.now(tzlocal())+datetime.timedelta(days=5)
        self.link_hash              = get_hmac(str(uuid.uuid4()))
        self.created_at             = datetime.datetime.now(tzlocal())
        self.link_reason            = link_reason
        self.petitioner_type        = petitioner_type
        self.petitioner_profile_id  = petitioner_profile_id
        self.receiver_type          = receiver_type
        self.receiver_profile_id    = receiver_profile_id
        self.link_object            = link_object
        self.link_object_id         = link_object_id
        self.link_target            = link_target
        self.link_target_id         = link_target_id
        self.link_status            = link_status or 'pending'
        self.link_response          = link_response
        self.link_responded_at      = link_responded_at
        self.created_at             = created_at
        self.updated_at             = updated_at
        self.link_extra_information = link_extra_information
        self.save()
        return self.link_hash

    def do_link(self, **kwargs):
        return self.link(
            **kwargs
            )

    def do_link_response(self, response):
        assert response in (
            'deactivated', 'rejected', 'accepted', 'pending', 'expired'
            ), "{} no es una respuesta valida"

        now = datetime.datetime.now()

        self.link_response = response
        self.link_responded_at = now
        self.save()

    @classmethod
    def link_team_responsable(
                    cls,
                    petitioner_type,
                    petitioner_profile_id,
                    receiver_type,
                    receiver_profile_id,
                    link_object,
                    link_object_id,
                    link_target,
                    link_target_id,
                    link_status,
                ):
        link = cls()
        return link.link(
            link_reason = 'link_team_responsable',
            petitioner_type = petitioner_type,
            petitioner_profile_id = petitioner_profile_id,
            receiver_type = receiver_type,
            receiver_profile_id = receiver_profile_id,
            link_object = link_object,
            link_object_id = link_object_id,
            link_target = link_target,
            link_target_id = link_target_id,
            link_status = link_status
        )

    @classmethod
    def link_player_to_team(
                    cls,
                    petitioner_type,
                    petitioner_profile_id,
                    receiver_type,
                    receiver_profile_id,
                    link_object,
                    link_object_id,
                    link_target,
                    link_target_id,
                ):
        return self.link(
            link_reason = 'link_player_to_team',
            petitioner_type = petitioner_type,
            petitioner_profile_id = petitioner_profile_id,
            receiver_type = receiver_type,
            receiver_profile_id = receiver_profile_id,
            link_object = link_object,
            link_object_id = link_object_id,
            link_target = link_target,
            link_target_id = link_target_id,
        )

    def accept_link_player_to_team(self, profile_id, link_hash):
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self

        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'player', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        profile = self.db.Profile.query.get(profile_id)
        if profile.role != self.link_object:
            profile = profile.user.get_profile('player')
            assert profile, "El Usuario no tiene un perfil de jugador."
            assert profile.profile_id == self.link_object_id, u"No tienes autorización para utilizar este link"
            profile_id = profile.profile_id
        
        assert self.link_target == 'team', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        team = self.db.Team.query.get(linkage.link_target_id)

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"
        team_player = self.db.TeamPlayer.query.filter_by(profile_id=profile_id, team_id=team.team_id).first()
        assert team_player, \
            u"El jugador no forma parte del equipo"

        player = self.db.Player.query.get(profile_id)
        
        assert player.role == 'player'
        assert player.is_active, u"El profile %s está inactivo" % str(profile_id)

        #Seteamos el id de jugador en el equipo (player_team)
        team_player.is_link_accepted = True
        team_player.is_active = True
        team_player.link_id = self.id
        team_player.save()

        #Buscamos al responsable para poder mandarle un mensaje de que el jugador
        #Aceptó formar parte del equipo
        responsable = self.db.Profile.query.get(self.petitioner_profile_id)

        self.do_link_response('accepted')

        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_player_to_team(
            player,
            responsable,
            team
        )
        return u"Ahora formas parte del equipo {0}".format(team.name)

    def reject_link_player_to_team(self, profile_id, link_hash):
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'player', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        assert self.link_object_id == profile_id, u"object_id != profile_id"
        assert self.link_target == 'team', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El link ya no es utilizable"

        team = self.db.Team.query.get(linkage.link_target_id)

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"
        assert team.player_id and profile_id in team.player_id, \
            u"El jugador no forma parte del equipo"

        player = self.db.Player.query.get(profile_id)
        
        player_profile = player_user_profile['profile']
        assert player_profile.role == 'player'
        assert player_profile.is_active, u"El profile %s está inactivo" % str(profile_id)

        #Seteamos el id de jugador en el equipo (player_team)
        self.db.player.unset_player_team(player_profile.profile_id,
                                            team.team_id)
        

        #Buscamos al responsable para poder mandarle un mensaje de que el jugador
        #Aceptó formar parte del equipo
        responsable = self.db.Profile.query.get(self.petitioner_profile_id)

        self.link_response(link_hash, 'rejected')
        #Enviamos el mensaje de aceptación
        self.db.Message().message__rejected_link_player_to_team(
            player_user_profile,
            responsable_user_profile,
            team
        )
        return u"Has decidido no formar parte del equipo {0}. El Delegado \
responsable será notificado".format(team.name)


    def accept_link_observer_to_match(self, profile_id, link_hash):
        print u"accept_link_observer_to_match"
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'observer', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        profile = self.db.Profile.query.get(profile_id)
        if profile.role != self.link_object:
            profile = profile.user.get_profile('observer')
            assert profile, "El Usuario no tiene un perfil de observador."
            assert profile.profile_id == int(str(self.link_object_id)), u"No tienes autorización para utilizar este link"
            assert profile.is_active, u"El perfil de observador del usuario está inactivo" % str(profile_id)
            profile_id = profile.profile_id

        assert self.link_target == 'match', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status in ('pending', None), u"El estado del link es incorrecto"

        match_id = linkage.link_target_id
        match = self.db.Match.query.get(match_id)

        assert match, u"El partido no existe"
        assert match.status == 'p', u"No puedes observar este partido."

        resource = self.db.MatchObserver.query.filter_by(match_id=match.match_id, profile_id=profile_id).first()
        assert resource.is_link_accepted == None
        resource.is_link_accepted = True
        resource.save()

        #Buscamos al organizer para poder mandarle un mensaje de que el arbitro
        #Aceptó formar parte del partido
        organizer = self.db.Organizer.qget(
                            profile_id=self.petitioner_profile_id,
        )

        #Buscamos la info de equipo1 y equipo2
        team1 = match.home_team
        team2 = match.away_team

        #Pretty things
        match_name = u"{0} vs {1}".format(team1.name, team2.name)

        match_date_humanize = arrow.get(
                match.match_date
            ).format('MMMM DD, YYYY', locale="es")

        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_observer_to_match(
            profile,
            organizer,
            match,
            team1,
            team2
        )

        return u"Aceptaste dirigir el partido {0} de la Fecha {1} - {2}".format(match_name, match.round+1, match_date_humanize)


    def accept_link_dt_to_team(self, profile_id, link_hash):
        profile_id = int(str(profile_id))
        
        dt = self.db.DT.get(profile_id=profile_id)
        assert dt, u"El profile {} no existe".format(str(profile_id))
        profiles = dt.user.profiles
        assert profiles, u"El profile {} no existe".format(str(profile_id))
        profile_ids = [p.profile_id for p in profiles]

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'dt', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        profile = self.db.Profile.query.get(profile_id)
        if profile.role != self.link_object:
            profile = profile.user.get_profile('dt')
            assert profile, "El Usuario no tiene un perfil de dt."
            assert profile.profile_id == self.link_object_id, u"No tienes autorización para utilizar este link"
            profile_id = profile.profile_id

        assert self.link_target == 'team', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        team = self.db.Team.query.get(linkage.link_target_id)

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"
        assert team.dt_id and team.dt_id in profile_ids, \
            u"El DT no forma parte del equipo"
        
        assert dt.role == 'dt'
        assert dt.is_active, u"El profile %s está inactivo" % str(profile_id)

        #Seteamos el id de DT en el equipo (dt_team)
        dtt = self.db.DtTeam.get(profile_id=profile_id,
            team_id=team.team_id)
        dtt.link = 'accepted'
        dtt.is_active = True
        dtt.save()
        #Buscamos al responsable para poder mandarle un mensaje de que el dt
        #Aceptó formar parte del equipo
        responsable = self.db.Profile.query.get(self.petitioner_profile_id)

        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_dt_to_team(
            dt,
            responsable,
            team
        )
        return u"Ahora formas parte del equipo {0}".format(team.name)

    def accept_link_referee_to_tournament(self, profile_id, link_hash):
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'referee', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        profile = self.db.Profile.query.get(profile_id)
        if profile.role != self.link_object:
            profile = profile.user.get_profile('referee')
            assert profile, "El Usuario no tiene un perfil de Referee."
            assert profile.profile_id == self.link_object_id, u"No tienes autorización para utilizar este link"
            profile_id = profile.profile_id
        
        assert self.link_object_id == profile_id, u"object_id != profile_id"
        assert self.link_target == 'tournament', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        tournament = self.db.Tournament.query.get(linkage.link_target_id)

        assert tournament, u"El torneo no existe"
        assert tournament.is_active, u"El torneo está inactivo"
        
        assert profile.role == 'referee'
        assert profile.is_active, u"El profile %s está inactivo" % str(profile_id)

        #Seteamos el id de observer en el torneo (observer_tournament)
        rt = self.db.TournamentReferee.query.filter_by(profile_id=profile_id,
            tournament_id=tournament.tournament_id).first()
        rt.is_link_accepted = True
        rt.is_active = True
        rt.link_id = self.id
        rt.save()
        
        #Buscamos al organizer para poder mandarle un mensaje de que el referee
        #Aceptó formar parte del torneo
        organizer = self.db.Profile.query.get(self.petitioner_profile_id)
        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_referee_to_tournament(
            profile,
            organizer,
            tournament
        )
        return u"Aceptaste dirigir en el torneo {0}".format(tournament.name)

    def accept_link_observer_to_tournament(self, profile_id, link_hash):
        profile_id = int(str(profile_id))
        
        now = datetime.datetime.now()

        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'observer', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        profile = self.db.Profile.query.get(profile_id)
        if profile.role != self.link_object:
            profile = profile.user.get_profile('observer')
            assert profile, "El Usuario no tiene un perfil de observador."
            assert profile.profile_id == self.link_object_id, u"No tienes autorización para utilizar este link"
            profile_id = profile.profile_id

        observer = profile

        assert self.link_target == 'tournament', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        tournament = self.db.Tournament.query.get(self.link_target_id)

        assert tournament, u"El torneo no existe"
        assert tournament.is_active, u"El torneo está inactivo"
        
        assert observer.role == 'observer'
        assert observer.is_active, u"El profile %s está inactivo" % str(profile_id)

        #Seteamos el id de observer en el torneo (observer_tournament)
        ot = self.db.TournamentObserver.query.filter_by(profile_id=profile_id,
            tournament_id=tournament.tournament_id).first()
        ot.link_id = self.id
        ot.is_link_accepted = True
        ot.save()

        #Buscamos al organizer para poder mandarle un mensaje de que el observer
        #Aceptó formar parte del torneo
        organizer = self.db.Profile.query.get(self.petitioner_profile_id)

        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_observer_to_tournament(
            observer,
            organizer,
            tournament
        )
        return u"Aceptaste observar partidos en el torneo {0}".format(tournament.name)


    def accept_link_responsable_to_team(self, profile_id, link_hash):
        u"""profile id del delegado que acepta ser responsable del equipo
            link_hash del link que tiene la información del acuerdo
        u"""
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'responsable', u"El link va dirigido a otro profile"
        assert self.link_object == 'profile', u"Tipo de object de link equivocado"
        profile = self.db.Profile.query.get(profile_id)
        #Puede que el usuario sea el correcto pero que este logueado con un perfil diferente
        # import pdb; pdb.set_trace()
        if profile.role != self.link_object:
            profile = profile.user.get_profile('responsable')
            assert profile.profile_id == self.link_object_id, u"No tienes autorización para utilizar este link"
            profile_id = profile.profile_id

        assert self.link_object_id == profile_id, u"Estas logueado con el perfil de usuario equivocado. Por favor deslogueate y vuelve a intentar."
        assert self.link_target == 'team', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        team = self.db.Team.query.get(linkage.link_target_id)

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"

        responsable = self.db.Profile.query.get(profile_id)
        assert responsable.role == 'responsable'
        assert responsable.is_active, \
            u"El profile %s está inactivo" % str(profile_id)
 
        team.set_responsable(responsable.profile_id, self.id)

        #Buscamos al organizador para poder mandarle un mensaje de que el
        #responsable aceptó formar parte del equipo
        organizer = self.db.Profile.query.get(self.petitioner_profile_id)
        #Enviamos el mensaje de aceptación
        self.db.Message().message__accepted_link_responsable_to_team(
            responsable,
            organizer,
            team
        )

        self.link_status = 'accepted'
        self.link_responded_at = datetime.datetime.now()
        self.link_response = u'Aceptó formar parte del equipo'
        self.save()
        return u"Aceptaste ser el encargado del equipo {0}".format(team.name)

    def accept_link_player_assist_confirmation(self, profile_id, link_hash):
        u"""profile id del jugador que acepta jugar el partido de la fecha
        u"""
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'player', u"El link va dirigido a otro profile"
        assert self.link_object == 'player', u"Tipo de object de link equivocado"
        assert self.link_object_id == profile_id, u"object_id != profile_id"
        assert self.link_target == 'match', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        player = self.db.profile.get_user_profile(
                profile_id,
                filters={'role': 'player'}
        )

        match = self.db.fixture.get(linkage.link_target_id)

        team = self.db.team.get_team(linkage.link_extra_information.get('team_id'))

        player_assistance = self.db.match.get_team_assistance_list(
            team.team_id, self.link_target_id
        )

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"

        player_profile = player['profile']
        assert player_profile.role == 'player'
        assert player_profile.is_active, \
            u"El profile %s está inactivo" % str(profile_id)

        self.db.match.set_assistance(
                match.match_id,
                'player',
                player_profile.profile_id,
                'yes'
            )

        #Buscamos al organizador para poder mandarle un mensaje de que el
        #responsable aceptó formar parte del equipo
        responsable = self.db.profile.get_user_profile(
                team.responsable_profile_id,
                filters={'role': 'responsable'}
        )

        the_other_team_id = match.home_team_id
        if the_other_team_id == team.team_id:
            the_other_team_id = match.away_team_id
        the_other_team = self.db.team.get_team(the_other_team_id)

        match_name = u"{0} vs {1}"
        match_date_humanize = arrow.get(
                match.match_date,
                'YYYY-MM-DDTHH:mm:ss'
            ).format('MMMM DD, YYYY', locale="es")
        #Enviamos el mensaje de aceptación
        # self.db.Message().message__accepted_player_assist_confirmation(
        #     player,
        #     responsable,
        #     the_other_team,
        #     match.match_date
        # )
        return u"Confirmaste tu presencia en el partido {0} a jugarse en {1}".format(match_name, match_date_humanize)

    def cancel_link_player_assist_confirmation(self, profile_id, link_hash):
        u"""profile id del jugador que cancela jugar el partido de la fecha
        u"""
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'player', u"El link va dirigido a otro profile"
        assert self.link_object == 'player', u"Tipo de object de link equivocado"
        assert self.link_object_id == profile_id, u"object_id != profile_id"
        assert self.link_target == 'match', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        player = self.db.profile.get_user_profile(
                profile_id,
                filters={'role': 'player'}
        )

        match = self.db.fixture.get(linkage.link_target_id)

        team = self.db.team.get_team(linkage.link_extra_information.get('team_id'))

        player_assistance = self.db.match.get_team_assistance_list(
            team.team_id, self.link_target_id
        )

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"

        player_profile = player['profile']
        assert player_profile.role == 'player'
        assert player_profile.is_active, \
            u"El profile %s está inactivo" % str(profile_id)

        self.db.match.set_assistance(
                match.match_id,
                'player',
                player_profile.profile_id,
                'no'
            )

        #Buscamos al responsable para poder mandarle un mensaje de que el
        #jugador no va a jugar el partido
        responsable = self.db.profile.get_user_profile(
                team.responsable_profile_id,
                filters={'role': 'responsable'}
        )

        the_other_team_id = match.home_team_id
        if the_other_team_id == team.team_id:
            the_other_team_id = match.away_team_id
        the_other_team = self.db.team.get_team(the_other_team_id)

        match_name = u"{0} vs {1}"
        match_date_humanize = arrow.get(
                match.match_date,
                'YYYY-MM-DDTHH:mm:ss'
            ).format('MMMM DD, YYYY', locale="es")
        #Enviamos el mensaje de cancelación
        # self.db.Message().message__cancelled_player_assist_confirmation(
        #     player,
        #     responsable,
        #     the_other_team,
        #     match.match_date
        # )
        return u"Confirmaste tu ausencia en el partido {0} a jugarse en {1}".format(match_name, match_date_humanize)

    def doubt_link_player_assist_confirmation(self, profile_id, link_hash):
        u"""profile id del jugador que duda jugar el partido de la fecha
        u"""
        profile_id = int(str(profile_id))
        

        now = datetime.datetime.now()

        linkage = self
        assert not self.link_responded_at, u"El link ya fue utilizado"
        assert not self.link_response, u"El link ya fue utilizado"
        assert self.receiver_type == 'player', u"El link va dirigido a otro profile"
        assert self.link_object == 'player', u"Tipo de object de link equivocado"
        assert self.link_object_id == profile_id, u"object_id != profile_id"
        assert self.link_target == 'match', u"Tipo de target de link equivocado"

        assert not self.expiration_date or self.expiration_date > now, \
        u"El link ha expirado"
        assert self.link_status == 'pending', u"El estado del link es incorrecto"

        player = self.db.profile.get_user_profile(
                profile_id,
                filters={'role': 'player'}
        )

        match = self.db.fixture.get(linkage.link_target_id)

        team = self.db.team.get_team(linkage.link_extra_information.get('team_id'))

        player_assistance = self.db.match.get_team_assistance_list(
            team.team_id, self.link_target_id
        )

        assert team, u"El equipo no existe"
        assert team.is_active, u"El equipo está inactivo"

        player_profile = player['profile']
        assert player_profile.role == 'player'
        assert player_profile.is_active, \
            u"El profile %s está inactivo" % str(profile_id)

        self.db.match.set_assistance(
                match.match_id,
                'player',
                player_profile.profile_id,
                'doubt'
            )

        #Buscamos al responsable para poder mandarle un mensaje de que el
        #jugador no sabe si va a jugar el partido
        responsable = self.db.profile.get_user_profile(
                team.responsable_profile_id,
                filters={'role': 'responsable'}
        )

        the_other_team_id = match.home_team_id
        if the_other_team_id == team.team_id:
            the_other_team_id = match.away_team_id
        the_other_team = self.db.team.get_team(the_other_team_id)

        match_name = u"{0} vs {1}"
        match_date_humanize = arrow.get(
                match.match_date,
                'YYYY-MM-DDTHH:mm:ss'
            ).format('MMMM DD, YYYY', locale="es")
        #Enviamos el mensaje de cancelación
        # self.db.Message().message__doubt_player_assist_confirmation(
        #     player,
        #     responsable,
        #     the_other_team,
        #     match.match_date
        # )
        return u"Aviso de duda para jugar en el partido {0} a jugarse en {1} enviado correctamente".format(match_name, match_date_humanize)