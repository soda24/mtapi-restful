# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta

import sqlalchemy as sa
import datetime

class PositionTableJsonSerializer(JsonSerializer):
    __json_public__ = [  
    'id', 'tournament_id', 'round', 'team_id', 'position', 'last_position',
    'played', 'won', 'drawn', 'lost', 'goals_for', 'goals_against',
    'goal_difference', 'yellow_cards', 'red_cards', 'points', 'status',
    ]


class PositionTable(PositionTableJsonSerializer, BaseModel):

    __tablename__ = 'position_tables'
    __schema__    = 'tournament_management'

    id              = sa.Column(sa.Integer, primary_key=True)
    tournament_id   = sa.Column(sa.Integer)
    round           = sa.Column(sa.Integer)
    team_id         = sa.Column(sa.Integer)
    position        = sa.Column(sa.Integer)
    last_position   = sa.Column(sa.Integer)
    played          = sa.Column(sa.Integer)
    won             = sa.Column(sa.Integer)
    drawn           = sa.Column(sa.Integer)
    lost            = sa.Column(sa.Integer)
    goals_for       = sa.Column(sa.Integer)
    goals_against   = sa.Column(sa.Integer)
    goal_difference = sa.Column(sa.Integer)
    yellow_cards    = sa.Column(sa.Integer)
    red_cards       = sa.Column(sa.Integer)
    points          = sa.Column(sa.Integer)
    status          = sa.Column(sa.CHAR(1))
    updated_at      = sa.Column(sa.DateTime)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def tournament(self):
        if self.tournament_id:
            return self.db.Tournament.query.get(self.tournament_id)

    def clean(self):
        self.position        = 0
        self.last_position   = 0
        self.played          = 0
        self.won             = 0
        self.drawn           = 0
        self.lost            = 0
        self.goals_for       = 0
        self.goals_against   = 0
        self.goal_difference = 0
        self.yellow_cards    = 0
        self.red_cards       = 0
        self.points          = 0
        self.status          = 'pending'
        self.save()

class TournamentPositionTable(list):

    def __init__(self):
        self.tournament_id = None
        self.round = None

    @classmethod
    def all(cls, tournament_id=None):
        assert tournament_id, "tournament_id not provided"
        tpt = cls()
        tpt.tournament_id = tournament_id
        try:
            query = PositionTable.query.filter_by(tournament_id=tournament_id)
            for ttp in query.all():
                tpt.append(ttp)
        except Model.DoesNotExist:
            pass
        tpt.sort(key=lambda x: (x.position)) #sorted by match number
        return tpt

    @classmethod
    def get(cls, tournament_id=None, vround=-1):
        assert tournament_id, "tournament_id not provided"
        tpt = cls()
        tpt.tournament_id = tournament_id
        if vround < 0:
            return
            # return cls.skel(tournament_id)
        tpt.round = vround
        try:
            query = PositionTable.query.filter_by(tournament_id=tournament_id, round=vround)
            for ttp in query.all():
                tpt.append(ttp)
        except Model.DoesNotExist:
            pass
        tpt.sort(key=lambda x: (x.position)) #sorted by match number
        return tpt

    @classmethod
    def get_last_position(cls, tournament_id):
        assert tournament_id, "tournament_id not provided"
        tpt = cls()
        tpt.tournament_id = tournament_id
        vround = (len(tpt.tournament.team_id)-2) or 0
        if vround < 0:
            return
            # return cls.skel(tournament_id)
        tpt.round = vround
        try:
            query = PositionTable.query.filter_by(tournament_id=tournament_id, round=vround)
            for ttp in query.all():
                tpt.append(ttp)
        except Model.DoesNotExist:
            pass
        tpt.sort(key=lambda x: (x.position)) #sorted by match number
        return tpt

    @classmethod
    def get_next_playable_round(cls, tournament_id=None):
        assert tournament_id, "tournament_id not provided"
        tpt = cls()
        tpt.round = None
        tpt.tournament_id = tournament_id

        query = PositionTable.query.filter_by(tournament_id=tournament_id)

        #Traigo TODA los TournamentTeamRoundPosition
        whole_table = [ttp for ttp in query.all()]

        pt = filter(lambda x: x.status == "complete", whole_table)
        #Si no hay ninguna marcada como completa, devolvemos la primer fecha
        if not pt:
            vround = 0
            pt = filter(lambda x: x.round == vround, whole_table)
        else:
            vround = max([x.round for x in pt])
            pt = filter(lambda x: x.round == vround, pt) #filter last round

        tpt.round = vround

        for r in pt:
            tpt.append(r)
        tpt.sort(key=lambda x: (x.position)) #sorted by match number
        return tpt

    @property
    def tournament(self):
        if self.tournament_id:
            return self.db.Tournament.query.get(self.tournament_id)

    @property
    def next_round(self):
        return TournamentPositionTable.get(tournament_id=self.tournament_id, vround=self.round+1)

    @property
    def prev_round(self):
        if self.round > 0:
            return TournamentPositionTable.get(tournament_id=self.tournament_id, vround=self.round-1)

    def clean(self):
        for team_position in self:
            team_position.clean()

    def process_position_table(self):
        """
        """
        print "TODO->Agregar fecha ultima actualizacion y quien lo actualizo" 
        tournament = self.tournament
        assert tournament, "El torneo no existe"
        # assert tournament.status == 'inprogress', \
        # """No se puede procesar la tabla de posiciones \
        # de un torneo que no está en progreso"""
        lrpt = self.prev_round or self[:] #prev_round_position_table
        lrpt = dict([[pt.team_id, pt] for pt in lrpt]) # prev_round_position_table convert to dict with team_id as key

        #Get Actual FixtureRound
        matches_data = self.db.FixtureRound.get(self.tournament_id, self.round)
        # convert Actual Fixture Round to a Team->Match Dict to access data by team_id
        matches = dict([[match.home_team_id, match] for match in matches_data])
        matches.update(dict([[match.away_team_id, match] for match in matches_data]))


        for team_position in self:
            #Clear old values
            team_position.clean()
            match = matches.get(team_position.team_id)
            #traer la posicion anterior del equipo
            # import pdb; pdb.set_trace()
            prev_team_position = lrpt.get(team_position.team_id)
            if match.observation_status == 'c':

                #Definir sin el partido fue local o visitante para saber de donde sacar las estadisticas
                home = match.home_team_id == team_position.team_id
                if home:
                    won   = match.home_goals  > match.away_goals
                    drawn = match.home_goals == match.away_goals
                    lost  = match.home_goals  < match.away_goals
                    goals_for = match.home_goals or 0
                    goals_against = match.away_goals or 0
                    goal_difference = (match.home_goals or 0) - (match.away_goals or 0)
                    yellow_cards = match.home_yellow_cards or 0
                    red_cards = match.home_red_cards or 0
                else:
                    won   = match.away_goals  > match.home_goals
                    drawn = match.away_goals == match.home_goals
                    lost  = match.away_goals  < match.home_goals
                    goals_for = match.away_goals or 0
                    goals_against = match.home_goals or 0
                    goal_difference = (match.away_goals or 0) - (match.home_goals or 0)
                    yellow_cards = match.away_yellow_cards or 0
                    red_cards = match.away_red_cards or 0

                team_position.last_position =    prev_team_position.position or 0
                team_position.played =           (prev_team_position.played or 0)+1
                team_position.won =              (prev_team_position.won or 0) + (won and 1 or 0)
                team_position.drawn =            (prev_team_position.drawn or 0) + (drawn and 1 or 0)
                team_position.lost =             (prev_team_position.lost or 0) + (lost and 1 or 0)
                team_position.goals_for =        (prev_team_position.goals_for or 0) + goals_for
                team_position.goals_against =    (prev_team_position.goals_against or 0) + goals_against
                team_position.goal_difference =  (prev_team_position.goal_difference or 0) + goal_difference
                team_position.yellow_cards =     (prev_team_position.yellow_cards or 0) + yellow_cards
                team_position.red_cards =        (prev_team_position.red_cards or 0) + red_cards
                team_position.points =           (prev_team_position.points or 0) + (won and 3 or drawn and 1 or lost and 0)
                team_position.status =           'complete'
            else:
                team_position.last_position =   prev_team_position.position
                team_position.played =          prev_team_position.played
                team_position.won =             prev_team_position.won
                team_position.drawn =           prev_team_position.drawn
                team_position.lost =            prev_team_position.lost
                team_position.goals_for =       prev_team_position.goals_for
                team_position.goals_against =   prev_team_position.goals_against
                team_position.goal_difference = prev_team_position.goal_difference
                team_position.yellow_cards =    prev_team_position.yellow_cards
                team_position.red_cards =       prev_team_position.red_cards
                team_position.points =          prev_team_position.points
                team_position.status =          'pending'
        
        #Calculate positions
        positions = sorted(self, key=lambda x: (x.points, x.won, x.drawn, x.goals_for, x.goal_difference, x.goals_against*-1), reverse=True)
        last = {}
        last_hash = ''
        position_numbers = sorted(list(range(1, len(positions)+1)), reverse=True)
        #Dos o mas equipos pueden compartir una misma posicion
        #Este pedazo de codigo construye un hash con todos los valores que
        #le dan una posicion a un equipo y si otro equipo tiene el mismo hash
        #entonces se le asigna ese valor al equipo, si no, se pasa al siguiente
        #valor.
        for team_position in positions:
            team_position_hash = '{}{}{}{}{}{}'.format(
                team_position.points, team_position.won, team_position.drawn,
                team_position.goals_for, team_position.goal_difference,
                team_position.goals_against*-1
            )
            if not last or team_position_hash != last_hash:
                team_position.position = position_numbers.pop()
            else:
                team_position.position = last.position
            last = team_position
            last_hash = team_position_hash

            #team_postion.updated_by = 
            team_position.save()
        return True

    @classmethod
    def initialize_position_table(cls, tournament_id):
        tournament = cls.db.Tournament.query.get(tournament_id)
        print tournament
        assert tournament, "El torneo {} no existe".format(tournament.tournament_id)
        assert tournament.status not in ('complete', 'cancelled'), \
        "No se puede inicializar una tabla de posiciones \
en un torneo con status {}".format(tournament.status)
        team_ids = [row[0] for row in db.session.query(cls.db.Team.id).filter_by(tournament_id=tournament_id).all()]

        re_match_factor = (tournament.match_rematch and 2) or 1
        rounds = (len(team_ids)-1)*re_match_factor
        for n in range(rounds):
            for team_id in team_ids:
                tp = PositionTable()
                tp.tournament_id   = tournament.tournament_id
                tp.round        = n
                tp.team_id         = team_id
                tp.position        = 0
                tp.last_position   = 0
                tp.played          = 0
                tp.won             = 0
                tp.drawn           = 0
                tp.lost            = 0
                tp.goals_for       = 0
                tp.goals_against   = 0
                tp.goal_difference = 0
                tp.points          = 0
                tp.yellow_cards    = 0
                tp.red_cars        = 0
                tp.status          = 'p'
                tp.updated_at      = datetime.datetime.now()
                tp.save()
        # except Exception as error:
        #     print error.message
        #     cls.__initialize_position_table_rollback(tournament)
        #     raise

    def __initialize_position_table_post_hook(self, tournament):
        position_table = self.get_whole_position_table(tournament.tournament_id)

        re_match_factor = (tournament.match_rematch and 2) or 1
        rows = (len(tournament.team_id)-1)*re_match_factor*len(tournament.team_id)
        assert len(position_table) == rows, \
        """La cantidad de fechas de la tabla de posición inicializada es """
        """menor a la calculada según el fixture del torneo."""
        return True
