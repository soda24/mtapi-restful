# -*- coding: utf-8 -*-
from flask_security import UserMixin, RoleMixin
from flask import current_app as app

import sqlalchemy as sa
from sqlalchemy.orm.session import object_session

from flc.core import db, BaseModel, limits
from flc.helpers import JsonSerializer
from flc import settings
# from flc.db.connector import BaseModel

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature

# from passlib.apps import custom_app_context as pwd_context
from passlib.context import CryptContext
import base64
import hashlib
import hmac

import uuid

import datetime

from dateutil.tz import tzlocal
from dateutil.relativedelta import relativedelta

pwd_context = CryptContext(schemes='sha512_crypt', default='sha512_crypt')

def get_hmac(password):
    h = hmac.new(settings.SECURITY_PASSWORD_SALT.encode('utf-8'), password.encode('utf-8'), hashlib.sha512)
    return base64.b64encode(h.digest())


class RoleJsonSerializer(JsonSerializer):
    __json_public__ = ['role_id', 'id', 'name', 'description']

class Role(RoleJsonSerializer, BaseModel):
    __tablename__ = 'roles'
    id            = sa.Column('role_id', sa.Integer, primary_key=True)
    name          = sa.Column(sa.String(100))
    description   = sa.Column(sa.String(255))

    @property
    def role_id(self):
        return self.id

class UserJsonSerializer(JsonSerializer):
    __json_public__ = [
        'id', 'user_id', 'email', 'first_name', 'last_name', 'name',
        'id_number', 'birthday', 'avatar_path', 'is_active', 'is_validated',
    ]

class User(UserJsonSerializer, BaseModel):

    __tablename__ = 'users'


    id            = sa.Column('user_id', sa.Integer, primary_key=True)
    email         = sa.Column(sa.String(255), unique=True)
    login_name    = sa.Column(sa.String(255), unique=True)
    password      = sa.Column(sa.String(255))
    first_name    = sa.Column(sa.String(120))
    last_name     = sa.Column(sa.String(120))
    id_number     = sa.Column(sa.Integer())
    birthday      = sa.Column(sa.Date())
    avatar_path   = sa.Column(sa.Text())
    created_at       = sa.Column(sa.Date(), default=datetime.datetime.now)
    created_by       = sa.Column(sa.Integer())
    updated_at       = sa.Column(sa.DateTime(), onupdate=datetime.datetime.now, default=datetime.datetime.now)
    updated_by       = sa.Column(sa.Integer)
    is_active        = sa.Column(sa.Boolean(), default=True)
    is_validated     = sa.Column(sa.Boolean())
    validated_at     = sa.Column(sa.DateTime())
    last_login_at    = sa.Column(sa.DateTime())
    current_login_at = sa.Column(sa.DateTime())
    last_login_ip    = sa.Column(sa.CHAR(20))
    current_login_ip = sa.Column(sa.CHAR(20))
    login_count      = sa.Column(sa.Integer())

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def user_id(self):
        return self.id

    def hash_password(self, password):
        signed = get_hmac(password).decode('ascii')
        self.password = pwd_context.encrypt(signed)

    def verify_password(self, password):
        password = get_hmac(password)
        return pwd_context.verify(password, self.password)

    def generate_auth_token(self, expiration = 25000):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
            print data
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

    @property
    def name(self):
        if self.first_name and self.last_name:
            return u'{} {}'.format(self.first_name, self.last_name).strip()
        else:
            return self.email.lower()

    def info(self):
        user = self.to_json()
        del user['password']
        return user

    def get_profile(self, role):
        profiles = filter(lambda x: x.role == role, self.profiles.all())
        if profiles:
            return profiles[0]

    def set_global(self, **kwargs):
        for key, value in kwargs.items():
            if key in ('user_id', 'id'):
                continue
            if key in self.__json_public__:
                if key in ('start_date', 'created_at', 'last_updated_at',):
                    if 'T' in value and 'Z' == value[-1]:
                        value = datetime.datetime.strptime(value[:-5], '%Y-%m-%dT%H:%M:%S')
                    elif len(value) == 10:
                        value = datetime.datetime.strptime(value, '%Y-%m-%d')
                elif key in ('birthday',) and value:
                    if 'T' in value and 'Z' == value[-1]:
                        value = str(datetime.datetime.strptime(value[:-5], '%Y-%m-%dT%H:%M:%S').date())
                    elif len(value) == 10:
                        value = str(datetime.datetime.strptime(value, '%Y-%m-%d').date())
            setattr(self, key, value)
        return self


class ProfileJsonSerializer(JsonSerializer):
    __json_public__ = ['profile_id', 'role', 'user_id', 'is_active']

class Profile(ProfileJsonSerializer, BaseModel):
    __tablename__ = 'profiles'

    id      = sa.Column('profile_id', sa.Integer, primary_key=True)
    role    = sa.Column(sa.String(120))
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.user_id'))
    is_active = sa.Column(sa.Boolean, default=True)
    user    = sa.orm.relationship('User', backref=sa.orm.backref('profiles', lazy='dynamic'))

    def generate_auth_token(self, expiration = 6000):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
            print data
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        profile = Profile.query.get(data['id'])
        return profile

    def info(self):
        return dict(
            name=self.user.name,
            email=self.user.email,
            id_number=self.user.id_number,
            birthday=self.user.birthday,
            avatar_path=self.user.avatar_path,
            profile_id=self.id,
            role=self.role,
            user_id=self.user_id,
            id=self.user_id
        )

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def profile_id(self):
        return self.id

    @property
    def user_profile(self):
        return {
            'user': self.user.info(),
            'profile': self.to_json()
        }

class UserRegistrationTokenJsonSerializer(JsonSerializer):
    __json_public__ = [
        'user_id', 'profile_id', 'registration_token', 'expiration_date'
    ]

class UserRegistrationToken(UserRegistrationTokenJsonSerializer, UserMixin, BaseModel):
    __tablename__ = 'registration_tokens'
    id            = sa.Column(sa.Integer, primary_key=True)

    user_id       = sa.Column(sa.Integer, sa.ForeignKey('users.user_id'))
    user          = sa.orm.relationship('User', backref=sa.orm.backref('registration_token', uselist=False))
    profile_id    = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    profile       = sa.orm.relationship('Profile', backref=sa.orm.backref('registration_token', uselist=False))

    registration_token = sa.Column(sa.String(100))
    expiration_date    = sa.Column(sa.DateTime)

    @property
    def token(self):
        return self.registration_token

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class BasicUserProfile(object):

    @classmethod
    def __create_user(cls, email):
        now = datetime.datetime.now(tzlocal())

        #generate new primary key uuid
        user = User()

        #Setting defaults
        user.is_active = False
        user.created_at = now
        user.last_updated_at = now
        user.password = pwd_context.encrypt(str(uuid.uuid4()))
        user.email = email.lower()
        user.login_name = email.lower()
        user.validated = False
        user.avatar_path = 'user/no_avatar.png'
        user.save()
        return user

    @classmethod
    def __create_profile(cls, user_id, role):

        now = datetime.datetime.now(tzlocal())
        profile = Profile()
        profile.user_id = user_id
        profile.role = role
        
        #Assertions - checks
        assert profile.role and profile.role in \
        limits.PROFILE_ROLES

        #Setting defaults
        profile.is_active = True
        profile.created_at = now

        profile.save()
        return profile

    @classmethod
    def create(cls, email, role):
        #Assertions - checks
        assert email, 'email es mandatorio'
        user = cls.__create_user(email)
        assert user and user.user_id, "Hubo un problema creando al usuario: %s" % email

        profile = cls.__create_profile(user.user_id, role)
        assert profile and profile.profile_id, "Hubo un problema creando el perfil del usuario: %s" % email

        registration_token = UserRegistrationToken.query.filter_by(user_id=user.user_id).first()
        if registration_token:
            registration_token.delete()

        registration_token = UserRegistrationToken()
        registration_token.user_id = user.user_id
        registration_token.profile_id = profile.profile_id
        registration_token.expiration_date = datetime.datetime.now(tzlocal())+datetime.timedelta(days=5)
        registration_token.registration_token = get_hmac(str(uuid.uuid4()))
        registration_token.save()

        return profile, registration_token.token



# class RefereeJsonSerializer(JsonSerializer):
#     __json_public__ = ['user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']


# class Referee(RefereeJsonSerializer, Profile):

#     @property
#     def user(self):
#         if self.profile_id:
#             return User.get(user_id=self.user_id)

#     @property
#     def tournament_rels(self):
#         query = self.sa.TournamentReferee.select().where(profile_id==self.profile_id, role_id==1)
#         tournaments = [t for t in query.all()]
#         return tournaments

#     @property
#     def tournaments(self):
#         query = self.sa.TournamentReferee.objects.filter(profile_id=self.profile_id)
#         tournaments = [t.tournament for t in query.all()]
#         return tournaments

#     def info(self, **kwargs):
#         assert self.profile_id
#         if self.role != 'referee':
#             return
#         info = {
#             'first_name': self.user.first_name,
#             'last_name': self.user.last_name,
#             'name': self.user.name,
#             'email': self.user.email,
#             'user_id': self.user_id,
#             'is_active': self.is_active,
#             'profile_id': self.profile_id,
#             'role': self.role,
#             'avatar_path': self.user.avatar_path
#         }
#         if 'tournament_id' in kwargs:
#             tr = self.sa.TournamentReferee.get(profile_id=self.profile_id, tournament_id=kwargs.get('tournament_id'))
#             if tr:
#                 info['tournament_link'] = tr.link
#         return info

#     def pre_set_tournament(self, tournament_id):
#         """Crea una relación inactiva de un referee con un torneo """ \
#         """que tendrá que ser validada posteriormente"""
#         ot = self.sa.RefereeTournament.associate(self.profile_id, tournament_id)

# class ObserverJsonSerializer(JsonSerializer):
#     __json_public__ = ['user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']

# class Observer(ObserverJsonSerializer, Profile):

#     @property
#     def user(self):
#         if self.profile_id:
#             return User.objects.get(user_id=self.user_id)

#     @property
#     def tournament_rels(self):
#         query = self.sa.TournamentObserver.objects.filter(profile_id=self.profile_id)
#         tournaments = [t for t in query.all()]
#         return tournaments

#     @property
#     def tournaments(self):
#         query = self.sa.TournamentObserver.objects.filter(profile_id=self.profile_id)
#         tournaments = [t.tournament for t in query.all()]
#         return tournaments

#     def info(self, **kwargs):
#         assert self.profile_id
#         if self.role != 'observer':
#             return
#         info = {
#             'first_name': self.user.first_name,
#             'last_name': self.user.last_name,
#             'name': self.user.name,
#             'email': self.user.email,
#             'user_id': self.user_id,
#             'is_active': self.is_active,
#             'profile_id': self.profile_id,
#             'role': self.role,
#             'avatar_path': self.user.avatar_path
#         }
#         if 'tournament_id' in kwargs:
#             try:
#                 tr = self.sa.TournamentObserver.get(profile_id=self.profile_id, tournament_id=kwargs.get('tournament_id'))
#                 info['tournament_link'] = tr.link
#             except Model.DoesNotExist:
#                 info['tournament_link'] = None
#                 pass
#         return info

#     def next_match(self):
#         query = self.sa.MatchResource.filter(profile_id=self.profile_id)
#         mrs = [mr for mr in query.all() if mr.status in ('pending', 'inprogress', None)]
#         matches = [mr.match for mr in mrs]
#         if matches:
#             matches.sort(key=lambda x: x.match_date)
#             return matches[0]

#     def pre_set_tournament(self, tournament_id):
#         """Crea una relación inactiva de un observer con un torneo """ \
#         """que tendrá que ser validada posteriormente"""
#         ot = self.sa.ObserverTournament.associate(self.profile_id, tournament_id)

class OrganizerJsonSerializer(JsonSerializer):
    __json_public__ = ['id', 'user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']

class Organizer(OrganizerJsonSerializer, Profile):

    @classmethod
    def qget(cls, profile_id):
        profile = db.session.query(Profile).filter(Profile.id==profile_id, Profile.role=='organizer').first()
        return profile

    def info(self, **kwargs):
        info = {
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'name': self.user.name,
            'email': self.user.email,
            'user_id': self.user_id,
            'is_active': self.is_active,
            'profile_id': self.profile_id,
            'role': self.role,
            'avatar_path': self.user.avatar_path
        }
        if 'tournament_id' in kwargs:
            try:
                tr = self.sa.TournamentObserver.get(profile_id=self.profile_id, tournament_id=kwargs.get('tournament_id'))
                info['tournament_link'] = tr.link
            except Model.DoesNotExist:
                info['tournament_link'] = None
                pass
        return info

class ResponsableJsonSerializer(JsonSerializer):
    __json_public__ = ['id', 'user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']

class Responsable(ResponsableJsonSerializer, Profile):

    def info(self, **kwargs):
        assert self.profile_id
        if self.role != 'responsable':
            return
        info = {
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'name': self.user.name,
            'email': self.user.email,
            'user_id': self.user_id,
            'is_active': self.is_active,
            'profile_id': self.profile_id,
            'role': self.role,
            'avatar_path': self.user.avatar_path
        }
        return info


class RefereeJsonSerializer(JsonSerializer):
    __json_public__ = ['id', 'user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']

class Referee(RefereeJsonSerializer, Profile):

    @classmethod
    def qget(cls, profile_id):
        profile = db.session.query(Profile).filter(Profile.id==profile_id, Profile.role=='referee').first()
        return profile

class ObserverJsonSerializer(JsonSerializer):
    __json_public__ = ['id', 'user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active']

class Observer(ObserverJsonSerializer, Profile):

    @classmethod
    def qget(cls, profile_id):
        profile = db.session.query(Profile).filter(Profile.id==profile_id, Profile.role=='observer').first()
        return profile

class PlayerJsonSerializer(JsonSerializer):
    __json_public__ = ['id', 'user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active']

class Player(PlayerJsonSerializer, Profile):

    @classmethod
    def qget(cls, profile_id):
        profile = db.session.query(Profile).filter(Profile.id==profile_id, Profile.role=='player').first()
        return profile


# class PlayerJsonSerializer(JsonSerializer):
#     __json_public__ = ['user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at']

# class Player(PlayerJsonSerializer, Profile):

#     @property
#     def user(self):
#         if self.profile_id:
#             return User.objects.get(user_id=self.user_id)

#     @property
#     def player_teams(self):
#         return [t for t in self.sa.PlayerTeam.objects.filter(profile_id=self.profile_id).all()]

#     @property
#     def teams(self):
#         return [t.team for t in self.sa.PlayerTeam.objects.filter(profile_id=self.profile_id).all()]

#     @property
#     def active_teams(self):
#         return [t.team for t in self.sa.PlayerTeam.objects.filter(profile_id=self.profile_id).all() if t.is_active]

#     def info(self, **kwargs):
#         assert self.profile_id
#         if self.role != 'player':
#             return
#         info = {
#             'first_name': self.user.first_name,
#             'last_name': self.user.last_name,
#             'name': self.user.name,
#             'email': self.user.email,
#             'user_id': self.user_id,
#             'is_active': self.is_active,
#             'profile_id': self.profile_id,
#             'role': self.role,
#             'avatar_path': self.user.avatar_path
#         }
#         return info

#     def set_global(self, **kwargs):
#         user = self.user
#         for key, value in kwargs.items():
#             if key in self.__json_public__:
#                 if key in ('start_date', 'created_at', 'last_updated_at'):
#                     if 'T' in value and 'Z' == value[-1]:
#                         value = datetime.datetime.strptime(value[:-5], '%Y-%m-%dT%H:%M:%S')
#             if key in self.user.__json_public__:
#                 if key in ('start_date', 'created_at', 'last_updated_at'):
#                     if 'T' in value and 'Z' == value[-1]:
#                         value = datetime.datetime.strptime(value[:-5], '%Y-%m-%dT%H:%M:%S')
#                 setattr(user, key, value)
#         user.save()
#         self.save()

# class DTJsonSerializer(JsonSerializer):
#     __json_public__ = ['user_id', 'profile_id', 'first_name', 'last_name', 'role', 'is_active', 'created_at', 'updated_at', ]

# class DT(DTJsonSerializer, Profile):

#     @property
#     def user(self):
#         if self.profile_id:
#             return User.objects.get(user_id=self.user_id)

#     def info(self, **kwargs):
#         assert self.profile_id
#         if self.role != 'dt':
#             return
#         info = {
#             'first_name': self.user.first_name,
#             'last_name': self.user.last_name,
#             'name': self.user.name,
#             'email': self.user.email,
#             'user_id': self.user_id,
#             'is_active': self.is_active,
#             'profile_id': self.profile_id,
#             'role': self.role,
#             'avatar_path': self.user.avatar_path
#         }
#         return info

#     def pre_set_team(self, team_id):
#         """Crea una relación inactiva de un dt con un equipo """ \
#         """que tendrá que ser validada posteriormente"""
#         dtt = self.sa.DtTeam.associate(self.profile_id, team_id)