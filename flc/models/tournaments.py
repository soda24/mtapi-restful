# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta

from flc.core.exceptions import *

import sqlalchemy as sa
import datetime

class TournamentJsonSerializer(JsonSerializer):
    __json_public__ = [ 
    'tournament_id', 'tournament_line_id', 'tournament_type_id', 
    'setup_stage', 'is_setup_complete', 'organizer_profile_id', 'name',
    'location', 'min_players', 'max_players', 'max_players_in_field',
    'min_players_in_field', 'max_players_age', 'min_players_age',
    'max_changes_allowed', 'match_day_of_week', 'match_half_length', 
    'match_recess_length', 'match_between_match_length', 'match_rematch', 
    'can_edit', 'status', 'start_date', 'end_date', 'is_active',
    'is_fixture_complete', 'terms_and_conditions_id', 'updated_at',
    'updated_by', 'created_at', 'created_by',
    'teams_amount', 'id'
    ]

class Tournament(TournamentJsonSerializer, BaseModel):

    __tablename__ = 'tournaments'
    __schema__ = 'tournament_management'

    id                        = sa.Column('tournament_id', sa.Integer, primary_key=True)
    tournament_line_id        = sa.Column(sa.Integer)
    tournament_type_id        = sa.Column(sa.Integer)
    organizer_profile_id      = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    organizer                 = sa.orm.relationship('Profile', backref=sa.orm.backref('tournaments', lazy='dynamic'), foreign_keys=[organizer_profile_id])
    name                      = sa.Column(sa.String(120))
    location                  = sa.Column(sa.String(120))
    min_players               = sa.Column(sa.Integer)
    max_players               = sa.Column(sa.Integer)
    max_players_in_field      = sa.Column(sa.Integer)
    min_players_in_field      = sa.Column(sa.Integer)
    max_players_age           = sa.Column(sa.Integer)
    min_players_age           = sa.Column(sa.Integer)
    max_changes_allowed       = sa.Column(sa.Integer)
    match_day_of_week         = sa.Column(sa.Integer)
    match_half_length         = sa.Column(sa.Integer)
    match_recess_length       = sa.Column(sa.Integer)
    match_between_match_length = sa.Column(sa.Integer)
    match_rematch             = sa.Column(sa.Boolean)
    teams_amount              = sa.Column(sa.Integer)
    can_edit                  = sa.Column(sa.Boolean)
    status                    = sa.Column(sa.String(10))
    start_date                = sa.Column(sa.Date)
    end_date                  = sa.Column(sa.Date)
    setup_stage               = sa.Column(sa.Integer)
    is_setup_complete         = sa.Column(sa.Integer)
    is_active                 = sa.Column(sa.Boolean)
    is_fixture_complete       = sa.Column(sa.Boolean)
    terms_and_conditions_id   = sa.Column(sa.Integer)
    updated_at                = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    updated_by                = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    created_at                = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by                = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))

    referees                  = sa.orm.relationship('Profile', secondary='tournaments_referees')
    observers                 = sa.orm.relationship('Profile', secondary='tournaments_observers')

    @property
    def tournament_id(self):
        return self.id

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def start_date_formal(self):
        if self.start_date:
            return self.start_date.strftime('%d/%m/%Y')

    @property
    def matches(self):
        return [m for m in self.db.FixtureMatch.objects.filter(tournament_id=self.tournament_id).all()]

    @property
    def gfixture(self):
        return [m for m in self.db.FixtureMatch.filter(tournament_id=self.tournament_id).all()]

    @property
    def teams(self):
        if self.team_id:
            return [t for t in self.db.Team.objects.filter(tournament_id=self.tournament_id).all()]

    def set_global(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__json_public__:
                if key in ('id', 'tournament_id'):
                    continue
                if value and key in ('end_date', 'start_date', 'created_at', 'updated_at'):
                    if 'T' in value:
                        value = datetime.datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                setattr(self, key, value)

    def initialize(self):
        self.status = 'inprogress'
        self.save()

    def set_observer(self, profile_id):
        u"""Inserta o actualiza la relación de un observer con un equipo"""
        profile_id = uuid.UUID(str(profile_id))
        observer = self.db.Observer.get(profile_id=profile_id)
        assert observer.role == 'observer', u"El profile_id no pertenece a un observador"
        assert self.is_active, u"El equipo no está activo"
        if not observer.profile_id in self.observer_id:
            self.observer_id.add(observer.profile_id)
            self.save()

    def set_sportcenter(self, sportcenter_id):
        u"""Inserta o actualiza la relación de un observer con un equipo"""
        sportcenter_id = uuid.UUID(str(sportcenter_id))
        sportcenter = self.db.Sportcenter.get(sportcenter_id=sportcenter_id)
        assert self.is_active, u"El equipo no está activo"
        assert sportcenter.is_active, u"El centro deportivo se encuentra inactivo"
        if not sportcenter.sportcenter_id in self.sportcenter_id:
            self.sportcenter_id.add(sportcenter.sportcenter_id)
            self.save()

    def unset_sportcenter(self, sportcenter_id):
        u"""Inserta o actualiza la relación de un observer con un equipo"""
        sportcenter_id = uuid.UUID(str(sportcenter_id))
        assert self.is_active, u"El equipo no está activo"
        if sportcenter_id in self.sportcenter_id:
            self.sportcenter_id.remove(sportcenter_id)
            self.save()

    def destroy_fixture(self):
        assert self.start_date > datetime.datetime.now(), \
            "El torneo {} ya está comenzado, no puedes eliminar el fixture.".format(
                self.name
            )
        fixture = self.gfixture
        assert fixture, "El torneo {} no tiene un fixture asociado".format(
            tournament.name
            )

        now = datetime.datetime.now()

        q = self.db.FixtureMatch.objects.filter(tournament_id=self.tournament_id)
        for r in q.all():
            r.delete()

        self.destroy_position_table()

        self.can_edit = True
        self.last_updated_at = now
        self.fixture = None
        self.save()

    def generate_fixture(self):
        print "VALIDAR QUE EL TORNEO SEA DEL ORGANIZADOR"
        return self.db.Fixture.generate(self.tournament_id)

    def destroy_position_table(self):
        for q in self.db.TournamentTeamPosition.objects.filter(tournament_id=self.tournament_id).all():
            q.delete()
        return True


class TournamentSportcenter(BaseModel):

    __tablename__ = 'tournaments_sportcenters'
    __schema__ = 'tournament_management'

    tournament_id  = sa.Column(sa.Integer, sa.ForeignKey('tournaments.tournament_id'), primary_key=True)
    sportcenter_id = sa.Column(sa.Integer, sa.ForeignKey('sportcenters.sportcenter_id'), primary_key=True)
    created_at     = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by     = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class SportcenterJsonSerializer(JsonSerializer):
    __json_public__ = [ 
    'id', 'sportcenter_id', 'latitude', 'longitude', 'name', 'description', 'fields',
    'country', 'state', 'address', 'city', 'is_active',
    ]

class Sportcenter(SportcenterJsonSerializer, BaseModel):

    __tablename__ = 'sportcenters'
    __schema__ = 'tournament_management'

    id          = sa.Column('sportcenter_id', sa.Integer, primary_key=True)
    latitude    = sa.Column(sa.Float)
    longitude   = sa.Column(sa.Float)
    name        = sa.Column(sa.String(400))
    description = sa.Column(sa.Text)
    fields      = sa.Column(sa.Integer)
    country     = sa.Column(sa.CHAR(2))
    state       = sa.Column(sa.String(200))
    address     = sa.Column(sa.String(400))
    city        = sa.Column(sa.String(100))
    is_active   = sa.Column(sa.Boolean)
    created_at  = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by  = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'),)
    updated_at  = sa.Column(sa.DateTime, onupdate=datetime.datetime.now)
    updated_by  = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'))
    tournaments = sa.orm.relationship('Tournament', secondary='tournaments_sportcenters', backref='sportcenters')

    @property
    def sportcenter_id(self):
        return self.id

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def set_globals(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__json_public__:
                if key in ('start_date', 'created_at', 'updated_at'):
                    if 'T' in value and 'Z' == value[-1]:
                        value = datetime.datetime.strptime(value[:-5], '%Y-%m-%dT%H:%M:%S')
                setattr(self, key, value)

    def self_check(self):
        errors = {}
        if not self.name:
            errors['name'] = 'nombre de centro deportivo obligatorio'
        if errors:
            raise IntegrityError(errors)
        return True

    @property
    def full_address(self):
        return u'{}, {}, {}'.format(self.address.title(), self.city.title(), self.state.title())

class TournamentRefereeJsonSerializer(JsonSerializer):
    __json_public__ = [
        'tournament_id', 'profile_id', 'is_valid', 'validated_at',
        'created_at', 'created_by', 
    ]

class TournamentReferee(TournamentRefereeJsonSerializer, BaseModel):

    __tablename__ = 'tournaments_referees'
    __schema__ = 'tournament_management'

    tournament_id  = sa.Column(sa.Integer, sa.ForeignKey('tournaments.tournament_id'), primary_key=True)
    tournament     = sa.orm.relationship('Tournament', foreign_keys=[tournament_id])
    profile_id     = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'), primary_key=True)
    profile        = sa.orm.relationship('Profile', foreign_keys=[profile_id])
    is_link_accepted = sa.Column(sa.Boolean)
    link_id        = sa.Column(sa.Integer)
    validated_at   = sa.Column(sa.DateTime)
    created_at     = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by     = sa.Column(sa.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

class TournamentObserverJsonSerializer(JsonSerializer):
    __json_public__ = [
        'tournament_id', 'profile_id', 'is_valid', 'validated_at',
        'created_at', 'created_by', 
    ]

class TournamentObserver(TournamentObserverJsonSerializer, BaseModel):

    __tablename__ = 'tournaments_observers'
    __schema__ = 'tournament_management'

    tournament_id  = sa.Column(sa.Integer, sa.ForeignKey('tournaments.tournament_id'), primary_key=True)
    profile_id     = sa.Column(sa.Integer, sa.ForeignKey('profiles.profile_id'), primary_key=True)
    is_link_accepted = sa.Column(sa.Boolean)
    link_id        = sa.Column(sa.Integer)
    validated_at   = sa.Column(sa.DateTime)
    created_at     = sa.Column(sa.DateTime, default=datetime.datetime.now)
    created_by     = sa.Column(sa.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()
