# -*- coding: utf-8 -*-
from flask_security import UserMixin, RoleMixin
from flask import current_app as app

import sqlalchemy as sa
from sqlalchemy.orm.session import object_session

from flc.core import db, BaseModel, limits
from flc.helpers import JsonSerializer
from flc import settings
from . import template
# from flc.db.connector import BaseModel

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature

from passlib.apps import custom_app_context as pwd_context
import base64
import hashlib
import hmac

import uuid

import datetime
import arrow

import simplejson as json

from dateutil.tz import tzlocal
from dateutil.relativedelta import relativedelta

from jinja2 import Environment, PackageLoader, Template
templates = Environment(loader=PackageLoader('flc', 'models/messages/templates'))

days_of_week = {
    0: u'Lunes',
    1: u'Martes',
    2: u'Miercoles',
    3: u'Jueves',
    4: u'Viernes',
    5: u'Sábado',
    6: u'Domingo',

}

def format_sender(sender):
    sender_data = [
                str(sender.profile_id),
                sender.user.email,
                u' '.join(
                    (sender.user.first_name or '',
                     sender.user.last_name or '')
                )
            ]
    return sender_data

def format_receiver(receiver):
    receivers_data = [
                [
                    str(receiver.profile_id),
                    receiver.user.email,
                    u' '.join(
                        (receiver.user.first_name or '',
                         receiver.user.last_name or '')
                    )
                ]
            ]
    return receivers_data

class MessageMetadataJsonSerializer(JsonSerializer):
    __json_public__ = [
    'profile_id', 'message_id', 'sender', 'receiver', 'subject',
    'body', 'received_date', 'label', 'marker',
    ]

class MessageMetadata(MessageMetadataJsonSerializer, BaseModel):

    __tablename__ = 'message_metadata'
    __schema__ = 'messages'
    __table_args__ = { 'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci' }

    id                     = sa.Column('message_id', sa.Integer, primary_key=True)
    profile_id             = sa.Column('profile_id', sa.Integer)
    sender                 = sa.Column(sa.String(200))
    receiver               = sa.Column(sa.String(200))
    subject                = sa.Column(sa.String(500))
    body                   = sa.Column(sa.Text())
    created_at             = sa.Column(sa.DateTime)
    status                 = sa.Column(sa.CHAR(1))

    @property
    def message_id(self):
        return self.id

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class MessageJsonSerializer(JsonSerializer):
    __json_public__ = [
    'profile_id', 'message_id', 'sender', 'receiver', 'subject',
    'body', 'received_date', 'label', 'marker',
    ]

class Message(MessageJsonSerializer):

    def write(self, 
            sender,
            profile_id,
            receiver,
            subject,
            body,
        ):

        now = datetime.datetime.now()

        msg_meta = MessageMetadata()
        msg_meta.profile_id = profile_id
        msg_meta.sender = json.dumps(sender)
        msg_meta.receiver = json.dumps(receiver)
        msg_meta.subject  = subject
        msg_meta.body = body
        msg_meta.now = now
        msg_meta.save()
        return profile_id, msg_meta.message_id

    def send_mail(self, profile_id, message_id):
        message = MessageMetadata.query.get(message_id)
        recipient = [ r[2].strip() and u'{0} <{1}>'.format(r[2].strip() or r[1], r[1]) or r[1] for r in json.loads(message.receiver)]
        sender = json.loads(message.sender)
        sender = u'{0} <{1}>'.format(sender[2].strip() or sender[1], sender[1])
        subject = message.subject
        body = message.body
        if app.config['SEND_MAIL_ENABLED']:
            from flc.backend import send_mail as celery_send_mail
            celery_send_mail(sender, recipient, subject, body)
        return


    def is_message_id_in_label_index(self, message_id, profile_label_id):
        try:
            label_index = self.db.IndexLabel.get(profile_label_id=profile_label_id)
            if message_id in label_index.message_id:
                return True
        except Model.DoesNotExist:
            pass
    def set_messsage_id_into_label_index(self, message_id, profile_label_id):
        try:
            index_label = IndexLabel.get(profile_label_id=profile_label_id)
        except Model.DoesNotExist:
            index_label = IndexLabel()
            index_label.profile_label_id = profile_label_id
        message_id = uuid.UUID(str(message_id))
        if message_id not in index_label.message_id:
            index_label.message_id = index_label.message_id + [message_id]
            index_label.save()
        return True

    def increment_counter_label(self, profile_id, label):
        u"""Incrementa en uno el contador de labels de un usuario"""
        profile_id = uuid.UUID(str(profile_id))

        labels = self.get_counter_labels(profile_id)

        counters = None
        if labels:
            counters = labels.counter

        if counters:
            if counters.has_key(label):
                counters[label] += 1
            else:
                counters[label] = 1
        else:
            counters = {label: 1}

        try:
            counter = self.db.MCounter.get(profile_id=profile_id)
            counter.counter = counters
            counter.save()
        except Model.DoesNotExist:
            pass

    def get_counter_labels(self, profile_id):
        profile_id = uuid.UUID(str(profile_id))
        try:
            counter = self.db.MCounter.get(profile_id=profile_id)
            return counter
        except Model.DoesNotExist:
            pass

    def message__link_responsable_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_responsable_to_team.html')
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            tournament=tournament,
            sportcenter=sportcenter,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para Responsable de equipo %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_dt_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tpl = Template(template.link_dt_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            link_hash=base64.b64encode( link_hash ),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para dirigir %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_link_observer_to_match(
        self, 
        sender,
        receiver,
        match,
        team1,
        team2
        ):
        tournament = team1.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)
        match_date_date = arrow.get(match.match_date).format('dddd DD/MM/YYYY', locale="es")
        match_date_hour = arrow.get(match.match_date).format('HH:MM', locale="es")


        tpl = Template(template.accepted_link_observer_to_match)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            match=match,
            sportcenter=sportcenter,
            match_date_date=match_date_date,
            match_date_hour=match_date_hour,
            team1=team1,
            team2=team2
        )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"{0}\" aceptó observar {1} vs {2}".format(
                    sender_data[2], team1.name, team2.name
                )

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_player_assist_confirmation(
        self, 
        sender,
        receiver,
        the_other_team,
        match_date,
        ):

        tpl = Template(template.accepted_player_assist_confirmation)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            the_other_team=the_other_team,
            match_date=match_date,

            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" confirma presencia vs %s" % \
            (sender_data[2], the_other_team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_link_responsable_to_team(
        self, 
        sender,
        receiver,
        team,
        ):

        tpl = Template(template.accepted_link_responsable_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" aceptó ser el responsable de %s" % \
            (sender_data[2], team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_link_referee_to_tournament(
        self, 
        sender,
        receiver,
        obj,
        ):

        tpl = Template(template.accepted_link_referee_to_tournament)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            obj=obj,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" aceptó ser referee de %s" % \
            (sender_data[2], obj.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_link_observer_to_tournament(
        self, 
        sender,
        receiver,
        obj,
        ):

        tpl = Template(template.accepted_link_observer_to_tournament)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            obj=obj,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" aceptó ser observador de %s" % \
            (sender_data[2], obj.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_dt_profile_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tpl = Template(template.link_non_existant_dt_profile_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            link_hash=base64.b64encode( link_hash ),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para dirigir %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return u"Solicitud para dirigir %s enviada con éxito" % team.name


    def message__link_non_existant_dt_user_profile_to_team(
        self, 
        sender,
        receiver,
        token,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tpl = Template(template.link_non_existant_dt_user_profile_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            token=base64.b64encode( token ),
            link_hash=base64.b64encode( link_hash ),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para dirigir %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return u"Solicitud para dirigir %s enviada con éxito" % team.name

    def message__link_non_existant_responsable_user_profile_to_team(
        self, 
        sender,
        receiver,
        team,
        token,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        """
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_non_existant_responsable_user_profile_to_team.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            team=team,
            tournament=tournament,
            day_of_week=day_of_week,
            token=base64.b64encode( token ),
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para Responsable de equipo %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )
        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_responsable_profile_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: responsable_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)


        tpl = templates.get_template('link_non_existant_responsable_profile_to_team.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            team=team,
            tournament=tournament,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud para Responsable de equipo %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )
        self.send_mail(*message_id)
        return subject


    def message__link_non_existant_referee_user_profile_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        token,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_non_existant_referee_user_profile_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            tournament=tournament,
            day_of_week=day_of_week,
            token=base64.b64encode( token ),
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud referee en el torneo %s" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )
        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_observer_user_profile_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        token,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_non_existant_observer_user_profile_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            tournament=tournament,
            link_hash=base64.b64encode( link_hash ),
            token=base64.b64encode( token ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud observador en el torneo %s" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_player_user_profile_to_team(
        self, 
        sender,
        receiver,
        team,
        token,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        print team
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_non_existant_player_user_profile_to_team.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            sportcenter=sportcenter,
            tournament=tournament,
            day_of_week=day_of_week,
            token=base64.b64encode( token ),
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud jugador en el equipo %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_referee_profile_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_referee_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            tournament=tournament,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"¿Querés ser referee en %s?" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_observer_profile_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_observer_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            tournament=tournament,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )

        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"¿Querés ser veedor en el torneo %s?" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_non_existant_player_profile_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        u"""
        :param sender: sender_user_profile
        :param receiver: player_user_profile
        :param subject: 'a string representing a subject'
        :param body: 'a string representing a body message'
        :param label: 'a list of strings with the labels that are 
           going to be included in the message',
        :param marker: 'a list of strings with the markers that are 
           going to be included in the message',
        u"""
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_player_to_team.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            tournament=tournament,
            sportcenter=sportcenter,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
        )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud jugador en el equipo %s" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_referee_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        link_hash,
        ):
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_referee_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            sportcenter=sportcenter,
            tournament=tournament,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud referee en el torneo %s" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_referee_to_match(
        self, 
        sender,
        receiver,
        match,
        home_team,
        away_team,
        link_hash,
        ):
        tournament = match.tournament
        sportcenter = match.sportcenter_id and self.db.Sportcenter.query.get(match.sportcenter_id)
        match_date_date = arrow.get(match.match_date).format('dddd DD/MM/YYYY', locale="es")
        match_date_hour = arrow.get(match.match_date).format('HH:MM', locale="es")

        tpl = templates.get_template('link_referee_to_match.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            match=match,
            tournament=tournament,
            sportcenter=sportcenter,
            match_date_date=match_date_date,
            match_date_hour=match_date_hour,
            home_team=home_team,
            away_team=away_team,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Arbitrar el partido {0} vs {1}".format(home_team.name, away_team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_observer_to_match(
        self, 
        sender,
        receiver,
        match,
        home_team,
        away_team,
        link_hash,
        ):
        tournament = match.tournament
        sportcenter = match.sportcenter_id and self.db.Sportcenter.query.get(match.sportcenter_id)
        match_date_date = arrow.get(match.match_date).format('dddd DD/MM/YYYY', locale="es")
        match_date_hour = arrow.get(match.match_date).format('HH:MM', locale="es")

        tpl = templates.get_template('link_observer_to_match.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            match=match,
            tournament=tournament,
            sportcenter=sportcenter,
            match_date_date=match_date_date,
            match_date_hour=match_date_hour,
            home_team=home_team,
            away_team=away_team,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Observar el partido {0} vs {1}".format(home_team.name, away_team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_observer_to_tournament(
        self, 
        sender,
        receiver,
        tournament,
        link_hash,
        ):
        
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_observer_to_tournament.html')

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            tournament=tournament,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Solicitud observador en el torneo %s" % tournament.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__link_player_to_team(
        self, 
        sender,
        receiver,
        team,
        link_hash,
        ):
        tournament = team.tournament
        sportcenters = tournament.sportcenters
        sportcenter = sportcenters and sportcenters[0]
        day_of_week = days_of_week.get(tournament.match_day_of_week)

        tpl = templates.get_template('link_player_to_team.html')
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            tournament=tournament,
            sportcenter=sportcenter,
            day_of_week=day_of_week,
            link_hash=base64.b64encode( link_hash ),
            domain=app.config.get('DOMAIN_NAME'),
            uri=app.config.get('URI_SERVICE'),
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" quiere que seas parte del equipo" % team.name

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__player_assist_confirmation(
        self, 
        sender,
        receiver,
        team,
        the_other_team,
        match,
        link_hash,
        ):
        tpl = Template(template.player_assist_confirmation)

        match_date_humanize = arrow.get(str(match.match_date), 'YYYY-MM-DD HH:mm:ss').format('MMMM DD, YYYY', locale="es")

        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            the_other_team=the_other_team,
            link_hash=base64.b64encode( link_hash ),
            match_date=match_date_humanize,
        )
        
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)


        subject = u"Confirmación de asistencia contra {0} [{1}]".format(the_other_team.name, match_date_humanize)
        
        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__accepted_link_player_to_team(
        self, 
        sender,
        receiver,
        team,
        ):

        tpl = Template(template.accepted_link_player_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"%s forma parte del equipo %s" % \
            (sender_data[2], team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__rejected_link_player_to_team(
        self, 
        sender,
        receiver,
        team,
        ):

        tpl = Template(template.rejected_link_player_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"Jugador %s -> %s - Solicitud rechazada" % \
            (sender_data[2], team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject


    def message__accepted_link_dt_to_team(
        self, 
        sender,
        receiver,
        team,
        ):

        tpl = Template(template.accepted_link_dt_to_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"\"%s\" aceptó dirigir %s" % \
            (sender_data[2], team.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject

    def message__email_validation(
        self, 
        receiver,
        token,
        ):
        tpl = Template(template.email_validation)
        tpl_text = tpl.render(
            receiver=receiver,
            token=base64.b64encode( token ),
            )
        sender_data = [
                    str(settings.SYSTEM_UUID),
                    settings.SYSTEM_EMAIL,
                    settings.SYSTEM_NAME
                ]
        receivers_data = [
                    [
                        str(receiver.profile_id),
                        receiver.user.email,
                        u', '.join(
                            (receiver.user.last_name or u'',
                             receiver.user.first_name or u'')
                        )
                    ]
                ]
        subject = u"Valida tu email"
        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )
        self.send_mail(*message_id)
        return subject

    def message__initialized_tournament(
        self, 
        tournament,
        receivers,
        token,
        ):
        tpl = Template(template.initialized_tournament)
        tpl_text = tpl.render(
            tournament=tournament
            )
        sender_data = [
                    str(settings.SYSTEM_UUID),
                    settings.SYSTEM_EMAIL,
                    settings.SYSTEM_NAME
                ]

        receivers_data = []
        for receiver in receivers:    
            [
                str(receiver.profile_id),
                receiver.user.email,
                ', '.join(
                    (receiver.user.last_name or '',
                     receiver.user.first_name or '')
                )
            ]
                
        subject = u"Torneo {0} iniciado".format(tournament.name)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )
        self.send_mail(*message_id)
        return subject

    def message__unset_dt_from_team(
        self, 
        sender,
        receiver,
        team,
        ):

        tpl = Template(template.unset_dt_from_team)
        tpl_text = tpl.render(
            sender=sender,
            receiver=receiver,
            team=team,
            )
        sender_data = format_sender(sender)
        receivers_data = format_receiver(receiver)

        subject = u"has sido relevado del cargo de DT del equipo %s" % \
            (team.name,)

        message_id = self.write(
                sender_data,
                receiver.profile_id,
                receivers_data,
                subject,
                tpl_text.encode('utf-8')
            )

        self.send_mail(*message_id)
        return subject
