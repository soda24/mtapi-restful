# -*- coding: utf-8 -*-
from flc import settings

email_validation = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    Para activar tu cuenta de {{receiver.role}} de GranLiga.com haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept">validar cuenta</a>.
    Si piensas que hubo un error, por favor haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject">cancelar cuenta</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_referee_to_match = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
El organizador {{sender.user.first_name}} {{sender.user.last_name}} cuenta con vos para dirigir el partido:<br>
A jugar el día {{match_date_date}} a las {{match_date_hour}} {% if sportcenter %} - En {{sportcenter.name}} ({{sportcenter.address}}, {{sportcenter.city}}) {% if match.sportcenter_field %} En la cancha {{match.sportcenter_field}}{% endif %}{% else %}Lugar a confirmar{% endif %}<br/>
<strong>{{team_one.name}} vs {{team_two.name}}</strong>.<br>
Para aceptar debes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto dirigir el partido</a><br/>
Si por algún motivo no puedes hacerlo por favor avisale al organizador del torneo haciendo click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">No puedo dirigir el partido</a>.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_observer_to_match = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
El organizador {{sender.user.first_name}} {{sender.user.last_name}} cuenta con vos para dirigir el partido:<br>
A jugar el día {{match_date_date}} a las {{match_date_hour}} {% if sportcenter %} - En {{sportcenter.name}} ({{sportcenter.address}}, {{sportcenter.city}}) {% if match.sportcenter_field %} En la cancha {{match.sportcenter_field}}{% endif %}{% else %}Lugar a confirmar{% endif %}<br/>
<strong>{{team_one.name}} vs {{team_two.name}}</strong>.<br>
Para aceptar debes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto dirigir el partido</a><br/>
Si por algún motivo no puedes hacerlo por favor avisale al organizador del torneo haciendo click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">No puedo dirigir el partido</a>.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_responsable_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El organizador del torneo <strong>{{tournament.name}}</strong> {{sender.user.first_name}} {{sender.user.last_name}} ha indicado que eres el responsable del equipo \"<b>{{team.name}}</b>\". Para aceptar este rol haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto ser el responsable del equipo</a>.
    Por otro lado, si no deseas ser el responsable del equipo, por favor haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">No acepto ser el responsable del equipo</a>.<br>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_responsable_to_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que {{sender.user.first_name}} {{sender.user.last_name}} ha aceptado ser el responsable de <b>{{team.name}}</b>.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_responsable_user_profile_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El organizador del torneo <strong>{{tournament.name}}</strong> <b>{{sender.user.first_name}} {{sender.user.last_name}}</b> quiere que te hagas cargo del equipo \"<b>{{team.name}}</b>\".<br/>
    Hemos pre-registrado tu cuenta de <i>Delegado Responsable</i> en el sistema. Para ingresar, formar parte de GranLiga.com y hacerte cargo del equipo, haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com como Delegado Responsable</a>.<br/>
    Si no deseas formar parte de GranLiga.com y quieres cancelar tu pre-registración haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject?next_url=/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignora el e-mail hasta el vencimiento del mismo.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_responsable_profile_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El organizador del torneo <strong>{{tournament.name}}</strong> <b>{{sender.user.first_name}} {{sender.user.last_name}}</b> quiere que te hagas cargo del equipo \"<b>{{team.name}}</b>\".<br/>
    Para hacerte cargo del equipo, haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto ser el Delegado Responsable de {{team.name}}</a>.<br/>
    Si por el contrario no deseas ser el Delegado responsable de {{team.name}} <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">no deseo responsabilizarme de {{team.name}}</a> o simplemente ignora el e-mail hasta el vencimiento del mismo.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_dt_user_profile_to_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} solicita que dirijas el equipo \"<b>{{team.name}}</b>\".<br/>
Hemos pre-registrado tu cuenta de DT en el sistema. Para ingresar y formar parte del equipo tienes que hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com como jugador</a>.<br/>
Si no deseas formar parte de GranLiga.com y cancelar tu pre-registración puedes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject?next_url=/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignorar el e-mail hasta el vencimiento del token.<br/>
Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_player_user_profile_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El Delegado Responsable {{sender.user.first_name}} {{sender.user.last_name}} quiere que formes parte del equipo \"<b>{{team.name}}</b>\".<br/>
    Hemos pre-registrado tu cuenta de Jugador en el sistema, para ingresar y formar parte del equipo deberás hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com como jugador</a>.<br/>
    Si no deseas formar parte de GranLiga.com y cancelar tu pre-registración puedes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject?next_url=/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignorar el e-mail hasta el vencimiento del token.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_player_profile_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El Delegado Responsable {{sender.user.first_name}} {{sender.user.last_name}} quiere que formes parte del equipo \"<b>{{team.name}}</b>\".<br/>
    Hemos asignado el perfil de jugador a tu cuenta, para ingresar y formar parte del equipo deberás hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com como jugador</a>.<br/>
    Si no deseas formar parte de GranLiga.com y cancelar tu pre-registración puedes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignorar el e-mail hasta el vencimiento del token.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)


link_non_existant_referee_user_profile_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} te necesita para dirigir \"<b>{{obj.name}}</b>\".<br/>
    Hemos pre-registrado tu cuenta de Referee en el sistema. Para ingresar y formar parte en el torneo tienes que hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com y dirigir en el torneo {{obj.name}}</a>.<br/>
    Si no deseas formar parte de GranLiga.com y cancelar tu pre-registración puedes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject?next_url=/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignorar el e-mail hasta el vencimiento del token.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_non_existant_observer_user_profile_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} te necesita para observar \"<b>{{obj.name}}</b>\".<br/>
    Hemos pre-registrado tu cuenta de Observador en el sistema. Para ingresar y formar parte en el torneo tienes que hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/accept?next_url=/link/{{link_id}}/accept">acepto formar parte de GranLiga.com y observar en el torneo {{obj.name}}</a>.<br/>
    Si no deseas formar parte de GranLiga.com y cancelar tu pre-registración puedes hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/user/registration/{{token}}/reject?next_url=/link/{{link_id}}/reject">no deseo formar parte de GranLiga.com</a> o simplemente ignorar el e-mail hasta el vencimiento del token.<br/>
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_dt_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} te pide que dirijas \"<b>{{team.name}}</b>\". Para aceptar haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto ser el responsable del equipo</a>.
    Por otro lado, si no deseas ser el DT del equipo, por favor hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto ser el DT del equipo</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_player_to_team = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} quiere que seas parte del equipo \"<b>{{team.name}}</b>\".<br>
    Para formar parte del equipo <b>{{team.name}}</b> tienes que hacer click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto formar parte de "{{team.name}}"</a>.
    Por otro lado, si no deseas formar parte de <b>{{team.name}}</b>, por favor haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto formar parte de "{{team.name}}"</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_referee_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} quiere que seas parte del torneo \"<b>{{obj.name}}</b>\".<br>
    Si aceptás hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto formar parte de "{{obj.name}}"</a>.
    Por otro lado, si no deseas formar parte de <b>{{obj.name}}</b>, por favor hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto formar parte de "{{obj.name}}"</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_responsable_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} quiere que seas parte del torneo \"<b>{{obj.name}}</b>\".<br>
    Si aceptás hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto formar parte de "{{obj.name}}"</a>.
    Por otro lado, si no deseas formar parte de <b>{{obj.name}}</b>, por favor hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto formar parte de "{{obj.name}}"</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

link_observer_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} quiere que seas parte del torneo \"<b>{{obj.name}}</b>\".<br>
    Si aceptás hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto formar parte de "{{obj.name}}"</a>.
    Por otro lado, si no deseas formar parte de <b>{{obj.name}}</b>, por favor hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto formar parte de "{{obj.name}}"</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)


link_non_existant_player_profile_to_team = link_player_to_team

link_non_existant_dt_profile_to_team = link_dt_to_team

link_non_existant_observer_profile_to_tournament = link_responsable_to_tournament

link_non_existant_referee_profile_to_tournament = u"""
    Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
    El {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} quiere que seas parte del torneo \"<b>{{obj.name}}</b>\".<br>
    Si aceptás hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">acepto formar parte de "{{obj.name}}"</a>.
    Por otro lado, si no deseas formar parte de <b>{{obj.name}}</b>, por favor hacé click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">NO acepto formar parte de "{{obj.name}}"</a>.
    Pasadas 48hs de recibido este aviso la solicitud quedará sin efecto.
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_referee_to_tournament = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} ha aceptado formar parte de <b>{{obj.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_observer_to_tournament = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} ha aceptado formar parte de <b>{{obj.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_dt_to_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} aceptó dirigir <b>{{team.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_observer_to_match = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que {{sender.user.first_name}} {{sender.user.last_name}} aceptó ser observador del partido:<br>
A jugar el día {{match_date_date}} a las {{match_date_hour}} {% if sportcenter %} - En {{sportcenter.name}} ({{sportcenter.address}}, {{sportcenter.city}}) {% if match.sportcenter_field %} En la cancha {{match.sportcenter_field}}{% endif %}{% else %}Lugar a confirmar{% endif %}<br/>
<strong>{{team1.name}} vs {{team2.name}}</strong>.<br>
 <b>{{team1.name}} vs {{team2.name}}</b> en {{match_date}}. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

rejected_link_player_to_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que {{sender.user.first_name}} {{sender.user.last_name}} rechazó formar parte del plantel de <b>{{team.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_link_player_to_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que {{sender.user.first_name}} {{sender.user.last_name}} aceptó formar parte del plantel de <b>{{team.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

player_assist_confirmation = u"""
Hola {{receiver.name}},<br>
Te pedimos que confirmes tu asistencia al partido que se jugará este {{match_date}} contra el equipo "{{the_other_team.name}}".<br/>
Para confirmar haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/accept">"confirmo que voy a jugar"</a>.</br>
Para avisar que no vas a poder jugar haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/reject">"no puedo jugar esta fecha"</a>.<br/>
Para avisar que estás en duda haz click en <a href="__URI_SERVICE__://__DOMAIN_NAME__/link/{{link_id}}/maybe">"no estoy seguro de poder jugar esta fecha"</a>.<br/>
De no confirmar un día antes del partido la inacción se tomará como una confirmación de que no puedes jugar.<br>
<br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

accepted_player_assist_confirmation = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el {{sender.role}} {{sender.user.first_name}} {{sender.user.last_name}} confirmó que podrá asistir al encuentro de la fecha a jugarse este {{match_date}} contra el equipo <b>{{the_other_team.name}}</b>.
<br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

rejected_player_assist_confirmation = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el Jugador {{sender.user.first_name}} {{sender.user.last_name}} confirmó que no podrá asistir al encuentro el {{match_date}} contra el equipo <b>{{the_other_team.name}}</b>.
<br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

initialized_tournament = u"""
Queremos informarte que el torneo <b>{{tournament.name}}</b> ya ha comenzado.<br/>
Puedes enterarte de mas entrando a <a href="__URI_SERVICE__://__DOMAIN_NAME__">GranLiga.com</a>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)

unset_dt_from_team = u"""
Hola {%if receiver.user.first_name and receiver.user.last_name %}{{receiver.user.first_name}} {{receiver.user.last_name}}{% else %} {{receiver.user.email}}{%endif%},<br>
Queremos informarte que el Responsable {{sender.user.first_name}} {{sender.user.last_name}} ha decidido relevarte del cargo de D.T. responsable del equipo <b>{{team.name}}</b>. <br>
""".replace('__DOMAIN_NAME__', settings.DOMAIN_NAME).replace('__URI_SERVICE__', settings.URI_SERVICE)
