# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta
from sqlalchemy import func, or_
import sqlalchemy as sa
import datetime

class PlayerTeamMetricJsonSerializer(JsonSerializer):
    __json_public__ = [
    'profile_id', 'team_id', 'yellow_cards', 'red_cards', 'green_cards',
    'blue_cards', 'goals_for', 'goals_against', 'time_played',
    ]

class PlayerTeamMetric(PlayerTeamMetricJsonSerializer, BaseModel):

    __tablename__ = 'players_teams'
    __table_args__ = {'schema':'metrics'}

    profile_id    = sa.Column(sa.Integer, primary_key=True)
    team_id       = sa.Column(sa.Integer, primary_key=True)
    yellow_cards  = sa.Column(sa.Integer, default=0)
    green_cards   = sa.Column(sa.Integer, default=0)
    blue_cards    = sa.Column(sa.Integer, default=0)
    red_cards     = sa.Column(sa.Integer, default=0)
    goals_for     = sa.Column(sa.Integer, default=0)
    goals_against = sa.Column(sa.Integer, default=0)
    time_played   = sa.Column(sa.Integer, default=0)
    updated_at    = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def increment(cls, event):
        metric = cls.query.filter_by(profile_id=event.profile_id, team_id=event.team_id).first()
        if not metric:
            metric = cls(profile_id=event.profile_id, team_id=event.team_id)
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 0 +1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 0 +1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 0 +1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 0 +1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 0 +1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 0 +1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()

    @classmethod
    def decrement(cls, event):
        metric = cls.query.filter_by(profile_id=event.profile_id, team_id=event.team_id).first()
        if not metric:
            return
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 1 -1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 1 -1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 1 -1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 1 -1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 1 -1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 1 -1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()


class PlayerMetricJsonSerializer(JsonSerializer):
    __json_public__ = [
    'profile_id', 'yellow_cards', 'red_cards', 'green_cards', 'blue_cards',
    'goals_for', 'goals_against', 'time_played',
    ]

class PlayerMetric(PlayerMetricJsonSerializer, BaseModel):

    __tablename__ = 'players'
    __table_args__ = {'schema':'metrics'}

    profile_id    = sa.Column(sa.Integer, primary_key=True)
    yellow_cards  = sa.Column(sa.Integer, default=0)
    green_cards   = sa.Column(sa.Integer, default=0)
    blue_cards    = sa.Column(sa.Integer, default=0)
    red_cards     = sa.Column(sa.Integer, default=0)
    goals_for     = sa.Column(sa.Integer, default=0)
    goals_against = sa.Column(sa.Integer, default=0)
    time_played   = sa.Column(sa.Integer, default=0)
    updated_at    = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def increment(cls, event):
        metric = cls.query.get(event.profile_id)
        if not metric:
            metric = cls(profile_id=event.profile_id)
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 0 +1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 0 +1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 0 +1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 0 +1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 0 +1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 0 +1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()

    @classmethod
    def decrement(cls, event):
        metric = cls.query.get(event.profile_id)
        if not metric:
            return
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 1 -1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 1 -1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 1 -1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 1 -1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 1 -1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 1 -1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()

class TeamMetricJsonSerializer(JsonSerializer):
    __json_public__ = [
    'team_id', 'yellow_cards', 'red_cards', 'green_cards', 'blue_cards',
    'goals_for', 'goals_against', 'time_played',
    ]

class TeamMetric(TeamMetricJsonSerializer, BaseModel):

    __tablename__ = 'teams'
    __table_args__ = {'schema':'metrics'}

    team_id       = sa.Column(sa.Integer, primary_key=True)
    yellow_cards  = sa.Column(sa.Integer, default=0)
    green_cards   = sa.Column(sa.Integer, default=0)
    blue_cards    = sa.Column(sa.Integer, default=0)
    red_cards     = sa.Column(sa.Integer, default=0)
    goals_for     = sa.Column(sa.Integer, default=0)
    goals_against = sa.Column(sa.Integer, default=0)
    time_played   = sa.Column(sa.Integer, default=0)
    updated_at    = sa.Column(sa.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def increment(cls, event):
        metric = cls.query.get(event.team_id)
        if not metric:
            metric = cls(team_id=event.team_id)
        metric.team_id = event.team_id
        #Aunque deberia existir
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 0 +1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 0 +1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 0 +1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 0 +1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 0 +1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 0 +1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()

    @classmethod
    def decrement(cls, event):
        metric = cls.query.get(event.team_id)
        if not metric:
            return
        #Aunque deberia existir
        if event.event_type == 6:
            metric.goals_for = metric.goals_for or 1 -1
        elif event.event_type == 7:
            metric.goals_against = metric.goals_against or 1 -1
        elif event.event_type == 8:
            metric.yellow_cards = metric.yellow_cards or 1 -1
        elif event.event_type == 9:
            metric.green_cards = metric.green_cards or 1 -1
        elif event.event_type == 10:
            metric.blue_cards = metric.blue_cards or 1 -1
        elif event.event_type == 11:
            metric.red_cards = metric.red_cards or 1 -1
        elif event.event_type == 'change':
            #No hacemos nada ya que no manejamos metricas de cambio
            return
        else:
            raise Exception(event.event_type, "no contemplado")
        metric.save()