# -*- coding: utf-8 -*-
from flc.core import BaseModel, db
from flc.helpers import JsonSerializer
from flask_security import UserMixin, RoleMixin
from dateutil.relativedelta import relativedelta
from sqlalchemy import func, or_
import sqlalchemy as sa
import datetime
from collections import Counter

class ScorerTable:

    def __init__(self):
        self.tournament_id = None

    @classmethod
    def get(cls, tournament_id, names=False):
        MatchEvent = cls.db.MatchEvent
        scores = MatchEvent.query.filter(MatchEvent.tournament_id==tournament_id, MatchEvent.event_type.in_([6])).all()
        # import pdb; pdb.set_trace()
        if names:
            scorers = Counter([(e.profile.user.name, e.team.name) for e in scores])
            scorers = [{'player': k[0], 'team': k[1], 'goals': v} for k, v in scorers.items()]
            scorers.sort(key=lambda x: x['goals'], reverse=True)
        else:
            scorers = Counter([(e.profile.user.name, e.team.name) for e in scores])
        return scorers

class FairPlayTable:

    def __init__(self):
        self.tournament_id = None

    @classmethod
    def get(cls, tournament_id, names=False):
        MatchEvent = cls.db.MatchEvent
        cards = MatchEvent.query.filter(MatchEvent.tournament_id==tournament_id, MatchEvent.event_type.in_([8,9,10,11])).all()

        players = {}
        for event in cards:
            key = (event.profile.user.name, event.team.name)
            if key not in players:
                players[key] = {'yellow_card': 0, 'red_card': 0}
            players[key][event.event_type_label] += 1
        fair_play_table = [{'player': k[0], 'team': k[1], 'red': v['red_card'], 'yellow': v['yellow_card']} for k, v in players.items()]
        fair_play_table.sort(key=lambda x: (x['red'], x['yellow']), reverse=True)
        return fair_play_table
