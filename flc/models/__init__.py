# -*- coding: utf-8 -*-

from .users import *
from .teams import *
from .tournaments import *
from .linkages import *
from .messages import *
from .matches import *
from .position_tables import *
from .events import *
from .metrics import *
from .tables import *

class DB(object):
    pass

container = DB()
container.User              = User
container.Role              = Profile
container.Profile           = Profile
container.Organizer         = Organizer
container.Observer          = Observer
container.Player            = Player
container.Linkage           = Linkage
container.Message           = Message
container.Fixture           = Fixture
container.FixtureRound      = FixtureRound
container.Match             = Match
container.MatchReferee      = MatchReferee
container.MatchObserver     = MatchObserver
container.PositionTable     = PositionTable
container.Team              = Team
container.TeamPlayer        = TeamPlayer
container.Tournament        = Tournament
container.PositionTable     = PositionTable
container.TournamentPositionTable = TournamentPositionTable
container.TournamentReferee = TournamentReferee
container.TournamentObserver = TournamentObserver
container.MatchEvent            = MatchEvent
container.MatchEvents           = MatchEvents
container.ScorerTable           = ScorerTable
container.FairPlayTable         = FairPlayTable

container.User.db                    = container
container.Role.db                    = container
container.Profile.db                 = container
container.Organizer.db               = container
container.Observer.db                = container
container.Player.db                  = container
container.Linkage.db                 = container
container.Message.db                 = container
container.Fixture.db                 = container
container.FixtureRound.db            = container
container.Match.db                   = container
container.MatchReferee.db            = container
container.MatchObserver.db           = container
container.PositionTable.db           = container
container.Team.db                    = container
container.TeamPlayer.db              = container
container.Tournament.db              = container
container.PositionTable.db           = container
container.TournamentPositionTable.db = container
container.TournamentReferee.db       = container
container.TournamentObserver.db      = container
container.MatchEvent.db              = container
container.MatchEvents.db             = container
container.ScorerTable.db             = container
container.FairPlayTable.db           = container
