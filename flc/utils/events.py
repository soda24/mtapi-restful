# -*- coding: utf-8 -*-

# 1:  'titular',
# 2:  'captain',
# 3:  'goalkeeper',
# 4:  'player_in',
# 5:  'player_out',
# 6:  'goal',
# 7:  'goal_against',
# 8:  'yellow_card',
# 9:  'green_card',
# 10: 'blue_card',
# 11: 'red_card',

class Player:
    def __init__(self, profile_id,):
        self.profile_id = profile_id
        self.time_in = []
        self.time_out = []

    def in_(self, half_time, time, position):
        self.time_in.append([half_time, time, position])

    def out_(self, half_time, time):
        self.time_out.append([half_time, time])

class Events:
    def __init__(self, evts):
        self.players = {}
        self.events = evts
        print "Events"
        self.events.sort(key=lambda x: [not x.get('is_initial_event'), int(x['half_time']), int(x['time'])])
        self.errors = []

    def analyze(self):
        for ev in [ev for ev in self.events if ev['event_type'] == 1]:
            p = self.players.get(ev['profile_id'])
            if not p:
                p = Player(ev['profile_id'])
                self.players[ev['profile_id']] = p
                p.in_(1, 1, ev['position'])
            else:
                p.in_(ev['half_time'], ev['time'], ev['position'])

        for ev in [ev for ev in self.events if ev['event_type'] == 5]:
            p = self.players.get(ev['profile_id'])
            p = Player(ev['profile_id'])
            self.players[ev['profile_id']] = p
            p.out_(ev['half_time'], ev['time'])

    def analyze2(self):
        for ev in self.events:
            if not ev.get('event_id'):
                self.errors.append(
                    dict(
                        message=u'El evento no está identificado correctamente',
                        event_id=ev.get('id') or ev.get('event_id'),
                        profile_id=ev.get('profile_id')
                    )
                )
            if ev['event_type'] in (1, 4):
                self.players[ev['profile_id']] = True
            elif ev['event_type'] in (5, 11):
                self.players[ev['profile_id']] = False
            elif ev['event_type'] in (6, 7, 8):
                if not self.players.get(ev['profile_id']):
                    self.errors.append(
                        dict(
                            message="El jugador no estaba en el campo",
                            event_id=ev.get('id') or ev.get('event_id'),
                            profile_id=ev.get('profile_id')
                        )
                    )
        return self.errors

def analyze(data):
    evts = Events(data)
    errors = evts.analyze2()
    return errors
