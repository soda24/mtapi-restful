# -*- coding: utf-8 -*-
"""
mitorneo.backend.tasks
~~~~~~~~~~~~~~

mitorneo backend tasks module
"""

from flask.ext.mail import Message as message
from flask import current_app

from werkzeug.local import LocalProxy

from flc import settings
# from mtapi.models import *
# from flc.backend.celery import celery

from datetime import datetime
from collections import Counter

from flc.models import *

_mail = LocalProxy(lambda: current_app.extensions['mail'])


# class SqlAlchemyTask(celery.Task):
#     """An abstract Celery Task that ensures that the connection the the
#     database is closed on task completion"""
#     abstract = True

#     def after_return(self, status, retval, task_id, args, kwargs, einfo):
#         db.session.remove()

# @celery.task(base=SqlAlchemyTask, name='tasks.send_mail', queue="celery")
def send_mail(sender, recipient, subject, body):

    sender = u'{0} <{1}>'.format(
                settings.SYSTEM_NAME,
                settings.SYSTEM_EMAIL
            ).encode('utf-8')
    msg = message(subject,
                  sender=sender,
                  )
    msg.recipients = recipient
    msg.body = body
    msg.html = body
    if settings.MAIL_CATCH_ALL:
        msg.recipients = [u'{0} <{1}>'.format(
            *settings.FORCED_MAIL).encode('utf-8')
        ]
    _mail.send(msg)

# @celery.task
def update_position_table(tournament_id=None):
    if tournament_id:
        tournament = Tournament.query.get(tournament_id)
        re_match_factor = (tournament.match_rematch and 2) or 1
        rounds = (len(tournament.teams.all())-1)*re_match_factor
        for x in range(rounds):
            print tournament.tournament_id, x
            tpt = TournamentPositionTable.get(tournament.tournament_id, x)
            tpt.process_position_table()
    else:
        tournaments = Tournament.query.filter(Tournament.status.in_(['p', 'i'])).all()
        for tournament in tournaments:
            for x in range(len(tournament.team_id)-1):
                tpt = TournamentPositionTable.get(tournament.tournament_id, x)
                tpt.process_position_table()


# @celery.task
def send_manager_removed_email(*recipients):
    print 'sending manager removed email...'
