import uuid
import requests

import simplejson as json
from flc.helpers import JsonSerializer, JSONEncoder

from flask_mail import Mail

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.sqlalchemy import declarative_base, _QueryProperty, Model, _BoundDeclarativeMeta, BaseQuery


encoder = JSONEncoder()

mail = Mail()

# db = SQLAlchemy()
db = SQLAlchemy(session_options={'expire_on_commit': False}) #Esto es muy peligroso!!! PEro es la unica manera de poder entrometerse despues de un commit()

def make_declarative_base(self):
    """Creates the declarative base."""
    base = declarative_base(cls=Model, name='Model',
                            metaclass=_BoundDeclarativeMeta)
    base.query = _QueryProperty(self)
    return base
BaseModel = make_declarative_base(db)

# database = peewee.MySQLDatabase("workflow_v1", host="workflow_ops", port=3306, user="root", passwd="root")
# database.connect()
