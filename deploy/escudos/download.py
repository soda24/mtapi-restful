import lxml.html
import scraperwiki
import unicodedata
import requests

def elimina_tildes(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

url = 'http://lapaginamillonaria.com/estadisticas/campeonato-argentino-primera-division/posiciones'
html = scraperwiki.scrape(url)
root = lxml.html.fromstring(html)
tags = root.cssselect('.contentEstadisticasGoleadores table tbody tr td.equipo')
for tag in tags:
    escudo = tag.cssselect('img')[0].get('src')
    s = tag.cssselect('.nombre')[0].text.strip()
    nombre = elimina_tildes(s.encode('utf-8').decode('utf-8')).replace('`', '').replace(' ', '_').replace('.', '').replace('(', '').replace(')', '')
    nombre_file = nombre.lower()+'.gif'
    print nombre_file
    r = requests.get(escudo, stream=True)
    if r.status_code == 200:
        with open(nombre_file, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)


